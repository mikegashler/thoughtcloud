# thoughtcloud

I made this little web app for organizing my thoughts with my phone. It has a web-server back-end and uses Javascript on the front-end.

It's kind of like a personal web. Each page holds a thought and a list of items. Items may link to other pages. And I added lots of buttons to make navigating and editing easy.
