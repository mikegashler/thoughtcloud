// ----------------------------------------------------------------
// The contents of this file are dedicated to the public domain as
// described at http://creativecommons.org/publicdomain/zero/1.0/.
// ----------------------------------------------------------------

// *** Table of element ids
// ************************
// action_menu_[id] = actions menu in page id
// aft = input on replace menu
// am_[account_name] = menu for the account (on the login page)
// bef = input on replace menu
// cb_[footer_name] = checkbox for adding a footer (on the footers menu)
// canvas_[id]_[i] = canvas for page id, item i
// debug_log = pre tag for the log in the debug window
// edit_text = text area for editing text
// item_[id]_[i] = span that wraps item i on page id, so it can be selected (on main page). Also used to turn it into a textarea for editing.
// item_menu_[id]_[i] = menu for page id, item i
// li_[account_name] = log in button for the account (on the login page)
// list_[id] = list for page id
// newname = the new name (on the change account name menu)
// new_area_[id] = span for the new buttons on page id
// new_name = input for new account name on login page
// newpw = new password (on change password menu)
// new_pw = input for new account password on login page
// old_pw = old password (on change password menu)
// page_[id] = the div for page [id]
// query = search string on move menu or search menu
// pw_[account_name] = password entry for an account (on the login page)
// raw_content = text area for raw content (on raw content page)
// the_pw = the password (on change name or change password menus)
// thought_area_[id] = span that wraps the thought on page id, so it can be turned into a textarea for editing.
// thought_below_[id] = thought menu on page id
// title_bar_menu = div for menus in the title bar at the top

function bad_menu_state() {
	alert('menu problem');
	console.trace();
}

function get_session_id() {
	let cookie = document.cookie;
	const start_pos = cookie.indexOf('sid=') + 4;
	if (start_pos < 0) {
		alert('expected a cookie');
	}
	const end_pos = cookie.indexOf(';', start_pos);
	let sid;
	if (end_pos >= 0) {
		sid = cookie.substring(start_pos, end_pos);
	} else {
		sid = cookie.substring(start_pos);
	}
	return sid;
}

function browser_name() {
	if (window.navigator.userAgent.search('Edg') >= 0) {
		return 'Edge'; // Edge strings may also contain "Chrome" and "Safari"
	} else if (window.navigator.userAgent.search('YaBrowser') >= 0) {
		return 'Yandex'; // Yandex strings may also contain "Chrome" and "Safari"
	} else if (window.navigator.userAgent.search('Firefox') >= 0) {
		return 'Firefox';
	} else if (window.navigator.userAgent.search('Chrome') >= 0) {
		return 'Chrome'; // Chrome strings may also contain "Safari"
	} else if (window.navigator.userAgent.search('Safari') >= 0) {
		return 'Safari';
	} else {
		return 'Other';
	}
}

function operating_system_name() {
	if (window.navigator.userAgent.search('Android') >= 0) {
		return 'Android'; // Android strings may also contain "Linux"
	} else if (window.navigator.userAgent.search('Linux') >= 0) {
		return 'Linux';
	} else if (window.navigator.userAgent.search('Macintosh') >= 0) {
		return 'Mac';
	} else if (window.navigator.userAgent.search('Windows') >= 0) {
		return 'Windows';
	} else if (window.navigator.userAgent.search('iPhone') >= 0) {
		return 'iPhone';
	} else {
		return 'Other';
	}
}

const g_origin = window.location.origin;
const g_op_sys = operating_system_name();
const g_mobile = (g_op_sys === 'Android' || g_op_sys === 'iPhone' ? true : false);
let g_content; // The client's copy of all the content.
let g_content_old_unsent; // Content (as it was before the client modified it) for modifications that have not yet been sent to the server.
let g_content_old_sent; // Content (as it was before the client modified it) that has been sent to the server, but has not yet been acknowledged.
let g_sent_time = 0;
let g_current_page_stack = []; // A list of (page_id, item_index) tuples
let g_menu_callback = bad_menu_state;
let g_menu_div_id;
const g_session_id = get_session_id();
let g_remerges = 0;
let g_selected_page = '';
let g_selected_items = [];
let g_search_params = null;
let g_selected_div = null; // Used for creating the prev-next menu at the currently-selected search match
let g_close_ops = []; // operations to perform when the menu is closed
let g_canvas_tool = 'pen';
let g_editing_page = ''; // The page id the user is currently editing
let g_editing_index = -3; // -3 = interruptable, -2 = not interruptable (in some menu), -1 = editing thought, 0-n = editing the specified item
let g_sw_cache_working = true;
let g_account_names = null; // A list of account names

let g_account = {
	logged_in: false, // indicates whether the user is logged in with the server
	dark_mode: true, // if true: display uses a dark background. if false: display uses a light background.
	debug_log: [], // a copy of messages that have been logged to the console
	debug_mode: false, // if true: display a debug window with extra information. if false: don't.
	last_sync_out_time: new Date(), // The last time a sync request was sent to the server
	last_sync_in_time: new Date(0), // The last time a sync response was received from the server
	revision: 0, // Revision number used by sync to keep changing pages updated on all clients. (Only the server ever updates this value.)
	username: '', // The username of the currently logged-in user
	undo_history: [], // Pages that have been modified
	undo_pos: 0,
	view_pages: [], // Objects that represent the stack of pages currently in the view
};

function reset_globals() {
	g_account.logged_in = false;
	g_content = {};
	g_content_old_unsent = {};
	g_content_old_sent = {};
	g_menu_div_id = '';
	g_account.revision = 0;
	g_account.username = '';
	delete(g_account.hash_pri);
	delete(g_account.hash_pub);
	set_current_page('start');
}

async function change_identity(username, password) {
	let p = httpGet(`${g_origin}/flush_cache.sw`); // make sure we don't use cached data from a different account
	g_account.username = username;
	const salt_private = 'zu5icgft82p4blwd';
	g_account.hash_pri = sha512(`${password}_${salt_private}_${username}`);
	const salt_public = 'd82jf7dwfh338fmx';
	g_account.hash_pub = sha512(`${salt_public}_${username}_${password}`);
	try {
		await p;
	} catch(ex) {
		console.log(`Failed to flush cache: ${ex}`);
	}
	set_current_page('start');
}

function hash_password(username, password) {
	const salt = 'd82jf7dwfh338fmx';
	return sha512(`${salt}_${username}_${password}`);
}

function rev_password() {
	log('Attempting to reverse password');
	const charset = 'abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	const username = '';
	const target = '';
	const it = counter(charset.length);
	while(true) {
		let c = it.next().value;
		if (c.length >= 7) {
			log('Tried all passwords with length less than 7. Alas, no matches');
			break;
		}
		let tmp = [];
		for (let i = 0; i < c.length; i++) {
			tmp.push(charset[c[i]]);
		}
		let cand = tmp.join('');
		const hash = hash_password(username, cand);
		if (hash === target) {
			log(`Got it: ${cand}`);
			return;
		}
	}
}

function on_login_key(el, account_name) {
	if (event.key === "Enter") {
		document.getElementById(`li_${account_name}`).click();
	}
}

function on_click_account_action(op) {
	g_menu_callback(op);
}

async function log_in_account(account_name) {
	let pw = document.getElementById(`pw_${account_name}`).value;
	await change_identity(account_name, pw);
	await log_in(false);
}

function make_login_page() {
	if (!g_account_names) {
		log_out('Loading account names...');
		return;
	}
	let s = [];
	let show_new_account = true;
	if (g_account_names.length > 0) {
		if (g_account.username.length > 0 && g_account_names.includes(g_account.username)) {
			s.push('<h3>Log back in</h3>');
			s.push('<table width="100%">');
			s.push('<tr>');
			s.push(`<td align="right" width="200px" valign="top">${g_account.username}</td>`);
			s.push(`<td valign="top"><input type="text" class="pw" autocomplete="off" id="pw_${account_name}" onkeypress="on_login_key(this, '${g_account.username}')"><span id="am_${g_account.username}"></span> &nbsp;</td>`);
			s.push(`<td valign="top" width="250px"> &nbsp;<button id="li_${account_name}" onclick="log_in_account('${account_name}')">Log in</button></td>`);
			s.push('</tr>');
			s.push('</table>');
			s.push('<button onclick="log_out()">Change user</button>');
			show_new_account = false;
		} else {
			s.push('<h3>Log in</h3>');
			s.push('<table width="100%">');
			for (account_name of g_account_names) {
				s.push('<tr>');
				s.push(`<td align="right" width="200px" valign="top">${account_name}</td>`);
				s.push(`<td valign="top">`);
				s.push(`<input type="text" class="pw" autocomplete="off" id="pw_${account_name}" onkeypress="on_login_key(this, '${account_name}')"><span id="am_${account_name}"></span> &nbsp;`);
				if (account_name === 'test') {
					s.push('(The password is "test".)');
				}
				s.push(`</td>`);
				s.push(`<td valign="top" width="250px"> &nbsp;<button id="li_${account_name}" onclick="log_in_account('${account_name}')">Log in</button></td>`);
				s.push('</tr>');
			}
			s.push('</table>');
		}
	}

	if (show_new_account) {
		s.push('<br><a class="link" href="docs/index.ajax">About ThoughtCloud</a>');
		s.push('<br><br>');

		s.push('<h3>New account</h3>');
		s.push('<table>');
		s.push('<tr><td align="right">name:</td><td width="520px"><input type="input" id="new_name" style="width:100%"></td></tr>');
		s.push('<tr><td align="right">password:</td><td width="520px"><input type="text" class="pw" autocomplete="off" id="new_pw"></td></tr>');
		s.push('<tr><td></td><td><button onclick="new_account()">Make new account</button></td></tr>');
		s.push('</table>');
	}

	if (g_account.debug_mode) {
		s.push('<br>')
		make_debug_page(s);
	}

	let the_content = document.getElementById('the_content');
	the_content.innerHTML = s.join('');
}
