// ----------------------------------------------------------------
// The contents of this file are dedicated to the public domain as
// described at http://creativecommons.org/publicdomain/zero/1.0/.
// ----------------------------------------------------------------

function log(s) {
	const msg = `${new Date().toISOString().replace('T', ' ')} ${s}`;
	console.log(msg);
	g_account.debug_log.push(msg);
	refresh_debug_log();
}

// Inserts a string into g_account.debug_log in alphabetical order
// (because the time-stamps at the beginning should make all the lines be in alphabetical order).
// hint_index indicates where you think the line should probably go.
// If you give good hints, it will run a bit faster.
function insert_log(s, hint_index) {
	// Find where s belongs in alphabetical order
	while(hint_index < g_account.debug_log.length && g_account.debug_log[hint_index].localeCompare(s) <= 0) {
		hint_index++;
	}
	while(hint_index > 0 && g_account.debug_log[hint_index - 1].localeCompare(s) > 0) {
		hint_index--;
	}

	// Insert s into the log
	g_account.debug_log.splice(hint_index, 0, s);

	// Return the next index
	return hint_index + 1
}

function close_menu() {
	// log('in close_menu()');
	if (g_menu_div_id === '')
		return;

	// Hide the virtual keyboard
	if ('virtualKeyboard' in navigator)
		navigator.virtualKeyboard.hide();

	// Do closing operations
	while (g_close_ops.length > 0) {
		const op = g_close_ops.pop();
		op();
	}

	// Clear the menu
	if (g_menu_div_id.length > 0) {
		let menu_div = document.getElementById(g_menu_div_id);
		g_menu_div_id = '';
		g_menu_callback = bad_menu_state;
		if (menu_div) {
			menu_div.innerHTML = '';
		}
	}
}

function get_original_text_being_edited() {
	if (!g_content[g_editing_page] || !g_content[g_editing_page].items) {
		throw new Error(`No page named ${g_editing_page} in get_original_text_being_edited`);
	}
	if (g_editing_index < -1) {
		log('get_original_text_being_edited called when not editing anything');
		return '';
	} else if (g_editing_index === -1) {
		return g_content[g_editing_page].thought;
	} else if (g_editing_index < g_content[g_editing_page].items.length) {
		return g_content[g_editing_page].items[g_editing_index].text;
	} else if (g_editing_index === g_content[g_editing_page].items.length) {
		return ''; // adding a new item
	} else {
		throw new Error('invalid value for g_editing_index');
	}
}

function clear_selection() {
	if (g_selected_page in g_content) {
		let items = g_content[g_selected_page].items;
		for(var i = 0; i < items.length; i++) {
			let item_span = document.getElementById(`item_${g_selected_page}_${i}`);
			if (item_span) {
				item_span.classList.remove('selected');
			}
		}
	}
	g_selected_items = [];
	g_selected_page = '';
}

function find_parents(page_id) {
	if (!g_content[page_id]) {
		log(`no content for page_id: ${page_id}`);
		console.trace();
		return [];
	}
	if (g_content[page_id]._parents) {
		return g_content[page_id]._parents;
	}
	let parents = [];
	for (let cand_page_id in g_content) {
		if (g_content[cand_page_id] && g_content[cand_page_id].items) {
			for (const [index, item] of g_content[cand_page_id].items.entries()) {
				if (item.dest === page_id) {
					parents.push([cand_page_id, index]);
				}
			}
		} else {
			log(`page ${cand_page_id} looks explicitly undefined`);
		}
	}
	g_content[page_id]._parents = parents;
	return parents;
}

function children_ids(page_id) {
	const children = [];
	for (const item of g_content[page_id].items) {
		if (item.dest) {
			children.push(item.dest);
		}
	}
	return children;
}

// Consumes a list of page ids.
// Returns a list of those page ids and all descendants.
function get_all_descendants(page_ids) {
	let all_pages = new Set();
	let frontier = [];
	for (let page_id of page_ids) {
		all_pages.add(page_id);
		frontier.push(page_id);
	}
	while(frontier.length > 0) {
		const page_id = frontier.pop();
		const children = children_ids(page_id);
		for (const child of children) {
			if (!all_pages.has(child)) {
				all_pages.add(child);
				frontier.push(child);
			}
		}
	}
	return [...all_pages];
}

function menu_confirm(message, div_id, cb) {
	close_menu();
	g_menu_div_id = div_id;
	g_menu_callback = cb;
	const s = [];
	s.push('<div class="modal">');
	s.push(message);
	s.push(`<br> <button onclick="g_menu_callback('confirm')">Yes</button>`);
	s.push(` <button onclick="close_menu()">Cancel</button>`);
	s.push('</div><br>');
	let menu_div = document.getElementById(g_menu_div_id);
    menu_div.innerHTML = s.join('');
}

function arrow_done() {
	close_menu();
	render_pages();
	sync();
}

function item_operation_done() {
	clear_selection();
	close_menu();
	render_pages();
	sync();
}

function accept_text_edit() {
	const new_text = document.getElementById('edit_text').value;
	log(`Accepting new text: ${new_text}`);
	g_menu_callback(new_text); // this line precedes close_menu(), which reloads the page 
	clear_selection();
	close_menu();
}

function show_virtual_keyboard() {
	if (!('virtualKeyboard' in navigator))
		return;
	navigator.virtualKeyboard.overlaysContent = true;
	navigator.virtualKeyboard.show();
}

// Add a menu below the textarea with Ok and Cancel buttons
function add_edit_text_menu(span_id, bullet, initial_text, cb) {
	let item_span = document.getElementById(span_id);
	if (!item_span) {
		throw new Error(`no element with id: ${span_id}`);
	}
	let s = [];
	s.push(`<table width="100%"><tr>`);
	if (bullet) {
		s.push(`<td valign="top" align="right" width="50px">●</td>`);
	}
	//s.push(`<td><textarea id="edit_text" oninput="auto_grow_text_area(this)" virtualkeyboardpolicy="manual" onfocus="show_virtual_keyboard();">${initial_text || ''}</textarea><br>`);
	s.push(`<td><textarea id="edit_text" oninput="auto_grow_text_area(this)">${initial_text || ''}</textarea><br>`);
	s.push('<div class="modal">');
	s.push(` <button onclick="accept_text_edit()"><big>&nbsp;Ok&nbsp;</big></button>`);
	s.push(` <button onclick="close_menu()"><big>Cancel</big></button>`);
	s.push('</div><br>');
	s.push(`</td></tr></table>`);
	item_span.innerHTML = s.join('');
	g_menu_div_id = span_id;
	g_menu_callback = cb;
	let edit_text = document.getElementById(`edit_text`);
	auto_grow_text_area(edit_text);
	const text_len = edit_text.value.length;
	edit_text.setSelectionRange(text_len, text_len);
	edit_text.focus();
}

function scroll_to_page(page_id) {
	scrollIntoViewWithOffset(`page_${page_id}`, 120);
}



function delete_page(page_id) {
	const parents = find_parents(page_id);
	for (const [parent, link_index] of parents) {
		before_page_change(parent);
		if (g_content[parent] && g_content[parent].items) {
			g_content[parent].items.splice(link_index, 1);
		}
	}
	before_page_change(page_id);
	delete g_content[page_id];
	sync();
}

function update_incoming_links_to_match_thought(pid) {
	const abbr = abbreviate(g_content[pid].thought);
	const parents = find_parents(pid);
	for (const [page_id, link_index] of parents) {
		before_page_change(page_id);
		if (g_content[page_id].items[link_index].text !== abbr) {
			g_content[page_id].items[link_index].mod = Date.now();
			g_content[page_id].items[link_index].text = abbr;
		}
	}
	sync();
}

function del_page(page_id) {
	if (page_id === 'start') {
		alert('Sorry, the start page cannot be deleted');
	} else {
		menu_confirm('Are you sure you want to delete this whole page?', `thought_below_${page_id}`, function(val) {
			let gotopage = 'start';
			const parents = find_parents(page_id);
			if (parents.length > 0) {
				gotopage = parents[0][0];
			}
			delete_page(page_id);
			render_pages(gotopage);
		});
	}
}

function on_click_dev_action(page_id, op) {
	if (op === 'copy_url') {
		let clean_url = crop_params(window.location.href);
		copy_to_clipboard(`${clean_url}?q=${page_id}`);
		alert('Direct link copied. Ready to paste it somewhere.');
	} else if (op === 'save') {
		save_server_state();
	} else {
		alert(`unrecognized operation ${op}`);
	}
}

// todo: remove this dead code and whatever it calls that isn't used
function menu_dev(page_id)
{
	close_menu();
	g_menu_div_id = `thought_below_${page_id}`;
	g_menu_callback = bad_menu_state;
	const s = [];
	s.push('<div class="modal">');
	s.push(` <button onclick="on_click_dev_action(page_id, 'copy_url')">Copy url</button>`);
	s.push(` <button onclick="on_click_dev_action(page_id, 'save')">Save (server-side)</button>`);
	s.push(' <button onclick="close_menu()">Close</button>');
	s.push('</div><br>');
	let menu_div = document.getElementById(g_menu_div_id);
	menu_div.innerHTML = s.join('');
}

function menu_copy(div_id, text)
{
	if (g_menu_div_id === div_id) {
		close_menu();
		return;
	}
	close_menu();
	g_menu_div_id = div_id;
	const s = [];
	s.push('<div class="modal">');
	s.push('Text to copy:<br>');
	s.push('<textarea id="copy_area"></textarea><br>');
	s.push('<button onclick="close_menu()">Close</button>');
	s.push('</div><br>');
	let menu_div = document.getElementById(g_menu_div_id);
    menu_div.innerHTML = s.join('');
	let copy_area = document.getElementById(`copy_area`);
	copy_area.textContent = text;
	copy_area.select();
}

function menu_paste(div_id, cb)
{
	close_menu();
	g_menu_callback = cb;
	g_menu_div_id = div_id;
	const s = [];
	s.push('<div class="modal">');
	s.push('Paste text here. I will parse it, and fill the current page with the content:<br>');
	s.push('<textarea id="paste_area" rows="6"></textarea><br>');
	s.push(`<input type="checkbox" id="have_thought" checked><label for="have_thought"> Have thought</label><br>`);
	s.push(`<input type="checkbox" id="asterisks" checked><label for="asterisks"> Items start with asterisks</label><br>`);
	s.push(' <button onclick="on_paste_text()">Ok</button>');
	s.push(' <button onclick="close_menu()">Cancel</button>');
	s.push('</div><br>');
	let menu_div = document.getElementById(g_menu_div_id);
    menu_div.innerHTML = s.join('');
	let paste_area = document.getElementById(`paste_area`);
	paste_area.select();
}

function on_click_thought(page_id) {
	if (g_editing_index >= -1) {
		let text_area = document.getElementById('edit_text');
		let orig = get_original_text_being_edited();
		if (orig !== text_area.value) {
			scrollIntoViewWithOffset('edit_text', 120);
			return; // The user has unsaved changes in another edit box, so let's implicitly discard those.
		}
	}

	let initial_text = g_content[page_id].thought || '';
	close_menu();
	g_close_ops.push(() => {
		g_editing_index = -3;
		if (!g_content[page_id].thought) {
			g_content[page_id].mod = Date.now();
			g_content[page_id].thought = 'Untitled';
		}
		if (!g_content[page_id].free) {
			update_incoming_links_to_match_thought(page_id);
		}
		render_pages(page_id);
	});

	// if (g_content[page_id].free) {
	// 	s.push('🦅');
	// }
	g_editing_page = page_id;
	g_editing_index = -1;
	add_edit_text_menu(`thought_area_${page_id}`, false, initial_text, function(new_text) {
		before_page_change(page_id);
		g_content[page_id].mod = Date.now();
		g_content[page_id].thought = new_text;
		item_operation_done();
	});
}


function action_date(page_id) {
	const now = new Date();
	const now_text = `${now.getFullYear()}-${('' + (now.getMonth() + 1)).padStart(2, '0')}-${('' + now.getDate()).padStart(2, '0')}`;
	before_page_change(page_id)
	if (g_selected_page !== page_id || g_selected_items.length === 0) {
		clear_selection();
		g_content[page_id].items.push({
			text: now_text,
		});
	} else {
		for (let item_index of g_selected_items) {
			let text = g_content[page_id].items[item_index].text;
			text = remove_leading_date(text);
			text = now_text + ' ' + text;
			g_content[page_id].items[item_index].mod = Date.now();
			g_content[page_id].items[item_index].text = text;
		}
	}
	render_pages(page_id);
	sync();
}

function get_sort_value_from_item(item) {
	if (document.getElementById('sort_alpha').checked) {
		return item.text || '';
	} else if (document.getElementById('sort_date').checked) {
		return item.mod || '';
	} else if (document.getElementById('sort_rand').checked) {
		return '' + (Math.random() * 1000000);
	} else {
		console.log('No sort method has been selected. Defaulting to random.');
		return '' + (Math.random() * 1000000);
	}
}

function do_sort(page_id) {
	// Determine what to sort
	let indexes_to_sort = [];
	let stuff_to_sort = []; // Each entry is a [ string to sort, index of where it came from ]
	if (g_selected_items.length === 1) {
		alert('Cannot sort just one selected item');
		return;
	} else if (g_selected_items.length > 1) {
		for (const item_index of g_selected_items) {
			stuff_to_sort.push([ get_sort_value_from_item(g_content[page_id].items[item_index]), item_index ]);
			indexes_to_sort.push(item_index);
		}
	} else {
		for (let i = 0; i < g_content[page_id].items.length; i++) {
			stuff_to_sort.push([ get_sort_value_from_item(g_content[page_id].items[i]), i ]);
			indexes_to_sort.push(i);
		}
	}

	// Sort it
	before_page_change(page_id)
	stuff_to_sort.sort((a, b) => smart_compare(a[0], b[0]));

	// Merge the sorted items back into the main list
	let new_items = [];
	let j = 0;
	for (let i = 0; i < g_content[page_id].items.length; i++) {
		if (indexes_to_sort.includes(i)) {
			const index = stuff_to_sort[j++][1];
			new_items.push(g_content[page_id].items[index]);
		} else {
			new_items.push(g_content[page_id].items[i]);
		}
	}
	if (new_items.length !== g_content[page_id].items.length) {
		log('Sorted items have different length! Aborting sort to prevent data loss!');
		return;
	}
	g_content[page_id].items = new_items;

	// Refresh
	render_pages(page_id);
	sync();
}

function menu_sort(page_id) {
	close_menu();

	// Make the menu
	g_menu_div_id = `new_area_${page_id}`;
	g_menu_callback = bad_menu_state;
	const s = [];
	s.push('<div class="modal">');
	s.push(`<h3>Sort items...</h3>`);
	s.push(`<input style="transform: scale(1.8)" type="radio" id="sort_alpha" name="sort_method" checked><label for="sort_alpha"> alphabetically</label><br>`);
	s.push(`<input style="transform: scale(1.8)" type="radio" id="sort_date" name="sort_method"><label for="sort_date"> by last modified date</label><br>`);
	s.push(`<input style="transform: scale(1.8)" type="radio" id="sort_rand" name="sort_method"><label for="sort_rand"> randomly</label><br>`);
	s.push(` <button onclick="do_sort('${page_id}')">Sort</button>`);
	s.push(' <button onclick="render_pages()">Cancel</button>');
	s.push('</div><br>');
	let menu_div = document.getElementById(g_menu_div_id);
    menu_div.innerHTML = s.join('');
}

function action_sort(page_id) {
	// Make sure only items on this page are selected
	if (g_selected_page !== page_id) {
		clear_selection();
	}
	g_selected_page = page_id;
	menu_sort(page_id);
}

// Adds a canvas to the first selected item that does not have a canvas.
// If there are no selected items or all of them have canvasses,
// a new empty item will be created with a canvas.
function action_draw(page_id) {
	// Make a blank image
	let temp_canvas = document.createElement('canvas');
	temp_canvas.style.visibility = 'hidden'; // make it invisible
	temp_canvas.style.position = 'fixed'; // make it way off the screen
	temp_canvas.style.right = -100000;
	document.body.appendChild(temp_canvas); // canvasses don't seem to work properly until they are added to the HTML DOM
	temp_canvas.style.width = '800px';
	temp_canvas.style.height = '800px';
	temp_canvas.width = temp_canvas.clientWidth;
	temp_canvas.height = temp_canvas.clientHeight;

	// Draw a grid
	let ctx = temp_canvas.getContext('2d');
	ctx.fillStyle = '#080820';
	ctx.fillRect(0, 0, temp_canvas.width, temp_canvas.height);
	ctx.strokeStyle = '#404080';
	for (let x = 0; x < temp_canvas.width; x += 20) {
		ctx.beginPath();
		ctx.moveTo(x, 0);
		ctx.lineTo(x, temp_canvas.height);
		ctx.stroke();
	}
	for (let y = 0; y < temp_canvas.height; y += 20) {
		ctx.beginPath();
		ctx.moveTo(0, y);
		ctx.lineTo(temp_canvas.width, y);
		ctx.stroke();
	}

	// Convert to a blob
	let data_url = temp_canvas.toDataURL('image/png').replace("image/png", "image/octet-stream");
	let blob = data_uri_to_blob(data_url);
	document.body.removeChild(temp_canvas);

	// Make a form
	let formData = new FormData();
  	formData.append("file", blob);
	let now = new Date();
	let date_text = zpad(now.getFullYear(), 2) + zpad(now.getMonth() + 1, 2) + zpad(now.getDate(), 2);
	let time_text = zpad(now.getHours(), 2) + zpad(now.getMinutes(), 2) + zpad(now.getSeconds(), 2);
	let dest_name = `${date_text}_${time_text}_${random_id(4)}_drawing.png`;
	formData.append("dest", dest_name);

	// Send it to the server
	fetch('upload.ajax', {
		method: "POST",
		body: formData
	}).then((response) => {
		console.log(`Upload image response: ${JSON.stringify(response)}`);
	});

	// Add the image somewhere
	before_page_change(page_id);
	let files_added = 0;
	for (let item_index of g_selected_items) {
		if (g_content[page_id].items[item_index].file === undefined) {
			g_content[page_id].items[item_index].file = dest_name;
			files_added++;
			break;
		}
	}
	if (files_added < 1) {
		let new_item = {
			text: '',
			file: dest_name,
		};
		g_content[page_id].items.push(new_item);
	}
	item_operation_done();
}

function auto_submit_camera_form() {
	let btn = document.getElementById('upload_button');
	btn.click();
}

async function upload_file(page_id) {
	// Get the file data
	let fileupload = document.getElementById('take_picture');
    let formData = new FormData();
    formData.append("file", fileupload.files[0]);

	// Generate a destination filename
	let now = new Date();
	let date_text = zpad(now.getFullYear(), 2) + zpad(now.getMonth() + 1, 2) + zpad(now.getDate(), 2);
	let time_text = zpad(now.getHours(), 2) + zpad(now.getMinutes(), 2) + zpad(now.getSeconds(), 2);
	let filename = basename(fileupload.files[0].name);
	let dest_name = `${date_text}_${time_text}_${random_id(4)}_${filename}`;
	formData.append("dest", dest_name);

	// Send it to the server
    let fetch_promise = fetch('upload.ajax', {
      method: "POST",
      body: formData
    });

	// Add it to the item
	before_page_change(page_id);
	let new_item = {
		text: '',
		file: dest_name,
	};
	g_content[page_id].items.push(new_item);
	sync();

	await fetch_promise;
	render_pages(page_id);
}

function action_camera(page_id) {
	close_menu();

	// Make an invisible menu with a "file" input that opens the phone camera program
	let span_id = `action_menu_${page_id}`;
	let item_span = document.getElementById(span_id);
	if (!item_span) {
		throw new Error(`no element with id: ${span_id}`);
	}
	let s = [];
	s.push(`<table width="100%" style="display:none"><tr><td>`);
	s.push('<input type="file" id="take_picture" name="image_filename" accept="image/*;capture=camera" onchange="auto_submit_camera_form();"></input><br>');
	log(`page_id=${page_id}`);
	s.push(`<button id="upload_button" onclick="upload_file('${page_id}')">Upload</button>`);
	s.push(`</td></tr></table>`);
	item_span.innerHTML = s.join('');
	g_menu_div_id = span_id;
	g_menu_callback = bad_menu_state;

	// Automatically push the button to "choose a file", which opens the camera program
	document.getElementById("take_picture").click();
}

function on_paste_text() {
	const text = document.getElementById('paste_area').value;
	g_menu_callback(text);
}

function action_new_item(page_id) {
	let new_area_id = `new_area_${page_id}`;
	if (!document.getElementById(new_area_id)) {
		return;
	}
	close_menu();

	// Set up to refresh the item when this menu is closed (whether accepted or canceled)
	g_close_ops.push(() => {
		g_editing_index = -3;
		render_pages(page_id);
	});
	g_editing_page = page_id;
	g_editing_index = g_content[page_id].items.length;
	add_edit_text_menu(new_area_id, true, '', function(new_text) {
		// This method is only called when the new text is accepted
		before_page_change(page_id)
		let new_item = {
			text: new_text,
		};
		g_content[page_id].items.push(new_item);
		sync();
		setTimeout(() => { action_new_item(page_id) }, 0); // Keep this menu open so the user can keep adding new items
	});
}
/*
function action_new_link(page_id) {
	if (g_selected_items.length > 1) {
		alert('Sorry, this option is not supported for multi-select');
		return;
	}
	let selected_item = g_selected_items.length > 0 ? g_selected_items[0] : -1;

	// If there's already a link, let's remove it
	if (selected_item >= 0 && g_selected_page === page_id && g_content[page_id].items[selected_item].dest) {
		const dest_id = g_content[page_id].items[selected_item].dest;
		if (g_content[dest_id]) {
			delete g_content[dest_id]._parents;
		}
		delete g_content[page_id].items[selected_item].dest;
		return;
	}

	// Search for a page to link to
	menu_search(page_id, `new_area_${page_id}`, false, false, function(dest_page_id) {
		if (dest_page_id === page_id) {
			alert('Linking to the page you are already on would be confusing! Let\'s not do that.');
			return;
		}
		before_page_change(page_id);
		let item;
		if (selected_item >= 0 && g_selected_page === page_id) {
			item = g_content[page_id].items[selected_item];
		} else {
			const abbr = abbreviate(g_content[dest_page_id].thought);
			item = {
				text: abbr,
			};
			g_content[page_id].items.push(item);
		}
		item.dest = dest_page_id;
		delete g_content[dest_page_id]._parents;
		item_operation_done();
	});
}
*/
function action_new_page(page_id) {
	if (g_selected_items.length > 1) {
		alert('Sorry, this option is not supported for multi-select');
		return;
	}
	let selected_item = g_selected_items.length > 0 ? g_selected_items[0] : -1;
	if (selected_item >= 0 && g_selected_page === page_id && g_content[page_id].items[selected_item].dest) {
		alert('The selected item already links to a page.');
		return;
	}
	before_page_change(page_id)
	let thought_text = '';
	let item;
	if (selected_item >= 0) {
		item = g_content[page_id].items[selected_item];
		thought_text = item.text;
	} else {
		item = {
			text: 'new page',
		};
		g_content[page_id].items.push(item);
	}

	// Make a new page
	let new_id = make_unique_id();
	before_page_change(new_id)
	item.dest = new_id;
	g_content[new_id] = { thought: thought_text, items: [] };

	// Mark changes and view the new page
	render_pages(new_id);
	sync();
}

function find_orphans() {
	let set = new Set();
	set.add('start');
	for (let page_id in g_content) {
		if (g_content[page_id]) {
			for (let item of g_content[page_id].items) {
				if (item.dest) {
					set.add(item.dest);
				}
			}
		} else {
			log(`page ${page_id} seems to be explicitly undefined`);
		}
	}
	let results = [];
	for (let page_id in g_content) {
		if (!(set.has(page_id))) {
			results.push(page_id);
		}
	}
	return results;
}

function collect_garbage() {
	let orphans = find_orphans();
	if (orphans.length < 1) {
		alert('No orphan pages to delete');
		log('No orphan pages to delete');
		return;
	}
	let text = `Are you sure you want to delete these ${orphans.length} pages? (`;
	log(`orphan_ids=${JSON.stringify(orphans)}`);
	let comma = false;
	for (const orphan of orphans) {
		if (comma) {
			text += ', ';
		} else {
			comma = true;
		}
		if (g_content[orphan]) {
			text += abbreviate(g_content[orphan].thought);
		} else {
			text += 'untitled';
		}
	}
	text += ')';
	menu_confirm(text, `title_bar_menu`, function(val) {
		for (const page_id of orphans) {
			before_page_change(page_id);
			delete g_content[page_id];
		}
		sync();
		close_menu();
	});
}

function save_raw_changes() {
	let ta = document.getElementById('raw_content');
	let raw = ta.value;

	// Check that there are not duplicate page ids
	let dupe_range = check_json_for_dupes(raw);
	if (dupe_range.length > 0) {
		alert('A duplicate key was found!');
		ta.focus();
		ta.setSelectionRange(dupe_range[0], dupe_range[1]);
		return;
	}

	// Check that every page has a thought
	let new_content = JSON.parse(raw);
	for (let page_id in new_content) {
		if (!new_content[page_id].thought) {
			alert(`Page ${page_id} has no thought.`);
			let pos = raw.indexOf(page_id);
			if (pos >= 0) {
				ta.focus();
				ta.setSelectionRange(pos, pos + page_id.length);
			}
			return;
		}
	}

	// Check that every dest has a valid page
	for (let page_id in new_content) {
		for (let item of new_content[page_id].items) {
			if (item.dest && !new_content[item.dest]) {
				alert(`${item.dest} is not a valid destination page`);
				let pos = raw.indexOf(item.dest);
				if (pos >= 0) {
					ta.focus();
					ta.setSelectionRange(pos, pos + item.dest.length);
				}
				return;
			}
		}
	}

	// Check for a start page
	if (new_content.start === 0) {
		alert('There is no "start" page.')
		return;
	}

	// Save all pages
	let page_id_set = new Set();
	for (let page_id of Object.keys(g_content)) {
		page_id_set.add(page_id);
		before_page_change(page_id);
	}
	for (let page_id of Object.keys(new_content)) {
		if (!page_id_set.has(page_id)) {
			before_page_change(page_id);
		}
	}
	g_content = new_content;

	render_pages('start');
}

function make_raw_content_page() {
	for (const page of Object.keys(g_content)) {
		delete g_content[page]._parents;
	}
	let s = [];
	s.push(`&nbsp;<button onclick="save_raw_changes()">Save changes</button>`);
	s.push(`&nbsp;<button onclick="render_pages('start')">Cancel</button>`);
	//s.push(`<a class="link" href="#javascript:void(0)" onclick="render_pages('start'); return false;">Return to start page</a>`);
	s.push(`<textarea id="raw_content" style="width:99%; height:900px">${JSON.stringify(g_content, null, 2)}</textarea>`);
	let the_content = document.getElementById('the_content');
	the_content.innerHTML = s.join('');
}

