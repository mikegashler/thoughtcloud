function make_close_button(s) {
	s.push('<span class="tool">❌<span class="tooltip">Close</span></span>');
}

function make_list_bottom_buttons(s, page_id) {
    s.push(` <button onclick="action_new_item('${page_id}')"><span class="tool">&nbsp;+&nbsp;<span class="tooltip">Add item</span></span></button>`);
    s.push(` <button onclick="action_new_page('${page_id}')"><span class="tool">📄<span class="tooltip">New page</span></span></button>`);
    s.push(` <button onclick="menu_page_view('${page_id}')"><span class="tool">🎛️<span class="tooltip">Change page view</span></span></button>`);
    s.push(` <button onclick="action_date('${page_id}')"><span class="tool">📅<span class="tooltip">Add date</span></span></button>`);
    //s.push(` <button onclick="action_new_link('${page_id}')"><span class="tool">🔗<span class="tooltip">Link to an existing page</span></span></button>`);
    s.push(` <button onclick="action_sort('${page_id}')"><span class="tool">🗂️<span class="tooltip">Sort</span></span></button>`);
    s.push(` <button onclick="action_draw('${page_id}')"><span class="tool">🎨<span class="tooltip">Add canvas</span></span></button>`);
    s.push(` <button onclick="action_camera('${page_id}')"><span class="tool">📷<span class="tooltip">Add photo</span></span></button>`);
    s.push(` <button onclick="action_help('${page_id}')"><span class="tool">?<span class="tooltip">Help</span></span></button>`);
}

function make_grid_bottom_buttons(s, page_id) {
    s.push(` <button onclick="grid_new_row('${page_id}')"><span class="tool">&nbsp;+&nbsp;<span class="tooltip">Add row</span></span></button>`);
    s.push(` <button onclick="menu_page_view('${page_id}')"><span class="tool">🎛️<span class="tooltip">Change page view</span></span></button>`);
    s.push(` <button onclick="menu_footers('${page_id}')"><span class="tool">📈<span class="tooltip">Add a footer</span></span></button>`);
    s.push(` <button onclick="action_sort('${page_id}')"><span class="tool">🗂️<span class="tooltip">Sort</span></span></button>`);
    s.push(` <button onclick="action_help('${page_id}')"><span class="tool">?<span class="tooltip">Help</span></span></button>`);
}

function make_tile_bottom_buttons(s, page_id) {
    s.push(` <button onclick="action_new_page('${page_id}')"><span class="tool">📄<span class="tooltip">New page</span></span></button>`);
    s.push(` <button onclick="menu_page_view('${page_id}')"><span class="tool">🎛️<span class="tooltip">Change page view</span></span></button>`);
    s.push(` <button onclick="action_sort('${page_id}')"><span class="tool">🗂️<span class="tooltip">Sort</span></span></button>`);
    s.push(` <button onclick="action_help('${page_id}')"><span class="tool">?<span class="tooltip">Help</span></span></button>`);
}

function make_pick_bottom_buttons(s, page_id) {
    s.push(` <button onclick="action_new_item('${page_id}')"><span class="tool">&nbsp;+&nbsp;<span class="tooltip">Add item</span></span></button>`);
    s.push(` <button onclick="menu_page_view('${page_id}')"><span class="tool">🎛️<span class="tooltip">Change page view</span></span></button>`);
    s.push(` <button onclick="action_help('${page_id}')"><span class="tool">?<span class="tooltip">Help</span></span></button>`);
}

function make_regs_bottom_buttons(s, page_id) {
    s.push(` <button onclick="add_recurring_task('${page_id}')"><span class="tool">&nbsp;+&nbsp;<span class="tooltip">Add recurring task</span></span></button>`);
    s.push(` <button onclick="menu_page_view('${page_id}')"><span class="tool">🎛️<span class="tooltip">Change page view</span></span></button>`);
    s.push(` <button onclick="action_help('${page_id}')"><span class="tool">?<span class="tooltip">Help</span></span></button>`);
}

function make_checks_bottom_buttons(s, page_id) {
    s.push(` <button onclick="menu_page_view('${page_id}')"><span class="tool">🎛️<span class="tooltip">Change page view</span></span></button>`);
    s.push(` <button onclick="action_help('${page_id}')"><span class="tool">?<span class="tooltip">Help</span></span></button>`);
}

function reset_primary_buttons(page_id) {
	let pb = document.getElementById('primary_buttons');
	let s = [];
    s.push(` <button onclick="toggle_dark_mode()">${g_account.dark_mode ? "🌑" : "🌕"}</button>`);
	s.push(` <button onclick="action_search('${page_id}')"><span class="tool">🔎<span class="tooltip">Search</span></span></button>`);
    s.push(` <button onclick="global_undo()"><span class="tool">↩<span class="tooltip">Undo</span></span></button></td>`);
    s.push(` <button onclick="global_redo()"><span class="tool">↪<span class="tooltip">Redo</span></span></button></td>`);
	s.push(` <button onclick="show_advanced_buttons()"><span class="tool">⚙️<span class="tooltip">Advanced</span></span></button>`);
	s.push(` <button onclick="log_out()"><span class="tool">🪵<span class="tooltip">Log out</span></span></button>`);
	pb.innerHTML = s.join('');

	let notif = document.getElementById('notification_area');
	s = [];
    s.push(` <button onclick="alert('Sorry, not yet implemented. This feature is coming soon.');"><span class="notify_bell">🔔<span class="notify_count">2</span></span></button>`);
	notif.innerHTML = s.join('');
}

function make_logo(s) {
	s.push('&nbsp;&nbsp;<font style="font-size:72px;"><span style="cursor: pointer;" id="logo_span" onclick="toggle_debug_mode()">☁️</span></font>&nbsp;&nbsp;');
}

function make_canvas_buttons(s, view_index)
{
    s.push(`  <td valign="bottom"><button id="image_edit_${view_index}_tool_0" onclick="on_select_tool(${view_index}, 0)">💉</button></td>`);
    s.push(`  <td valign="bottom"><button id="image_edit_${view_index}_tool_1" onclick="on_select_tool(${view_index}, 1)">~</button></td>`);
    s.push(`  <td valign="bottom"><button id="image_edit_${view_index}_tool_2" onclick="on_select_tool(${view_index}, 2)">⁄</button></td>`);
    s.push(`  <td valign="bottom"><button id="image_edit_${view_index}_tool_3" onclick="on_select_tool(${view_index}, 3)">⬭</button></td>`);
    s.push(`  <td align="center"><label id="color_value_${view_index}" for="color_picker_${view_index}">#00a0a0</label><br>`);
    s.push(`    <input id="color_picker_${view_index}" type="color" onchange="on_color_picker_change(${view_index})" value="#00a0a0"></td>`);
    s.push(`  <td align="center"><label id="brush_size_${view_index}" for="brush_slider">8</label><br>`);
    s.push(`    <input id="brush_slider_${view_index}" type="range" min="0" max="1" value="0.3" step="0.1" onchange="on_brush_slider_change(${view_index})"></td>`);
    s.push(`  <td valign="bottom"><button id="save_button_${view_index}" onclick="on_save_canvas_changes(${view_index})">💾</button></td>`);
    s.push(`  <td valign="bottom"><button onclick="canvas_undo(${view_index})">↩</button></td>`);
    s.push(`  <td valign="bottom"><button onclick="canvas_redo(${view_index})">↪</button></td>`);
    //s.push('<td>-I+<br>JKL</td>');
}

function show_advanced_buttons()
{
	if (g_menu_div_id === `title_bar_menu` && g_menu_callback === bad_menu_state) {
		close_menu();
		return;
	}
	close_menu();

	// Make the menu
	g_menu_div_id = `title_bar_menu`;
	g_menu_callback = bad_menu_state;
	const s = [];
	s.push(` <button onclick="collect_garbage()">🧹</button>`);
	s.push(` <button onclick='make_raw_content_page()')">🩻</button>`);
	s.push(` <button onclick="make_backup()">💾</button>`);
	s.push('<br>');
	let menu_div = document.getElementById(g_menu_div_id);
    menu_div.innerHTML = s.join('');
}

function make_item_bullet(s, item, counters) {
    if (item.icon) {
        if (item.icon === '#') {
            for (let j = 0; j < counters.length; j++) {
                s.push((j === 0 ? '' : '.') + counters[j]);
            }
        } else {
            s.push(item.icon);
        }
    } else {
		if (g_account.dark_mode)
			s.push('<img class="item_bullet icon" src="marble.png">');
		else
			s.push('<img class="item_bullet icon" src="marble_light.png">');
    }
}

// Shows the item buttons if at least one item is selected.
// Hides the item buttons if no items are selected.
function show_item_buttons(page_id)
{
	close_menu();
	if (g_selected_items.length < 1) {
		return; // nothing is selected, so let's actually not show the item buttons
	}

	// Make sure only items on this page are selected
	if (g_selected_page !== page_id) {
		clear_selection();
	}
	g_selected_page = page_id;

	// Make the menu
	g_menu_div_id = `title_bar_menu`;
	g_menu_callback = (op) => { do_item_action(page_id, op); }
	const s = [];
	s.push(` <button onclick="on_click_item_action('bullet')">●</button>`);
	s.push(` <button onclick="on_click_item_action('num')">#</button>`);
	s.push(` <button onclick="on_click_item_action('check')">✅</button>`);
	if (g_selected_items.length === 1 && g_content[page_id].items[g_selected_items[0]].dest) {
		s.push(` <button onclick="on_click_item_action('edit')">✍️</button>`);
	}
	s.push(` <button onclick="on_click_item_action('left')">⬅️</button>`);
	s.push(` <button onclick="on_click_item_action('right')">➡️</button>`);
	s.push(` <button onclick="on_click_item_action('del')">🗑</button>`);
	s.push(` <button onclick="on_click_item_action('clone')">👯</button>`);
	s.push(` <button onclick="on_click_item_action('move')">🚚</button>`);
	if (g_selected_items.length === 1) {
		if (g_content[page_id].items[g_selected_items[0]].text.search('\n') >= 0) {
			s.push(` <button onclick="on_click_item_action('split')">🔪</button>`);
		}
	}
	if (g_selected_items.length > 1) {
		s.push(` <button onclick="on_click_item_action('merge')">🪢</button>`);
	}
	/*if (g_content[page_id].free) {
		s.push(` <button onclick="action_bind('${page_id}')">🔒</button>`);
	} else {
		s.push(` <button onclick="action_unbind('${page_id}')">🔓</button>`);
	}
	s.push(` <button onclick="action_copy('${page_id}')">⎘</button>`);
	s.push(` <button onclick="action_paste('${page_id}')">📋</button>`);*/
	s.push(' <button onclick="close_menu()">Close</button>');
	s.push('<br>');
	let menu_div = document.getElementById(g_menu_div_id);
    menu_div.innerHTML = s.join('');
}

function make_grid_primary_buttons(s, view_index) {
	let view_grid = g_account.view_pages[view_index];
	s.push(` <button onclick="on_grid_copy(${view_index})">▸📋</button>`);
	s.push(` <button onclick="on_grid_paste(${view_index})">📋▸</button>`);
	if (view_grid.sel_end_row - view_grid.sel_start_row === 1)
		s.push(` <button onclick="on_grid_edit_row(${view_index})"><span class="tool">✏️<span class="tooltip">Edit row</span></span></button>`);
	s.push(` <button onclick="on_grid_add_row(${view_index})"><span class="tool">+⎶<span class="tooltip">Add row</span></span></button>`);
	s.push(` <button onclick="on_grid_del_row(${view_index})"><span class="tool">-⎶<span class="tooltip">Delete row</span></span></button>`);
	s.push(` <button onclick="on_grid_add_col(${view_index})"><span class="tool">+][<span class="tooltip">Add column</span></span></button>`);
	s.push(` <button onclick="on_grid_del_col(${view_index})"><span class="tool">-][<span class="tooltip">Delete column</span></span></button>`);
	s.push(` <button onclick="on_grid_row_up(${view_index})"><span class="tool">⍏<span class="tooltip">Move row up</span></span></button>`);
	s.push(` <button onclick="on_grid_row_down(${view_index})"><span class="tool">⍖<span class="tooltip">Move row down</span></span></button>`);
	s.push(` <button onclick="on_grid_col_left(${view_index})"><span class="tool">⍅<span class="tooltip">Move column left</span></span></button>`);
	s.push(` <button onclick="on_grid_col_right(${view_index})"><span class="tool">⍆<span class="tooltip">Move column right</span></span></button>`);
}

function make_regs_item_form(s, page_id, index, cycle_days, show_completion_button) {
	if (show_completion_button)
		s.push(`<button onclick="did_recurring_task('${page_id}', ${index})">✅</button>`);
	s.push(`<button onclick="regs_toggle_dets('${page_id}', ${index})">⋮</button>`);
	s.push(`<button onclick="delete_recurring_task('${page_id}', ${index})">🗑</button>`);
	s.push(`<div id="regs_details_${page_id}_${index}" style="display: none;">`);
	s.push(`Completed <input id="days_ago_${page_id}_${index}" type="text" size="3" value="0"> days ago.<br>`);
	s.push(`Do every <input id="cycle_len_${page_id}_${index}" type="text" size="3" value="${cycle_days}"> days.<br>`);
	s.push(`Push back <input id="push_back_${page_id}_${index}" type="text" size="3" value="1"> days.`);
	s.push(`<button onclick="push_back_recurring_task('${page_id}', ${index})">🫸</button>`);
	s.push(`</div>`);
}

function update_network_status_icon(changed_page_count, time_since_last_network_response) {
	let logo_span = document.getElementById('logo_span');
	if (changed_page_count === 0) {
		if (time_since_last_network_response < 20_000) {
			logo_span.innerHTML = '☁️'; // The network is working and everything is sync'd up
		} else {
			logo_span.innerHTML = '💨'; // The network appears to be down (but there are no changes)
		}
	} else {
		if (time_since_last_network_response < 20_000) {
			logo_span.innerHTML = '🌧️'; // Recent changes are pending sync
		} else if (time_since_last_network_response < 40_000) {
			logo_span.innerHTML = '⛈️'; // Struggling to get some changes through
		} else {
			logo_span.innerHTML = '🌪️'; // Changes are getting lost! ⚡️🚨
			log('Network failure. No update from the server in a long time');
		}
	}
}

function action_help(page_id) {
	if (g_menu_div_id !== '') {
		close_menu();
		return;
	}
	close_menu();
	g_menu_div_id = `action_menu_${page_id}`;
	const s = [];
	s.push('<div class="modal">');
	s.push('<table>');
	s.push(`<tr><td valign="top" colspan="2"><big><b>Lower buttons</b></big></td></tr>`);
	s.push(`<tr><td valign="top"><button onclick="action_new_item('${page_id}')">&nbsp;+&nbsp;</button></td><td>Add a new item to this page</td></tr>`);
	s.push(`<tr><td valign="top"><button onclick="action_new_page('${page_id}')">📄</button></td><td>Make a new page</td></tr>`);
	s.push(`<tr><td valign="top"><button onclick="action_date('${page_id}')">📅</button></td><td>Add an item with today\'s date</td></tr>`);
	s.push(`<tr><td valign="top"><button onclick="menu_footers('${page_id}')">📈</button></td><td>Edit page footers</td></tr>`);
	//s.push(`<tr><td valign="top"><button onclick="action_new_link('${page_id}')">🔗</button></td><td>Link to an existing page</td></tr>`);
	s.push(`<tr><td valign="top"><button onclick="action_sort('${page_id}')">🗂️</button></td><td>Sort the items on this page</td></tr>`);
	s.push(`<tr><td valign="top"><button onclick="action_draw('${page_id}')">🎨</button></td><td>Add a drawing canvas</td></tr>`);
	s.push('<tr><td valign="top">?&nbsp;&nbsp;&nbsp;</td><td>See these descriptions</td></tr>');

	s.push(`<tr><td valign="top" colspan="2"><big><b>Upper buttons</b></big></td></tr>`);
	s.push(`<tr><td valign="top"><button onclick="toggle_dark_mode()">🌕 / 🌑</button></td><td>Toggle between light mode and dark mode</td></tr>`);
	s.push(`<tr><td valign="top"><button onclick="action_search('start')">🔎</button></td><td>Search</td></tr>`);
	s.push(`<tr><td valign="top"><button onclick="show_advanced_buttons()">⚙️</button></td><td>Show advanced buttons</td></tr>`);
	s.push(`<tr><td valign="top"><button onclick="log_out()">🪵</button></td><td>Log out</td></tr>`);

	s.push(`<tr><td valign="top" colspan="2"><big><b>Item buttons</b></big></td></tr>`);
	s.push(`<tr><td valign="top"><button onclick="do_item_action(page_id, 'bullet')">●</button></td><td>Change to bullet icon</td></tr>`);
	s.push(`<tr><td valign="top"><button onclick="do_item_action(page_id, 'num')">#</button></td><td>Change to item number icon</td></tr>`);
	s.push(`<tr><td valign="top"><button onclick="do_item_action(page_id, 'check')">✅</button></td><td>Change to check box icon</td></tr>`);
	s.push(`<tr><td valign="top"><button onclick="do_item_action(page_id, 'edit')">✍️</button></td><td>Edit the item text</td></tr>`);
	s.push(`<tr><td valign="top"><button onclick="do_item_action(page_id, 'right')">➡️</button></td><td>Indent selected items</td></tr>`);
	s.push(`<tr><td valign="top"><button onclick="do_item_action(page_id, 'left')">⬅️</button></td><td>Unindent selected items</td></tr>`);
	s.push(`<tr><td valign="top"><button onclick="do_item_action(page_id, 'del')">🗑</button></td><td>Delete selected items</td></tr>`);
	s.push(`<tr><td valign="top"><button onclick="do_item_action(page_id, 'clone')">👯</button></td><td>Clone the selected item</td></tr>`);
	s.push(`<tr><td valign="top"><button onclick="do_item_action(page_id, 'move')">🚚</button></td><td>Move selected items to another page</td></tr>`);
	s.push(`<tr><td valign="top"><button onclick="do_item_action(page_id, 'split')">🔪</button></td><td>Cut a multi-line item into multiple items</td></tr>`);
	s.push(`<tr><td valign="top"><button onclick="do_item_action(page_id, 'merge')">🪢</button></td><td>Tie multiple items into one multi-line item</td></tr>`);
	//s.push(`<tr><td valign="top"><button onclick="action_bind(page_id, 'bind')">🔒</button></td><td>Lock the text of incoming links to the thought text</td></tr>`);
	//s.push(`<tr><td valign="top"><button onclick="action_unbind(page_id, 'free')">🔓</button></td><td>Unlock incoming links from thought text</td></tr>`);
	//s.push(`<tr><td valign="top"><button onclick="action_copy('${page_id}')">⎘</button></td><td>Copy the page to the clipboard</td></tr>`);
	//s.push(`<tr><td valign="top"><button onclick="action_paste('${page_id}')">📋</button></td><td>Paste the page content</td></tr>`);

	s.push(`<tr><td valign="top" colspan="2"><big><b>Gear buttons</b></big></td></tr>`);
	s.push(`<tr><td valign="top"><button onclick="collect_garbage()">🧹</button></td><td>Clean up the orphan pages</td></tr>`);
	s.push(`<tr><td valign="top"><button onclick="make_raw_content_page()">🩻</button></td><td>View raw source</td></tr>`);
	s.push(`<tr><td valign="top"><button onclick="make_backup()">💾</button></td><td>Download a backup copy of all your content</td></tr>`);

	s.push('</table>');
	s.push('<button onclick="close_menu()">Close</button>');
	s.push('</div><br>');
	let menu_div = document.getElementById(g_menu_div_id);
    menu_div.innerHTML = s.join('');
}

