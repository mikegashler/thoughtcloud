// ----------------------------------------------------------------
// The contents of this file are dedicated to the public domain as
// described at http://creativecommons.org/publicdomain/zero/1.0/.
// ----------------------------------------------------------------

function draw_dot(ctx, x, y, radius, color) {
  ctx.fillStyle = color;
  ctx.beginPath();
  ctx.arc(x, y, radius, 0, 2 * Math.PI);
  ctx.fill();
}

function draw_line(ctx, x1, y1, x2, y2, thickness, color) {
  ctx.lineCap = "round";
  ctx.strokeStyle = color;
  ctx.lineWidth = thickness;
  ctx.beginPath();
  ctx.moveTo(x1, y1);
  ctx.lineTo(x2, y2);
  ctx.stroke();
}

function draw_ellipse(ctx, x1, y1, x2, y2, thickness, color) {
  let w = (x2 - x1) / 2;
  let h = (y2 - y1) / 2;

  // Adjust values so the ellipse will pass through (x1,y1) and (x2,y2)
  x1 = x1 + w - Math.sqrt(2 * w * w);
  y1 = y1 + h - Math.sqrt(2 * h * h);
  w = Math.sqrt(2 * w * w);
  h = Math.sqrt(2 * h * h);

  // Compute control points
  let k = .5522848;
  let ox = w * k;
  let oy = h * k;
  let xe = x1 + w + w;
  let ye = y1 + h + h;
  let xm = x1 + w;
  let ym = y1 + h;

  // Draw the curves
  ctx.strokeStyle = color;
  ctx.lineWidth = thickness;
  ctx.beginPath();
  ctx.moveTo(x1, ym);
  ctx.bezierCurveTo(x1, ym - oy, xm - ox, y1, xm, y1);
  ctx.bezierCurveTo(xm + ox, y1, xe, ym - oy, xe, ym);
  ctx.bezierCurveTo(xe, ym + oy, xm + ox, ye, xm, ye);
  ctx.bezierCurveTo(xm - ox, ye, x1, ym + oy, x1, ym);
  ctx.stroke();
}

// Rounds pos to the integer value of step
function snap(pos, step) {
  return Math.floor(pos / step + (step / 10.)) * step;
}

// Finds a starting point and step size for tick marks from min_val to max_val.
// start will be some integer value. Ticks should be drawn like below:
//
// for (let i = start; i * step <= max_val; i++) {
//    if (i % base === 0)
//       draw power-of-10 (usually bold) tick
//    else
//       draw intermediate (usually gray) tick
// }
function find_tick_spacing(max_ticks, min_val, max_val) {
  let min_step = (max_val - min_val) / max_ticks;
  let n = Math.ceil(Math.log(min_step) / Math.log(10));
  let step = Math.pow(10, n);
  let base = 1;
  if (step / 5 >= min_step) {
    step = step / 5;
    base = 5;
  } else if (step / 2 >= min_step) {
    step = step / 2;
    base = 2;
  }
  let start = Math.ceil(min_val / step);
  return [step, start, base];
}

// Plots things on a canvas in mathy coordinates
class Plotter {
	constructor(canvas, xmin, ymin, xmax, ymax) {
		this.canvas = canvas;
		this.xmin = xmin;
		this.ymin = ymin;
		this.xmax = xmax;
		this.ymax = ymax;
	}

	to_x(x) {
		return (x - this.xmin) * this.canvas.width / (this.xmax - this.xmin);
	}

	to_y(y) {
		return this.canvas.height - 1 - ((y - this.ymin) * this.canvas.height / (this.ymax - this.ymin));
	}

	// Consumes an n-by-2 table of (x,y) values.
	// Draws a line connecting them in the order given.
	plot_line(points, color='#000000') {
		let ctx = this.canvas.getContext("2d");
    ctx.strokeStyle = color;
		ctx.beginPath();
		ctx.moveTo(this.to_x(points[0][0]), this.to_y(points[0][1]));
		for (let i = 1; i < points.length; i++) {
	        ctx.lineTo(this.to_x(points[i][0]), this.to_y(points[i][1]));
		}
		ctx.stroke();
	}

	// Consumes an n-by-3 table of (x,y,label) values.
	// Draws the labels at the specified locations.
	plot_labels(labels) {
		let ctx = this.canvas.getContext("2d");
		ctx.font = '24px sans';
		for (let i = 0; i < labels.length; i++) {
			const x = this.to_x(labels[i][0]);
			const y = this.to_y(labels[i][1]);
			ctx.save();
			ctx.translate(x, y);
			ctx.rotate(Math.PI / 2);
			if (y < this.canvas.height / 2) {
				ctx.textAlign = 'start';
			} else {
				ctx.textAlign = 'end';
			}
			ctx.beginPath();
			ctx.fillStyle = '#008040';
			ctx.arc(0, 0, 3/*radius*/, 0, 2 * Math.PI, false);
			ctx.stroke();
			ctx.fillStyle = '#000000';
			ctx.fillText(' ' + labels[i][2] + ' ', 0, 0);
			ctx.restore();
		}
	}

  // Fills a rectange from the coordinates of any two opposing corners
  fill_rect(x1, y1, x2, y2, color) {
    let ctx = this.canvas.getContext("2d");
    ctx.fillStyle = color;
    let xx1 = this.to_x(x1);
    let yy1 = this.to_y(y1);
    let xx2 = this.to_x(x2);
    let yy2 = this.to_y(y2);
    ctx.fillRect(Math.min(xx1, xx2), Math.min(yy1, yy2), Math.abs(xx2 - xx1), Math.abs(yy2 - yy1));
  }

  // Draws the vertical lines that indicate positions along the horizontal axis
  horiz_ticks(max_ticks, draw_labels) {
    const [step, start, base] = find_tick_spacing(max_ticks, this.xmin, this.xmax);
    let ctx = this.canvas.getContext("2d");
    for (let i = start; i * step <= this.xmax; i++) {
      let color = (i % base === 0) ? '#909090' : '#b0b0b0';
      ctx.strokeStyle = color;
      ctx.beginPath();
      let x = this.to_x(i * step);
      ctx.moveTo(x, this.to_y(this.ymin));
      ctx.lineTo(x, this.to_y(this.ymax));
      ctx.stroke();
      if (draw_labels) {
        ctx.font = '16px sans';
        ctx.save();
        ctx.translate(x, this.to_y(this.ymin));
        ctx.rotate(-Math.PI / 2);
        ctx.textAlign = 'start';
        ctx.fillStyle = color;
        ctx.fillText(' ' + (i * step) + ' ', 0, 0);
        ctx.restore();
      }
    }
  }

  // Draws the horizontal lines that indicate positions along the vertical axis
  vert_ticks(max_ticks, draw_labels) {
    const [step, start, base] = find_tick_spacing(max_ticks, this.ymin, this.ymax);
log(`step=${step}, start=${start}, base=${base}`);
    let ctx = this.canvas.getContext("2d");
    for (let i = start; i * step <= this.ymax; i++) {
      let color = (i % base === 0) ? '#909090' : '#b0b0b0';
      ctx.strokeStyle = color;
      ctx.beginPath();
      let y = this.to_y(i * step);
      ctx.moveTo(this.to_x(this.xmin), y);
      ctx.lineTo(this.to_x(this.xmax), y);
      ctx.stroke();
      if (draw_labels) {
        ctx.font = '16px sans';
        ctx.save();
        ctx.translate(this.to_x(this.xmin), y);
        ctx.textAlign = 'start';
        ctx.fillStyle = color;
        ctx.fillText(' ' + (i * step) + ' ', 0, 0);
        ctx.restore();
      }
    }
  }
}

class ImageEditor
{
  constructor(options)
  {
    this.options = options;
    this.view = options.canvas;
    this.on_change = options.on_change;
    this.on_sample_color = options.on_sample_color;
    this.view_ctx = this.view.getContext('2d');
    this.max_scale = 32;
    this.min_scale = 0.03125;
    this.draw_color = '#000000';
    this.brush_size = 8;
    this.width = 0; // the width of the source image or canvas, not the view
    this.height = 0; // the height of the source image or canvas, not the view
    this.tool = 0;
    this.editable = options.editable;
    this.view_dirty = false;
    this.prev_scale = null;
    this.prev_x = null;
    this.prev_y = null;
    this.mouse_down = false;
    this.mouse_x = 0;
    this.mouse_y = 0;
    this.history = [];
    this.history_pos = 0;
    this.view.width = this.view.clientWidth;
    this.view.height = this.view.clientHeight;

    this._setup_event_listeners();

    // Start animating the view at regular intervals
    requestAnimationFrame(() => this._draw_view());
  };

  // This is not a built-in method.
  // It is called explicitly when the ImageEditor is destroyed.
  destructor() {
    this.view.removeEventListener('touchstart', this.touchstart_handler);
    this.view.removeEventListener('touchend', this.touchend_handler);
    this.view.removeEventListener('touchmove', this.touchmove_handler);
    window.removeEventListener('mousedown', this.mousedown_handler);
    window.removeEventListener('mouseup', this.mouseup_handler);
    window.removeEventListener('mousemove', this.mousemove_handler);
    window.removeEventListener('keydown', this.keydown_handler);
  }

  // Respond to mouse, touch, and keystrokes
  _setup_event_listeners() {
    this.touchstart_handler = (e) => {
      let canvas_rect = this.view.getBoundingClientRect();
      if (e.targetTouches.length == 2) {
        let p1 = e.targetTouches[0];
        let p2 = e.targetTouches[1];
        let center_x = (p1.clientX + p2.clientX) / 2;
        let center_y = (p1.clientY + p2.clientY) / 2;
        this.prev_x = center_x - canvas_rect.left;
        this.prev_y = center_y - canvas_rect.top;
      } else if (e.targetTouches.length == 1) {
        this.mouse_down = true;
        this.prev_x = e.targetTouches[0].clientX - canvas_rect.left;
        this.prev_y = e.targetTouches[0].clientY - canvas_rect.top;
        if (this.tool === 0) {
        } else if (this.tool === 1) {
          this._async_record_history();
        } else if (this.tool === 2) {
          this._async_record_history();
        } else if (this.tool === 3) {
          this._async_record_history();
        }
      }
    };
    this.view.addEventListener('touchstart', this.touchstart_handler);

    this.touchend_handler = (e) => {
      this.prev_scale = null;
      if (this.mouse_down) {
        this.mouse_down = false;
        let canvas_rect = this.view.getBoundingClientRect();
        let x = this.mouse_x;
        let y = this.mouse_y;
        if (this.tool === 0) {
        } else if (this.tool === 1) {
        } else if (this.tool === 2) {
          let [x1, y1] = this._view_to_draw(this.prev_x, this.prev_y);
          let [x2, y2] = this._view_to_draw(x, y);
          draw_line(this.draw_context, x1, y1, x2, y2, this.brush_size, this.draw_color);
          this.view_dirty = true;
        } else if (this.tool === 3) {
          let [x1, y1] = this._view_to_draw(this.prev_x, this.prev_y);
          let [x2, y2] = this._view_to_draw(x, y);
          draw_ellipse(this.draw_context, x1, y1, x2, y2, this.brush_size, this.draw_color);
          this.view_dirty = true;
        }
      }
    };
    this.view.addEventListener('touchend', this.touchend_handler);

    this.touchmove_handler = (e) => {
      e.preventDefault();
      let canvas_rect = this.view.getBoundingClientRect();        
      if (e.targetTouches.length == 2) {
        let p1 = e.targetTouches[0];
        let p2 = e.targetTouches[1];

        // Scale
        let t = Math.sqrt(Math.pow(p2.clientX - p1.clientX, 2) + Math.pow(p2.clientY - p1.clientY, 2));
        if (this.prev_scale) {
          this._adjust_scale((t - this.prev_scale) / 250);
        }
        this.prev_scale = t;

        // Move
        let center_x = (p1.clientX + p2.clientX) / 2;
        let center_y = (p1.clientY + p2.clientY) / 2;
        let x = center_x - canvas_rect.left;
        let y = center_y - canvas_rect.top;
        this._set_pos(x, y);
      } else if (e.targetTouches.length == 1) {
        let x = e.targetTouches[0].clientX - canvas_rect.left;
        let y = e.targetTouches[0].clientY - canvas_rect.top;
        this.mouse_x = x;
        this.mouse_y = y;
        this.view_dirty = true;
        if (this.tool === 0) {
        } else if (this.tool === 1) {
          let [x1, y1] = this._view_to_draw(this.prev_x, this.prev_y);
          let [x2, y2] = this._view_to_draw(x, y);
          draw_line(this.draw_context, x1, y1, x2, y2, this.brush_size, this.draw_color);
          this.prev_x = x;
          this.prev_y = y;   
        } else if (this.tool === 2) {
        } else if (this.tool === 3) {
        }
      }
    };
    this.view.addEventListener('touchmove', this.touchmove_handler);

    this.mousedown_handler = (e) => {
      if (e.target !== this.view)
        return;
      this.mouse_down = true;
      let canvas_rect = this.view.getBoundingClientRect();
      this.prev_x = e.clientX - canvas_rect.left;
      this.prev_y = e.clientY - canvas_rect.top;
      if (this.tool === 0) {
        let [xx, yy] = this._view_to_draw(this.prev_x, this.prev_y);
        const rgba = this.draw_context.getImageData(xx, yy, 1, 1).data;
        let hex = '#' + zpad(((rgba[0] << 16) | (rgba[1] << 8) | rgba[2]).toString(16), 6);
        this.set_color(hex);
        this.on_sample_color(this.options, hex);
      } else if (this.tool === 1) {
        this._async_record_history();
        let [xx, yy] = this._view_to_draw(this.prev_x, this.prev_y);
        draw_dot(this.draw_context, xx, yy, this.brush_size / 2, this.draw_color);
        this.view_dirty = true;
      } else if (this.tool === 2) {
        this._async_record_history();
      } else if (this.tool === 3) {
        this._async_record_history();
      }
    };
    window.addEventListener('mousedown', this.mousedown_handler);
    console.log('registered mousedown');

    this.mouseup_handler = (e) => {
      if (e.target !== this.view || !this.mouse_down) {
        this.mouse_down = false;
        return;
      }
      this.mouse_down = false;
      let canvas_rect = this.view.getBoundingClientRect();
      let x = e.clientX - canvas_rect.left;
      let y = e.clientY - canvas_rect.top;
      if (this.tool === 0) {
      } else if (this.tool === 1) {
      } else if (this.tool === 2) {
        let [x1, y1] = this._view_to_draw(this.prev_x, this.prev_y);
        let [x2, y2] = this._view_to_draw(x, y);
        draw_line(this.draw_context, x1, y1, x2, y2, this.brush_size, this.draw_color);
        this.view_dirty = true;
      } else if (this.tool === 3) {
        let [x1, y1] = this._view_to_draw(this.prev_x, this.prev_y);
        let [x2, y2] = this._view_to_draw(x, y);
        draw_ellipse(this.draw_context, x1, y1, x2, y2, this.brush_size, this.draw_color);
        this.view_dirty = true;
      }
    };
    window.addEventListener('mouseup', this.mouseup_handler);

    this.mousemove_handler = (e) => {
      let canvas_rect = this.view.getBoundingClientRect();
      let x = e.clientX - canvas_rect.left;
      let y = e.clientY - canvas_rect.top;
      this.mouse_x = x;
      this.mouse_y = y;
      this.view_dirty = true;
      if (e.target === this.view && this.mouse_down) {
        if (this.tool === 0) {
        } else if (this.tool === 1) {
          let [x1, y1] = this._view_to_draw(this.prev_x, this.prev_y);
          let [x2, y2] = this._view_to_draw(x, y);
          draw_line(this.draw_context, x1, y1, x2, y2, this.brush_size, this.draw_color);
          this.prev_x = x;
          this.prev_y = y;
        } else if (this.tool === 2) {
        } else if (this.tool === 3) {
        }
      }
      if (x <= 0 || x >= this.view.clientWidth || y <= 0 || y >= this.view.clientHeight) {
        this.mouse_down = false;
      }
    };
    window.addEventListener('mousemove', this.mousemove_handler);

    this.keydown_handler = (e) => {
      let k = e.key;
      if (k === '=' || k === '+' || k === 'o') {
        this._adjust_scale(0.15);
      } else if (k === '-' || k === '_' || k === 'u') {
        this._adjust_scale(-0.15);
      } else if (k === 'i') {
        this._set_pos(this.prev_x ? this.prev_x : 0, this.prev_y ? this.prev_y + 50 : 0);
      } else if (k === 'j') {
        this._set_pos(this.prev_x ? this.prev_x + 50 : 0, this.prev_y ? this.prev_y : 0);
      } else if (k === 'k') {
        this._set_pos(this.prev_x ? this.prev_x : 0, this.prev_y ? this.prev_y - 50 : 0);
      } else if (k === 'l') {
        this._set_pos(this.prev_x ? this.prev_x - 50 : 0, this.prev_y ? this.prev_y : 0);
      }
    };
    window.addEventListener('keydown', this.keydown_handler);
  }

  // Draw the view canvas
  _draw_view() {
    if (this.view_dirty) {
      this.view_dirty = false;
      this.view_ctx.clearRect(0, 0, this.view.width, this.view.height);
      if (this.image) {
        this.view_ctx.drawImage(
          this.image,
          this.pos_x, this.pos_y,
          this.scale_x * this.width,
          this.scale_y * this.height
        );
      } else if (this.draw_canvas) {
        this.view_ctx.drawImage(
          this.draw_canvas,
          this.pos_x, this.pos_y,
          this.scale_x * this.width,
          this.scale_y * this.height
        );
      }

      // Draw tool ghosts
      if (this.tool === 0) {
      } else if (this.tool === 1) {
        if (!this.mouse_down) {
          let ctx = this.view_ctx;
          ctx.strokeStyle = this.draw_color;
          ctx.lineWidth = 1;
          ctx.beginPath();
          ctx.arc(this.mouse_x, this.mouse_y, this.brush_size * this.scale_x * 0.5, 0, 2 * Math.PI);
          ctx.stroke();
        }
      } else if (this.tool === 2) {
        if (this.mouse_down) {
          draw_line(this.view_ctx, this.prev_x, this.prev_y, this.mouse_x, this.mouse_y, this.brush_size * this.scale_x, this.draw_color);
        }
      } else if (this.tool === 3) {
        if (this.mouse_down) {
          draw_ellipse(this.view_ctx, this.prev_x, this.prev_y, this.mouse_x, this.mouse_y, this.brush_size * this.scale_x, this.draw_color);
        }
      }
    }

    // Do it again soon
    requestAnimationFrame(() => this._draw_view());
  }

  // Changes the scale by the specified amount
  _adjust_scale(delta) {
    let current_scale = this.scale_x;
    let new_scale = this.scale_x + delta * this.scale_x;
    let delta_scale = new_scale - current_scale;
    let delta_width = this.width * delta_scale;
    let delta_height = this.height * delta_scale;
    let new_pos_x = this.pos_x - delta_width / 2;
    let new_pos_y = this.pos_y - delta_height / 2;
    if (new_scale > this.max_scale || new_scale < this.min_scale) {
      return;
    }
    this.view_dirty = true;
    this.scale_x = new_scale;
    this.scale_y = new_scale;
    this.pos_x = new_pos_x;
    this.pos_y = new_pos_y;
  }

  // Sets the position of the image
  _set_pos(x, y) {
    if (this.prev_x && this.prev_y) {
      let delta_x = x - this.prev_x;
      let delta_y = y - this.prev_y;
      this.pos_x += delta_x;
      this.pos_y += delta_y;
    }
    this.prev_x = x;
    this.prev_y = y;
    this.view_dirty = true;
  }

  _view_to_draw(x, y) {
    return [(x - this.pos_x) / this.scale_x, (y - this.pos_y) / this.scale_y];
  }

  // Scale the image to fit to the view
  _fit_image_to_view() {
      let scale_ratio = 1;
      if (this.width / this.view.clientWidth > this.height / this.view.clientHeight) {
        scale_ratio = this.view.clientWidth / this.width;
        this.pos_y = (this.view.clientHeight - scale_ratio * this.height) / 2;
      } else {
        scale_ratio = this.view.clientHeight / this.height;
        this.pos_x = (this.view.clientWidth - scale_ratio * this.width) / 2;
      }
      this.scale_x = scale_ratio;
      this.scale_y = scale_ratio;
      this.view_dirty = true;
  }

  _on_image_loaded() {
    // Ensure a valid image was loaded
    if (!this.loading.width || !this.loading.height || this.loading.width <= 0 || this.loading.height <= 0) {
      throw new Error('Loaded image has no dimensions');
    }

    // Indicate the image as loaded
    this.width = this.loading.width;
    this.height = this.loading.height;
    this.image = this.loading;
    this.loading = null;

    // Make the drawing canvas
    if (this.editable) {
      this.enable_editing();
    }

    // Reset the view 
    this.pos_x = 0;
    this.pos_y = 0;
    this.scale_x = 1;
    this.scale_y = 1;
    this.mouse_down = false;
    this.prev_scale = null;
    this.prev_x = null;
    this.prev_y = null;
    this._fit_image_to_view();
  }

  async _async_record_history() {
    this._record_history();
  }

  // This method should be called immediately *before* a change is made to the canvas
  // to establish an undo point.
  _record_history() {
    if (!this.draw_canvas) {
      console.log('Cannot record history. No canvas');
      return;
    }

    // We only encode the present state if it does not already exist.
    // (This way, we won't make redundant copies if we got here by undoing.)
    if (this.history_pos >= this.history.length)
    {
      let data_url = this.draw_canvas.toDataURL('image/png');
      this.history.push({ time: new Date(), data: data_url });
    }

    // Advance the history
    this.history_pos++;

    // Drop future and present entries (because the change we
    // are about to make will cause us to diverge from them).
    while (this.history.length > this.history_pos)
    {
      console.log('trashing divergent futures');
      this.history.pop();
    }
    if (this.on_change)
      this.on_change(this.options, this.history_pos);
  }

  undo() {
    if (this.history_pos === 0) {
      console.log('Cannot undo. Already at beginning of undo history');
      return;
    }

    // Record the present state if it is not already in the history.
    // (Undoing is a type of change, so we must record the history before we do it.)
    if (this.history_pos >= this.history.length) {
      let data_url = this.draw_canvas.toDataURL('image/png');
      this.history.push({ time: new Date(), data: data_url });
    }

    // Undo
    this.history_pos--;
    let data_url = this.history[this.history_pos].data;
    this.load(data_url, this.url());
    if (this.on_change)
      this.on_change(this.options, this.history_pos);
  }

  redo() {
    if (this.history_pos >= this.history.length - 1) {
      console.log('Cannot redo. Already at beginning of end of the history');
      return;
    }
    this.history_pos++;
    let data_url = this.history[this.history_pos].data;
    this.load(data_url, this.url());
    if (this.on_change)
      this.on_change(this.options, this.history_pos);
  }

  // Set the color to draw with
  set_color(color) {
    this.draw_color = color;
  }

  // Set the brush size
  set_brush_size(size) {
    this.brush_size = size;
  }

  set_tool(id) {
    this.tool = id;
  }

  // Make the image editable by converting it to a canvas
  enable_editing() {
    this.editable = true;
    if (!this.image) {
      console.log('enabling editing, but no image yet loaded');
      return; // This method will be called again when an image is loaded
    }

    // Make a canvas if needed
    if (!this.draw_canvas || this.draw_canvas.width != this.width || this.draw_canvas.height != this.height) {
      if (this.draw_canvas) {
        document.body.removeChild(this.draw_canvas);
      }
      this.draw_canvas = document.createElement('canvas');
      this.draw_canvas.style.visibility = 'hidden'; // make it invisible
      this.draw_canvas.style.position = 'fixed'; // make it way off the screen
      this.draw_canvas.style.right = -100000;
      document.body.appendChild(this.draw_canvas); // canvasses don't seem to work properly until they are added to the HTML DOM
      this.draw_canvas.style.width = '' + this.width + 'px';
      this.draw_canvas.style.height = '' + this.height + 'px';
      this.draw_canvas.width = this.draw_canvas.clientWidth;
      this.draw_canvas.height = this.draw_canvas.clientHeight;
    }

    // Draw the image on the canvas
    this.draw_context = this.draw_canvas.getContext("2d");
    this.draw_context.drawImage(this.image, 0, 0, this.width, this.height);
    this.image = null;
  }

  // Get rid of the canvas and keep as an image
  disable_editing() {
    this.editable = false;
    if (!this.draw_canvas) {
      return;
    }
    let data_url = this.draw_canvas.toDataURL('image/png').replace("image/png", "image/octet-stream");
    document.body.removeChild(this.draw_canvas);
    this.load(data_url, this.url());
  }

  // Returns the url to save this image to.
  // (This url was provided when we loaded the image,
  // and may or may not be the same url it was loaded from.)
  url() {
	  return this.save_url;
  }

  // Returns the number of unsaved changes
  change_count() {
    return this.history_pos;
  }

  // Creates a member named this.loading to be a new Image.
  // Sets this.loading.src to the url to load.
  // When the image finishes loading, changes it from this.loading to this.image
  // or this.draw_canvas (if this.editable is true).
  // Only the first parameters is required.
  // It provides the data to be loaded.
  // If the second parameter is provided, that url will be returned instead when the user calls "url()".
  // (This facilitates loading from a big dataUrl but later saving it to some remote location.)
  load(load_url, save_url) {
    let su = save_url ? save_url : load_url;
    if (su.length > 4096)
      throw new Error(`save_url has a size of ${su.length}. It should be a simple web url, rather than a big dataURL`);
    this.loading = new Image();
    this.loading.addEventListener('load', (e) => { this._on_image_loaded(); });
    this.loading.src = load_url;
	  this.save_url = su;
  }

  // Returns a data uri that encodes the image
  save() {
    if (!this.draw_canvas) {
      enable_editing();
    }
    if (!this.draw_canvas) {
      throw new Error('Nothing to save');
    }
    let data_url = this.draw_canvas.toDataURL('image/png'); // todo: is converting jpeg to a png always a good idea?
    this.history = [];
    this.history_pos = 0;
    if (this.on_change)
      this.on_change(this.options, this.history_pos);
    return data_url;
  }
};

function on_color_picker_change(view_index) {
  let color_picker = document.getElementById(`color_picker_${view_index}`);
  document.getElementById(`color_value_${view_index}`).innerHTML = color_picker.value;
  let image_editor = g_account.view_pages[view_index].image_editor;
  image_editor.set_color(color_picker.value);
}

function on_brush_slider_change(view_index) {
  let brush_slider = document.getElementById(`brush_slider_${view_index}`);
  let brush_size = Math.pow(2, brush_slider.value * 10);
  document.getElementById(`brush_size_${view_index}`).innerHTML = brush_size;
  let image_editor = g_account.view_pages[view_index].image_editor;
  image_editor.set_brush_size(brush_size);
}

function on_save_canvas_changes(view_index) {
	// Extract the image data
  let image_editor = g_account.view_pages[view_index].image_editor;
	let data_uri = image_editor.save();
	let blob = data_uri_to_blob(data_uri);

	// Make a form
	let formData = new FormData();
  formData.append("file", blob);
	formData.append("dest", image_editor.url());

	// Send it to the server
	fetch('upload.ajax', {
		method: "POST",
		body: formData
	}).then((response) => {
    // Reload thumbnails
    let images = document.getElementsByTagName('img');
    for (img of images) {
      if (img.src.search(`thumb_${image_editor.url()}`)) {
        img.src = `thumb_${image_editor.url()}?rand=${Math.floor(Math.random() * 1000000)}`;
        console.log('reloading thumbnail');
      }
    }
  });
}

function on_select_tool(view_index, tool_index) {
  let tool_count = 4;
  for (let i = 0; i < tool_count; i++) {
    let button = document.getElementById(`image_edit_${view_index}_tool_${i}`);
    if (i == tool_index) {
      button.style.borderStyle = 'inset';
    } else {
      button.style.borderStyle = 'outset';
    }
  }
  let image_editor = g_account.view_pages[view_index].image_editor;
  image_editor.set_tool(tool_index);
}

function canvas_undo(view_index) {
  let image_editor = g_account.view_pages[view_index].image_editor;
  image_editor.undo();
}

function canvas_redo(view_index) {
  let image_editor = g_account.view_pages[view_index].image_editor;
  image_editor.redo();
}

function on_sample_color(options, hex) {
  let color_picker = document.getElementById(`color_picker_${options.view_index}`);
  color_picker.value = hex;
}

function on_image_editor_change(options, change_count) {
  let save_button = document.getElementById(`save_button_${options.view_index}`);
  save_button.style.borderColor = '#ff0000';
  if (change_count <= 0) {
    save_button.style.borderWidth = '0px';
  } else {
    save_button.style.borderWidth = '3px';
  }
}

function on_close_canvas_page(view_index) {
  let image_editor = g_account.view_pages[view_index].image_editor;
  let page_id = image_editor.options.page_id;
  if (image_editor.change_count() <= 0) {
    close_page(page_id); // page_id is the id of the parent, not the id of the page we are closing
  } else {
    menu_confirm('Are you sure you want to abandon your unsaved changes?',
      `canvas_tool_menu_${view_index}`,
      function(val) {
        close_page(page_id); // page_id is the id of the parent, not the id of the page we are closing
      }
    );
  }
}

class ViewCanvas {
  constructor(s, view_index, url, page_id, item_index, initializers) {
    s.push('<table width="100%"><tr><td>');
    s.push('  <table><tr>');
    make_canvas_buttons(s, view_index);
    s.push('  </tr></table>');
    s.push('</td><td width="60px" valign="top">');
    s.push(`  <span style="cursor: pointer;" onclick="on_close_canvas_page(${view_index})">`);
    make_close_button(s);
    s.push('  </span>');
    s.push('</td></tr></table>');
    s.push(`<div id="canvas_tool_menu_${view_index}"></div>`);
    s.push(`<div style="border:2px; border-color:yellow;"><canvas id="canvas_${view_index}" style="width:800px; height:800px;"></canvas></div><br>`);
    initializers.push(() => {
      this.image_editor = new ImageEditor({
        canvas: document.getElementById(`canvas_${view_index}`),
        editable: true,
        on_change: on_image_editor_change,
        on_sample_color: on_sample_color,
        page_id: page_id,
        item_index: item_index,
        view_index: view_index,
      });
      this.image_editor.load(url);
      on_select_tool(view_index, 0);
      on_color_picker_change(view_index);
    });
  }

  // This is not a built-in method.
  // It is called explicitly when this view is no longer being used.
  destructor() {
    this.image_editor.destructor();
  }
}

function on_click_thumbnail(event, page_id, item_index) {
  render_pages(page_id, item_index).then(() => {
    scroll_to_page(`${page_id}_${item_index}`);
  });
}

// Makes a page for editing the image attached to an item
function make_image_edit_page(s, page_id, item_index, initializers) {
	s.push(`<div id="page_${page_id}_${item_index}" class="bubble">`);
	let page = g_content[page_id];
	if (page === undefined)
	{
		s.push(`No such page: ${page_id}`);
		s.push('</div><br>'); // End of bubble
		return;
	}
	let item = page.items[item_index];
	if (item === undefined)
	{
		s.push(`No such item: ${page_id}:${item_index}`);
		s.push('</div><br>'); // End of bubble
		return;
	}
	if (item.file === undefined)
	{
		s.push(`Item ${page_id}:${item_index} has no file`);
		s.push('</div><br>'); // End of bubble
		return;
	}
	let view_index = g_account.view_pages.length;
	g_account.view_pages.push(new ViewCanvas(s, view_index, item.file, page_id, item_index, initializers));

	s.push('</div><br>'); // End of bubble
}
