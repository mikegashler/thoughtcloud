function get_session_id() {
	let cookie = document.cookie;
	const start_pos = cookie.indexOf('sid=') + 4;
	if (start_pos < 0) {
		alert('expected a cookie');
	}
	const end_pos = cookie.indexOf(';', start_pos);
	let sid;
	if (end_pos >= 0) {
		sid = cookie.substring(start_pos, end_pos);
	} else {
		sid = cookie.substring(start_pos);
	}
	return sid;
}

let g_id = random_id(12);
let g_thread = 0;
let g_secure = false;
let g_remote_id;
let g_local_asymmetric_keys = null;
let g_local_symmetric_key = null;
let g_local_symmetric_key_exported = null;
let g_remote_symmetric_key = null;
let g_received_encryption_key = false;
let g_received_public_key = false;
const g_origin = new URL(window.location.href).origin;
const g_session_id = get_session_id();
let g_displayed_chats = {};
let g_received_chats = [];
let g_chime_sound = new Audio('chime.mp3');

function httpPost(payload, callback)
{
	const body = JSON.stringify(payload);
	fetch(g_secure ? `${g_origin}/aether_chat.html` : `${g_origin}/public_chat.ajax`, {
		body: body,
		cache: "no-cache",
		headers: {
			'Content-Type': 'application/json',
			'Brownie': `sid=${g_session_id}`,
		},
		method: "POST",
	})
	.then((response) => response.text())
	.then(val => {
		//log(`Got a httpPost response: ${val.substring(0, 550)}`);
		return JSON.parse(val);
	})
	.then(ob => callback(ob))
	.catch(ex => {
		console.error(`Error in httpPost: ${ex}\n${ex.trace}`);
	});
}

function display_chat(is_me, timestamp, text, chat_id) {
    if (chat_id in g_displayed_chats) {
        // This message is already displayed, so just update its timestamp
        let date_stamp = g_displayed_chats[chat_id];
        date_stamp.innerHTML = scrub(timestamp);
    } else {
        // Add a new message bubble
        let chats_table = document.getElementById('chats');
        let new_row = chats_table.insertRow();
        let cell = new_row.insertCell(0);
        let date_stamp_id = `ds_${scrub(chat_id)}`;
        cell.innerHTML = `<div class="${is_me ? 'bubble_right' : 'bubble_left'}"><span id="${date_stamp_id}" class="date_stamp">${scrub(timestamp)}</span><br><code>${scrub(text)}</code></span>`;
        cell.scrollIntoView({ behavior: 'smooth', block: 'nearest' });

        // Record that we are displaying this message
        let date_stamp = document.getElementById(date_stamp_id);
        g_displayed_chats[chat_id] = date_stamp;
    }
}

function sync_private_callback(response) {
    let i_am_first = g_remote_id ? false : true;
    let received_a_message = false;
    for (let chat of response.chats) {
        let is_me = (chat[0] === i_am_first);
        let timestamp = chat[1];
        let salt = base64StringToUint8Array(chat[2]);
        let encrypted = base64StringToUint8Array(chat[3]);
        let chat_id = chat[4];
        if (!is_me)
            received_a_message = true;
        window.crypto.subtle.decrypt({
                name: "AES-GCM",
                iv: salt, // The initialization vector you used to encrypt
            },
            is_me ? g_local_symmetric_key : g_remote_symmetric_key,
            encrypted, // The data to decrypt
        ).then(function(decrypted){
            let text = uint8ArrayToString(decrypted);
            if (text === '/clear') {
                reset_page();
                setup_compose_area();
            } else {
                display_chat(is_me, timestamp, text, chat_id);
            }
        }).catch(function(err){
            console.error(`Error in sync_private_callback: ${err}\n${err.trace}`);
        });
    }

    // Update the number of people in the room
    let people_span = document.getElementById('people');
    people_span.innerHTML = response.clients;

    // Play a notification sound
    if (received_a_message)
        g_chime_sound.play();
}

function sync_public_callback(response) {
    // See if the thread changed
    if (response.thread !== g_thread) {
        g_thread = response.thread;
        reset_page();
        setup_compose_area();
    }

    // Receive new messages
    let received_a_message = false;
    for (let chat of response.chats) {
        g_received_chats.push(chat);
        let is_me = (chat.id === g_id);
        display_chat(is_me, chat.time, chat.text, chat.uid);
        if (!is_me)
            received_a_message = true;
    }

    // Update the number of people in the room
    let people_span = document.getElementById('people');
    people_span.innerHTML = response.clients;

    // Play a notification sound
    if (received_a_message)
        g_chime_sound.play();
}

function sync_private() {
    let payload = {
        action: 'sync',
        room: g_remote_id ? g_remote_id : g_id,
        first: g_remote_id ? false : true,
    }
    httpPost(payload, sync_private_callback);
}

function sync_public() {
    let payload = {
        action: 'sync',
        room: 'public',
        id: g_id,
        pos: g_received_chats.length,
        thread: g_thread,
    }
    httpPost(payload, sync_public_callback);
}

function send_private_callback(response) {
}

function send_public_callback(response) {
}

function send_private(text) {
    // Display this message immediately without a timestamp.
    // The timestamp will be updated later when we get this same message back from the server.
    let chat_id = random_id(12);
    if (text === '/clear') {
        reset_page();
        setup_compose_area();
    } else {
        display_chat(true, '', text, chat_id);
    }

    // Encrypt and send the message to the server
    let salt = window.crypto.getRandomValues(new Uint8Array(12));
    window.crypto.subtle.encrypt({
            name: "AES-GCM",
            iv: salt, // this should be different every time you encrypt
        },
        g_local_symmetric_key,
        stringToUint8Array(text), // data you want to encrypt
    ).then(function(encrypted){
        // Make a payload with the encrypted message
        let cypher = new Uint8Array(encrypted)
        payload = {
            action: 'chat',
            chat_id: chat_id,
            room: g_remote_id ? g_remote_id : g_id,
            first: g_remote_id ? false : true,
            salt: uint8ArrayToBase64String(salt),
            text: uint8ArrayToBase64String(cypher),
        };

        // Send the payload with the encrypted message to the server
        httpPost(payload, send_private_callback);
    }).catch(function(err){
        console.error(`Error in send: ${err}\n${err.trace}`);
    });
}

function send_public(text) {
    // Display this message immediately without a timestamp.
    // The timestamp will be updated later when we get this same message back from the server.
    let chat_id = random_id(12);
    display_chat(true, 'Sending...', text, chat_id);

    // Send the message to the server
    payload = {
        action: 'chat',
        room: 'public',
        chat: {
            'id': g_id,
            'text': text,
            'uid': chat_id,
        }
    };

    // Send the payload with the encrypted message to the server
    httpPost(payload, send_public_callback);
}

function request_sym_key() {
    if (g_received_encryption_key)
        return;
    hs4();

    // Check again in 0.5 seconds.
    // (We know the remote client has started the handshake process, so this shouldn't take long.)
    setTimeout(request_sym_key, 500);
}

function request_public_key() {
    if (g_received_public_key)
        return;
    hs2();

    // Check again in 3 seconds.
    // (We're waiting for the remote client to connect, so it might take a while.)
    setTimeout(request_public_key, 3000);
}

// This function is called when the crypto handshake is complete
// and we are ready to begin chatting.
function start_chatting() {
    setup_compose_area();

    // Check for new messages at regular intervals
    setInterval(sync_private, 1500);
}

function hs4_callback(response) {
    if (!response.sym_key)
        return;

    // Record that we have the encrypted symmetric key, so we don't keep asking for it
    g_received_encryption_key = true;
    let cypher = base64StringToUint8Array(response.sym_key);

    // (9) Decrypt the other client's encryption key
    window.crypto.subtle.decrypt(
        {
            name: "RSA-OAEP",
            //label: Uint8Array([...]) //optional
        },
        g_local_asymmetric_keys.privateKey, // The local client's private key
        cypher, // The encrypted data
    )
    .then(function(decrypted){
        let exported_sym_key = JSON.parse(uint8ArrayToString(decrypted));
        window.crypto.subtle.importKey(
            "jwk", //can be "jwk" or "raw"
            exported_sym_key, // The result of exporting the remote client's encryption key
            {   //this is the algorithm options
                name: "AES-GCM",
            },
            false, //whether the key is extractable (i.e. can be used in exportKey)
            ["encrypt", "decrypt"] //can "encrypt", "decrypt", "wrapKey", or "unwrapKey"
        )
        .then(function(symmetric_key){
            g_remote_symmetric_key = symmetric_key
            start_chatting();
        })
        .catch(function(err){
            console.error(`error in hs4_callback: ${err}\n${err.trace}`);
        });
    })
    .catch(function(err){
        console.error(`error in hs4_callback: ${err}\n${err.trace}`);
    });
}

function hs4() {
    let payload = {
        action: 'hs4',
        room: g_remote_id ? g_remote_id : g_id,
        first: g_remote_id ? false : true,
    }
    httpPost(payload, hs4_callback);
}

function hs3_callback(response) {
    request_sym_key();
}

function hs3(byteArray) {
    let payload = {
        action: 'hs3',
        room: g_remote_id ? g_remote_id : g_id,
        first: g_remote_id ? false : true,
        sym_key: uint8ArrayToBase64String(byteArray),
    }
    httpPost(payload, hs3_callback);
}

function hs2_callback(response) {
    if (!response.public_key)
        return;

    // Record that we received the remote client's public key, so we don't keep asking for it
    g_received_public_key = true;

    // (5) Receive the other client's public key
    window.crypto.subtle.importKey(
        "jwk", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
        response.public_key,
        {   //these are the algorithm options
            name: "RSA-OAEP",
            hash: {name: "SHA-256"}, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
        },
        false, //whether the key is extractable (i.e. can be used in exportKey)
        ["encrypt"] //"encrypt" or "wrapKey" for public key import or
                    //"decrypt" or "unwrapKey" for private key imports
    ).then(function(publicKey){
        // (6) Encrypt my encryption key with the other client's public key
        window.crypto.subtle.encrypt(
            {
                name: "RSA-OAEP",
                //label: Uint8Array([...]) //optional
            },
            publicKey, // The remote client's public key
            g_local_symmetric_key_exported, // A byte array containing the data we want to encrypt
        ).then(function(encrypted){
            // (7) Send it to the server
            hs3(new Uint8Array(encrypted));
        }).catch(function(err){
            console.error(`error in hs2_callback: ${err}\n${err.trace}`);
        });
    }).catch(function(err){
        console.error(`error in hs2_callback: ${err}\n${err.trace}`);
    });
}

function hs2() {
    let payload = {
        action: 'hs2',
        room: g_remote_id ? g_remote_id : g_id,
        first: g_remote_id ? false : true,
    }
    httpPost(payload, hs2_callback);
}

function hs1_callback(response) {
    // (3) Make an encryption key
    window.crypto.subtle.generateKey(
        {
            name: "AES-GCM",
            length: 256, //can be 128, 192, or 256
        },
        true, //whether the key is extractable (i.e. can be used in exportKey)
        ["encrypt", "decrypt"] //can "encrypt", "decrypt", "wrapKey", or "unwrapKey"
    ).then(function(key){
        g_local_symmetric_key = key;

        // Export it
        window.crypto.subtle.exportKey(
            "jwk", //can be "jwk" or "raw"
            key //extractable must be true
        ).then(function(keydata){
            g_local_symmetric_key_exported = stringToUint8Array(JSON.stringify(keydata));

            // (4) Request public key
            request_public_key();
        }).catch(function(err){
            console.error(`error in hs1_callback: ${err}\n${err.trace}`);
        });
    }).catch(function(err){
        console.error(`error in hs1_callback: ${err}\n${err.trace}`);
    });
}

function hs1(public_key) {
    let payload = {
        action: 'hs1',
        room: g_remote_id ? g_remote_id : g_id,
        first: g_remote_id ? false : true,
        public_key: public_key,
    }
    httpPost(payload, hs1_callback);
}

function on_post_chat() {
    let text_area = document.getElementById('chat_text');
    if (g_secure)
        send_private(text_area.value);
    else
        send_public(text_area.value);
    text_area.value = '';
    let chat_text = document.getElementById('chat_text');
    auto_grow_text_area(chat_text);
    text_area.focus();
}

// remote_id is optional.
// If it is defined, the server will try to connect to a chat room with that id.
// If it is undefined, the server will create a new chat room with your id.
//
// This code was developed using the fine examples at: https://github.com/diafygi/webcrypto-examples/
// How the handshake works:
// (1) Clients generate asymmetric keys.
// (2) Clients call "hs1" to pass their public keys to the server.
// (3) Clients generate symmetric encryption keys for themselves.
// (4) Clients repeatedly call "hs2" to request the other client's public key.
// (5) The server replies with the other client's public key.
// (6) Clients encrypt their encryption keys with the other client's public key.
// (7) Clients call "hs3" to send it to the server.
// (8) Clients call "hs4" to request the other client's encrypted symmetric key.
// (9) Clients use their own private keys to decrypt the other client's encryption key.
// (10a) Clients call "send" to send messages encrypted with their own encryption keys.
// (10b) Clients call "sync_private" to get updates.
function connect(remote_id) {
    if (remote_id)
        g_remote_id = remote_id;
    var buf = new Uint8Array(64);
    window.crypto.getRandomValues(buf);

    // (1) Make an asymmetric key
    window.crypto.subtle.generateKey(
        {
            name: "RSA-OAEP",
            modulusLength: 2048, //can be 1024, 2048, or 4096
            publicExponent: new Uint8Array([0x01, 0x00, 0x01]),
            hash: {name: "SHA-256"}, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
        },
        false, //whether the private key is extractable (i.e. can be used in exportKey)
        ["encrypt", "decrypt"] //must be ["encrypt", "decrypt"] or ["wrapKey", "unwrapKey"]
    ).then(function(asymmetric_keys){
        g_local_asymmetric_keys = asymmetric_keys;

        // Export it
        window.crypto.subtle.exportKey(
            "jwk", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
            asymmetric_keys.publicKey, //can be a publicKey or privateKey, as long as extractable was true
        )
        .then(function(keydata){
            // (2) Send public key to the server
            hs1(keydata);
        })
        .catch(function(err){
            console.error(`error in connect: ${err}\n${err.trace}`);
        });
    }).catch(function(err){
        console.error(`error in connect: ${err}\n${err.trace}`);
    });
}

function reset_page() {
    let s = [];
	s.push('<div style="position:fixed; top:0; left:0; width:100%; background:#808080; display:flex;">'); // title bar
	s.push(' <div style="float:left; width:100%; display:flex; flex-flow:column; justify-content:space-around;">'); // buttons area
	s.push('  <div id="compose_area"></div>');
	s.push(' </div>'); // end buttons area
	s.push('</div>'); // end title bar
	s.push('<br><br><br><br>'); // space under the title bar
    s.push('<table id="chats" width="100%"></table>');
    let the_content = document.getElementById('the_content');
	the_content.innerHTML = s.join('');
    g_displayed_chats = {};
    g_received_chats = [];
}

function setup_compose_area() {
    // Set up the compose area
    let compose_area = document.getElementById('compose_area');
    let s = [];
    s.push('<table width="100%"><tr><td>');
    s.push('People in room: <span id="people">?</span>')
    s.push('</td></tr>')
    s.push('<tr><td>')
    s.push(`<textarea id="chat_text" oninput="auto_grow_text_area(this)" style="box-sizing: border-box;"></textarea>`);
    s.push('</td><td valign="bottom" width="80px">');
    s.push('<button onclick="on_post_chat()">Post</button>');
    s.push('</td></tr></table>');
    compose_area.innerHTML = s.join('');
    let chat_text = document.getElementById('chat_text');
    auto_grow_text_area(chat_text);
}

function launch_private() {
    g_secure = true;
	reset_page();

    // Display a link to this chat room
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const id = urlParams.get('id');
    if (!id) {
        let url = window.location.href;
        let compose_area = document.getElementById('compose_area');
        compose_area.innerHTML = `Please send this link to the person you want to chat with:<br><a href="${url}?id=${g_id}">${url}?id=${g_id}</a><br>(This link may be used only once.)`;
    }
    connect(id);
}

function launch_public() {
    g_secure = false;
	reset_page();
    setup_compose_area();

    // Check for new messages at regular intervals
    setInterval(sync_public, 1500);
}

function launch_welcome_screen() {
    // See if there is an id for a private room in the url
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const id = urlParams.get('id');
    if (id) {
        launch_private();
        return;
    }

    // Make the welcome page
    let s = [];
    s.push('<table bgcolor=#203040 cellpadding=25 width=800 align=center><tr><td>');
    s.push('<h1>Welcome to chat</h1>');
    s.push('<button onclick="launch_public()">Enter the public chat room</button><br>');
    s.push('Messages persist until the server is reset or someone posts the message "/clear".<br><br>');
    s.push('<button onclick="launch_private()">Make a private chat room</button><br>');
    s.push('Messages are encrypted end-to-end, and even the encrypted messages are purged from the server immediately after being relayed.<br>');
    s.push('</td></tr></table>');
    let the_content = document.getElementById('the_content');
	the_content.innerHTML = s.join('');
}

launch_welcome_screen();
