// ----------------------------------------------------------------
// The contents of this file are dedicated to the public domain as
// described at http://creativecommons.org/publicdomain/zero/1.0/.
// ----------------------------------------------------------------

function on_change_checks_dims(page_id) {
	let days = Number(document.getElementById(`days_${page_id}`).value);
	let cols = Number(document.getElementById(`cols_${page_id}`).value);
	if (days > 0 && days < 735 && cols > 0 && cols <= 100) {
		let checks = g_content[page_id].checks;
		before_page_change(page_id);
		checks.days = days;
		checks.cols = cols;
		render_pages(page_id);
	} else {
		alert('Values out of range');
	}
}

function on_click_checks_box(page_id, item_index, col) {
	if (item_index < 0) {
		alert('That day is before this record began');
		return;
	}
	let cb = document.getElementById(`check_${page_id}_${item_index}_${col}`);
	if (!cb) {
		alert('Failed to find checkbox. This should not happen');
		return;
	}
	before_page_change(page_id);
	while (g_content[page_id].items.length <= item_index)
		g_content[page_id].items.push({ text: '' });
	let item_text = g_content[page_id].items[item_index].text;
	item_values = smart_split(item_text);
	while (item_values.length < col)
		item_values.push('');
	item_values[col] = cb.checked ? 'y' : '';
	g_content[page_id].items[item_index].text = smart_join(item_values);
}

function is_value_truthy(arr, index) {
	if (!arr)
		return false;
	if (index < 0 || index >= arr.length)
		return false;
	let s = arr[index].trim().toLowerCase();
	if (s === '' || s === 'n' || s === 'f' || s == '0' || s === 'false' || s === 'no')
		return false;
	return true;
}

// This view is for recording that daily tasks were performed.
class ViewChecks {
    constructor(s, view_index, page_id, opened_index, initializers, search_highlighter) {
		this.view_index = view_index;
		this.page_id = page_id;

		// Make sure we have a checks field
		let cm = new CalendarMaker(s);
		let checks = g_content[page_id].checks;
		let checks_changed = false;
		if (checks === undefined) {
			let last_week = new Date(cm.today);
			last_week.setDate(cm.today.getDate() - 7);
			checks_changed = true;
			checks = {
				date: date_to_locale_string(last_week),
				days: 8,
				cols: 5,
			}
		}

		s.push(`Days=<input type="text" id="days_${page_id}" size="2" value="${checks.days}"></input> `);
		s.push(`Cols=<input type="text" id="cols_${page_id}" size="2" value="${checks.cols}"></input> `);
		s.push(`<button onclick="on_change_checks_dims('${page_id}');">Change</button><br>`);

		// Render the daily check boxes
		cm.beginCalendar();
		let prev_day = null;
		for (let i = 1 - checks.days; i <= 0; i++) {
			// Compute the relevant data
			let day = new Date(cm.today);
			day.setDate(cm.today.getDate() + i);
			let item_index = Math.round((day - locale_string_to_date(checks.date)) / (24 * 60 * 60 * 1000));
			if (item_index < 0) {
				cm.beginDay(day, prev_day);
				s.push('<td>before record began</td>');
				cm.endDay();
				prev_day = day;
				continue;
			}
			let item_text = '';
			if (g_content[page_id] && g_content[page_id].items && item_index < g_content[page_id].items.length) {
				item_text = g_content[page_id].items[item_index].text;
			}
			let item_values = smart_split(item_text);

			// Render the date
			cm.beginDay(day, prev_day);

			// Render the check boxes for the day
			s.push('<td>');
			for (let j = 0; j < checks.cols; j++) {
				let is_checked = is_value_truthy(item_values, j);
				s.push(`&nbsp;&nbsp;&nbsp;<input type="checkbox" id="check_${page_id}_${item_index}_${j}" onclick="on_click_checks_box('${page_id}', ${item_index}, ${j})" style="scale:1.8;"${is_checked ? ' checked' : ''}></input>&nbsp;&nbsp;&nbsp;`);
			}
			s.push('</td>');
			cm.endDay();
			prev_day = day;
		}
		cm.endCalendar();

		// todo: deal with search highlighters

        // Action buttons
        s.push(`<span id="new_area_${page_id}">`);
        s.push('<table width="100%"><tr>');
        s.push('<td width="50px"></td><td>'); // Horizontal spacer
        make_checks_bottom_buttons(s, page_id);
        s.push('</td></tr></table>')
        s.push('</span><br>');
        s.push(`<div id="action_menu_${page_id}"></div>`);

		// Store changes
		if (checks_changed) {
			before_page_change(page_id);
			g_content[page_id].checks = checks;
		}
	}

    // This is not a built-in method.
    // It is called explicitly when this view is no longer being used.
    destructor() {
    }
}
