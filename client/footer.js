// ----------------------------------------------------------------
// The contents of this file are dedicated to the public domain as
// described at http://creativecommons.org/publicdomain/zero/1.0/.
// ----------------------------------------------------------------


// Consumes an n-by-2 table of values and string-labels.
// Returns an m-by-2 table of bucket y-values, and string-labels.
function histogram_of_ints(value_label_pairs) {
	if (value_label_pairs.length < 1)
		return [];

	// Compute buckets
	let min_val = value_label_pairs[0][0];
	let min_lab = value_label_pairs[0][1];
	let max_val = value_label_pairs[0][0];
	let max_lab = value_label_pairs[0][1];
	for (val_lab of value_label_pairs) {
		if (val_lab[0] < min_val) {
			min_val = val_lab[0];
			min_lab = val_lab[1];
		}
		if (val_lab[0] > max_val) {
			max_val = val_lab[0];
			max_lab = val_lab[1];
		}
	}

	// Check the labels
	if (!Number.isInteger(Number(min_lab)))
		throw new Error('Expected integer labels');
	if (!Number.isInteger(Number(max_lab)))
		throw new Error('Expected integer labels');
	let k = Number(max_lab) - Number(min_lab) + 1;
	if (k < 1)
		return [];
	if (k > 2000)
		throw new Error('Range too big for an integer histogram');

	// Fill the buckets
	let results = [];
	for (let i = 0; i < k; i++) {
		results.push([0, Number(min_lab) + i]);
	}
	for (val_lab of value_label_pairs) {
		let bucket = Math.max(0, Math.min(k - 1, Math.round(Number(val_lab[0]) - Number(min_val))));
		results[bucket][0]++;
	}
	return results;
}
/*
// Consumes an n-by-2 table of values and string-labels.
// Returns an n-by-3 table of x-values, y-values, and string-labels.
// The (x,y) pairs give points in a histogram of the original data.
// The resulting table will have 2*k fewer rows than the input data.
function histogram(value_label_pairs, k, reg) {
    // Filter out nulls and sort
    k = Math.floor(Math.max(k, 1));
    const sorted = [...value_label_pairs.filter(n => n[0] || n[0] === 0)].sort((a, b) => a[0] - b[0]);

    // For each value, measure distance on left and right to kth nearest neighbor
	let left = [];
	let right = [];
	for (let i = k; i + k < sorted.length; i++) {
		j = sorted.length - 1 - i;
		left.push(sorted[i][0] - sorted[i - k][0]);
		right.push(sorted[j + k][0] - sorted[j][0]);
	}
	right.reverse();

    // Turn it into a histogram
	let hist = [];
	for (let i = 0; i < left.length; i++) {
		hist.push([
            sorted[i + k][0], // x
			2 / (left[i] + right[i] + reg), // y-ave
            sorted[i + k][1], // label
		]);
	}

	// Smooth the histogram by averaging values with the same horizontal position
	let hist_smoothed = [];
	let i = 0;
	while (i < hist.length) {
		let j = i + 1;
		let sum_y = hist[i][1];
		while (j < hist.length && hist[j][0] === hist[i][0]) {
			sum_y += hist[j][1];
			j++;
		}
		hist_smoothed.push([
			hist[i][0], // x
			sum_y / (j - i), // y-ave
			hist[i][2], // label
		]);
		i = j;
	}

	return hist_smoothed;
}
*/

// Consumes an n-by-3 table of (x,y,labels)
// Produces an m-by-3 table, where m <= max_label_count,
// and the most-interesting labels are kept
function cull_histogram_labels(x_y_labs, max_label_count, width, height) {
    while(true) {
        if (x_y_labs.length <= max_label_count) {
            return x_y_labs;
        }

        // Compute the average y-value
        let sum = 0;
        for (let i = 0; i < x_y_labs.length; i++) {
            sum += x_y_labs[i][1];
        }
        const vert_ave = sum / x_y_labs.length;

        // Compute interestingness of each point as distance from average plus distance from left and right neighbors
        let interestingness = [];
        for (let i = 0; i < x_y_labs.length; i++) {
            const vert_dist = Math.abs(x_y_labs[i][1] - vert_ave) / height;
            const left_dist = (i > 0 ? Math.abs(x_y_labs[i][0] - x_y_labs[i - 1][0]) : Math.abs(x_y_labs[i][0] - x_y_labs[x_y_labs.length - 1][0])) / width;
            const right_dist = (i + 1 < x_y_labs.length ? Math.abs(x_y_labs[i][0] - x_y_labs[i + 1][0]) : Math.abs(x_y_labs[i][0] - x_y_labs[0][0])) / width;
            interestingness.push([i, vert_dist + left_dist + right_dist]);
        }

        // Identify the indexes to cull
        interestingness.sort((a, b) => a[1] - b[1]); // sort from least-interesting to most-interesting
        let cull_indexes = [];
        const cull_count = Math.max(1, Math.min(Math.floor(x_y_labs.length / 4), x_y_labs.length - max_label_count));
        for (let i = 0; i < cull_count; i++) {
            cull_indexes.push(interestingness[i][0]);
        }

        // Remake the list of labels
        let new_x_y_labs = [];
        for (let i = 0; i < x_y_labs.length; i++) {
            if (!cull_indexes.includes(i)) {
                new_x_y_labs.push(x_y_labs[i]);
            }
        }
        x_y_labs = new_x_y_labs;
    }
}

function similarity_forecast(values, window, neighbors) {
    // Find the most similar past occurrences
    window = Math.max(window, 1);
    let cands = [];
    for (let i = window; i + 1 < values.length; i++) {
        let dissimilarity = 0;
        for (let j = 0; j < window; j++) {
            let t = values[values.length - window + j] - values[values.length - 1 - window + j];
            let c = values[i - window + j] - values[i + 1 - window + j];
            dissimilarity += ((t - c) * (t - c));
        }
        let delta = values[i + 1] - values[i];
        cands.push([dissimilarity, delta]);
    }
    cands.sort((a, b) => a[0] - b[0]); // sort from least to most dissimilar

    // Find the median prediction delta
    neighbors = Math.min(cands.length, neighbors);
    if (neighbors < 1) {
        if (values.length > 0) {
            return values[values.length - 1] + 1;
        } else {
            return null;
        }
    }
    let preds = cands.slice(0, neighbors);
    preds.sort((a, b) => a[1] - b[1]); // sort from smallest to largest delta
    let delta = preds[Math.floor(preds.length / 2)][1];
    return values[values.length - 1] + delta;
}

function median_delta_forecast(values) {
    let deltas = [];
    for (let i = 1; i < values.length; i++) {
        deltas.push(values[i] - values[i - 1]);
    }
    deltas.sort((a, b) => a - b); // sort from smallest to largest delta
    let delta = deltas[Math.floor(deltas.length / 2)];
    return values[values.length - 1] + delta;
}



function make_histogram(page, initializers, value_label_pairs) {
	const hist = histogram_of_ints(value_label_pairs);
	if (hist.length < 2) {
		log('Aborting histogram. Not enough data');
		return;
	}
	let xmin = 0;
	let ymin = 0;
	let xmax = hist.length;
	let ymax = 0;
	for (let i = 0; i < hist.length; i++) {
		ymax = Math.max(ymax, hist[i][0]);
	}
	// xmin -= (xmax - xmin) * .05;
	// xmax += (xmax - xmin) * .05;
	ymax *= 1.3;
	//const labels = cull_histogram_labels([...hist], 8, xmax - xmin, ymax);
	const id = `hist_${(Math.random() + 1).toString(36).substring(2)}`;
	page.push(`<canvas id='${id}' width='700' height='400' style='border:1px solid #cccccc;'></canvas><br>`);
	initializers.push(() => {
		const canvas = document.getElementById(id);
		const plotter = new Plotter(canvas, xmin, ymin, xmax, ymax);
		for (let x = 0; x < hist.length; x++) {
			let h = hist[x];
			let y = h[0];
			let label = `${h[1]} (${y})`;
			plotter.fill_rect(x, 0, x + 1, y, '#404080');
			plotter.plot_labels([[x, ymax, label]]);
		}
	});
}

// function make_histogram_of_values(items, page, initializers, table_strings, table_numbers) {
// 	if (table_numbers) {
// 		const value_label_pairs = [];
// 		for (let i = 0; i < Math.min(table_strings.length, table_numbers.length); i++) {
// 			value_label_pairs.push([table_numbers[i][0], table_strings[i][0]]);
// 		}
// 		make_histogram(page, initializers, value_label_pairs);
// 	} else {
// 		page.push('Items not suitable for a histogram of values<br>');
// 	}
// }


// Plots lines connecting the points in xs and ys.
// Both xs and ys are lists of lists of [value, label] pairs.
function make_line_plot(xs, ys, page, initializers) {
	if (xs.length > ys.length) {
		throw new Error('more horizontal lists than vertical lists');
	}
	let xmin = 1e100;
	let xmax = -1e100;
	let ymin = 1e100;
	let ymax = -1e100;
	for (const xlist of xs) {
		for (const x of xlist) {
			xmin = Math.min(xmin, x[0]);
			xmax = Math.max(xmax, x[0]);
		}
	}
	for (const ylist of ys) {
		for (const y of ylist) {
			ymin = Math.min(ymin, y[0]);
			ymax = Math.max(ymax, y[0]);
		}
	}
	const id = 'plot_' + (Math.random().toString(36) + '00000').slice(2, 7);
	page.push(`<canvas id='${id}' width='700' height='400' style='border:1px solid #cccccc;'></canvas><br>`);
	initializers.push(() => {
		const canvas = document.getElementById(`${id}`);
		if (canvas === null) {
			log(`found no element with id=${id}`);
		}
		const plotter = new Plotter(canvas, xmin, ymin, xmax, ymax);
		plotter.horiz_ticks(25, true);
		plotter.vert_ticks(25, true);
		for (const [i, ylist] of ys.entries()) {
			const xlist = xs[i % xs.length];
			const x_y_labels = xlist.map((e, i) => [e[0], ylist[i][0], `(${e[1]},${ylist[i][1]})`]);
			plotter.plot_line(x_y_labels, '#000000');
			//plotter.plot_labels(x_y_labels);
		}
	});
}


const grid_footers = {
	line: (page, initializers, value_grid, params) => {
		let [ row_beg, col_beg, row_end, col_end ] = params;
		row_beg = smart_mod(row_beg, value_grid.length);
		row_end = smart_mod(row_end - 1, value_grid.length) + 1;
		col_beg = smart_mod(col_beg, value_grid[0].length);
		col_end = smart_mod(col_end - 1, value_grid[0].length) + 1;

		// Make the X-axis data
		const xs = [];
		const x_xlab = [];
		for (let r = row_beg; r < row_end; r++) {
			x_xlab.push([r, '' + r]);
		}
		xs.push(x_xlab);

		// Make the Y-axis data
		const ys = [];
		for (let c = col_beg; c < col_end; c++) {
			const y_ylab = [];
			for (let r = row_beg; r < row_end; r++) {
				const el_str = value_grid[r][c];
				const el_val = as_num(el_str);
				y_ylab.push([el_val, el_str]);
			}
			ys.push(y_ylab);
		}
		make_line_plot(xs, ys, page, initializers)
	},
	histogram: (page, initializers, value_grid, params) => {
		let [ row_beg, col_beg, row_end, col_end ] = params;
		row_beg = smart_mod(row_beg, value_grid.length);
		row_end = smart_mod(row_end - 1, value_grid.length) + 1;
		col_beg = smart_mod(col_beg, value_grid[0].length);
		col_end = smart_mod(col_end - 1, value_grid[0].length) + 1;
		const value_label_pairs = [];
		for (let r = row_beg; r < row_end; r++) {
			for (let c = col_beg; c < col_end; c++) {
				const el_str = value_grid[r][c];
				const el_val = as_num(el_str);
				value_label_pairs.push([el_val, el_str]);
			}
		}
		make_histogram(page, initializers, value_label_pairs);
	},
	forecast: (page, initializers, value_grid, params) => {
		let [ row_beg, col_beg, row_end, col_end ] = params;
		row_beg = smart_mod(row_beg, value_grid.length);
		row_end = smart_mod(row_end - 1, value_grid.length) + 1;
		col_beg = smart_mod(col_beg, value_grid[0].length);
		col_end = smart_mod(col_end - 1, value_grid[0].length) + 1;
		const vals = [];
		for (let r = row_beg; r < row_end; r++) {
			for (let c = col_beg; c < col_end; c++) {
				const el_str = value_grid[r][c];
				const el_val = as_num(el_str);
				vals.push(el_val);
			}
		}
		let mdf_val = median_delta_forecast(vals);
		let sf_val = similarity_forecast(vals, 2, 5);
		let mdf_str = as_str(mdf_val, value_grid[row_end - 1][col_end - 1]);
		let sf_str = as_str(sf_val, value_grid[row_end - 1][col_end - 1]);
		page.push(`Median delta forecast: ${mdf_str}<br>`);
		page.push(`Similarity forecast: ${sf_str}<br>`);
	},
}

// footers is a JSON string that encodes both the name and params
// page is a list of strings (for building the page)
// value_grid is a list of list of string values (to plot)
// initializers is a list of functions to call after the page is made
function make_grid_footers(footers, page, value_grid, initializers) {
	// Make all the footers
	page.push('<br><br>');
	for (const footer_string of footers) {
		//log(`about to parse: ${JSON.stringify(footer_string)}`);
		let footer = JSON.parse(footer_string);
		const footer_maker = grid_footers[footer.name];
		if (footer_maker === undefined)
			throw new Error(`Unrecognized footer name: ${footer.name}`);
		try {
			footer_maker(page, initializers, value_grid, footer.params);
		} catch(ex) {
			page.push(`Problem with footer: ${footer_string}, ${ex}`);
			console.log(ex.stack);
		}
	}
}

function remove_footer(page_id, footer_index) {
	const footers = g_content[page_id].footers || [];
	if (footer_index < 0 || footer_index >= footers.length)
		throw new Error('footer_index out of range');
	before_page_change(page_id);
	footers.splice(footer_index, 1);
	close_menu();
	render_pages();
}

function check_bounds(rmin, cmin, rmax, cmax) {

}

function add_line_footer(page_id) {
	const footers = g_content[page_id].footers || [];
	let row_beg = document.getElementById(`${page_id}_row_beg`).value;
	let col_beg = document.getElementById(`${page_id}_col_beg`).value;
	let row_end = document.getElementById(`${page_id}_row_end`).value;
	let col_end = document.getElementById(`${page_id}_col_end`).value;
	if (col_end === col_beg) {
		alert(`Start col and End col are the same. This results in a difference of zero columns to plot! Perhaps End col should be ${col_beg + 1} or 0 (which means until the end)?`);
		return;
	}
	if (row_end === row_beg) {
		alert(`Start row and End row are the same. This results in a difference of zero rows to plot! Perhaps End row should be 0 (which means until the end)?`);
		return;
	}
	// todo: check the values for consistency
	before_page_change(page_id);
	footers.push(JSON.stringify({
		name: 'line',
		params: [row_beg, col_beg, row_end, col_end],
	}));
	g_content[page_id].footers = footers;
	close_menu();
	render_pages();
}

function add_hist_footer(page_id) {
	const footers = g_content[page_id].footers || [];
	let row_beg = document.getElementById(`${page_id}_row_beg`).value;
	let col_beg = document.getElementById(`${page_id}_col_beg`).value;
	let row_end = document.getElementById(`${page_id}_row_end`).value;
	let col_end = document.getElementById(`${page_id}_col_end`).value;
	if (col_end === col_beg) {
		alert(`Start col and End col are the same. This results in a difference of zero columns to analyze! Perhaps End col should be ${col_beg + 1} or 0 (which means until the end)?`);
		return;
	}
	if (row_end === row_beg) {
		alert(`Start row and End row are the same. This results in a difference of zero rows to analyze! Perhaps End row should be 0 (which means until the end)?`);
		return;
	}
	before_page_change(page_id);
	footers.push(JSON.stringify({
		name: 'histogram',
		params: [row_beg, col_beg, row_end, col_end]
	}));
	g_content[page_id].footers = footers;
	close_menu();
	render_pages();
}

function add_forecast_footer(page_id) {
	const footers = g_content[page_id].footers || [];
	let row_beg = document.getElementById(`${page_id}_row_beg`).value;
	let col_beg = document.getElementById(`${page_id}_col_beg`).value;
	let row_end = document.getElementById(`${page_id}_row_end`).value;
	let col_end = document.getElementById(`${page_id}_col_end`).value;
	if (col_end === col_beg) {
		alert(`Start col and End col are the same. This results in a difference of zero columns to forecast! Perhaps End col should be ${col_beg + 1} or 0 (which means until the end)?`);
		return;
	}
	if (row_end === row_beg) {
		alert(`Start row and End row are the same. This results in a difference of zero rows to forecast! Perhaps End row should be 0 (which means until the end)?`);
		return;
	}
	before_page_change(page_id);
	footers.push(JSON.stringify({
		name: 'forecast',
		params: [row_beg, col_beg, row_end, col_end]
	}));
	g_content[page_id].footers = footers;
	close_menu();
	render_pages();
}

function menu_footers(page_id) {
	// Find the view (which contains the grid and the selection ranges)
	let view = find_view(page_id);

	let div_id = `action_menu_${page_id}`;
	if (g_menu_div_id !== '') {
		close_menu();
		return;
	}
	close_menu();

	// Don't interrupt if someone else edits the page we're on
	g_editing_page = page_id;
	g_editing_index = -2;

	// Make sure we reload the whole page when this menu is closed
	g_close_ops.push(() => {
		render_pages();
	});

	g_menu_div_id = div_id;
	g_menu_callback = bad_menu_state;
	const footers = g_content[page_id].footers || [];
	const s = [];
	s.push('<div class="modal">');
	if (footers.length > 0) {
		s.push('<h3>Remove an existing footer:</h3>');
		s.push('<table cellspacing=10px>');
		for (let i = 0; i < footers.length; i++) {
			const footer = footers[i];
			s.push(`<tr>`);
			s.push(`<td><button onclick="remove_footer('${page_id}',${i})">Remove</button></td>`);
			s.push(`<td>${footer}</td>`);
			s.push(`</tr>`);
		}
		s.push('</table>');
	}

	s.push('<h3>Add a new footer:</h3>');
	s.push('<div style="border-style:solid; padding:5px">Note that End col is exclusive, so it should not be the same as Start col.');
	s.push('Same for End row. Also note that values are torroidal, so an end value of 0 will go to the end ');
	s.push('of the rows or columns, and negative values will count back from the end.</div>');
	s.push('<table cellspacing=10px>');
	s.push(`<tr>`);
	s.push(`	<td align="right">Start row</td><td><input id="${page_id}_row_beg" type="text" size="4" value="1"></td>`);
	s.push(`	<td align="right">, Start col</td><td><input id="${page_id}_col_beg" type="text" size="4" value="0"></td>`);
	s.push(`</tr>`);
	s.push(`<tr>`);
	s.push(`	<td align="right">End row</td><td><input id="${page_id}_row_end" type="text" size="4" value="0"></td>`);
	s.push(`	<td align="right">, End col</td><td><input id="${page_id}_col_end" type="text" size="4" value="1"></td>`);
	s.push(`</tr>`);
	s.push(`</table>`);
	s.push(`<button onclick="add_line_footer('${page_id}')">Add line graph</button>&nbsp;&nbsp;&nbsp;`);
	s.push(`<button onclick="add_hist_footer('${page_id}')">Add histogram</button>&nbsp;&nbsp;&nbsp;`);
	s.push(`<button onclick="add_forecast_footer('${page_id}')">Add forecast</button>`);
	s.push('<br><br><br>');
	s.push('<button onclick="close_menu()">Close</button>');
	s.push('</div><br>');
	let menu_div = document.getElementById(g_menu_div_id);
    menu_div.innerHTML = s.join('');
}

