function grid_accept_row_edits(view_index) {
	// Store all the values
	let view_grid_row = g_account.view_pages[view_index];
	let item = g_content[view_grid_row.page_id].items[view_grid_row.row_index];
	let raw_cells = get_raw_cells(item);
	let need_column_reorder = false;
	view_grid_row.input_elements.sort((a, b) => {
		let rect_a = a.getBoundingClientRect();
		let rect_b = b.getBoundingClientRect();
		return rect_a.top - rect_b.top;
	});
	for (let i = 0; i < view_grid_row.input_elements.length; i++) {
		let el = view_grid_row.input_elements[i];
		if (el.col_index !== i)
			need_column_reorder = true;
		raw_cells[el.col_index] = el.value;
	}
	item.text = smart_join(raw_cells);

	// Reorder columns (if the user reordered any of them)
	if (need_column_reorder) {
		for (let row = 0; row < g_content[view_grid_row.page_id].items.length; row++)
		{
			item = g_content[view_grid_row.page_id].items[row];
			let raw_cells_bef = get_raw_cells(item);
			let raw_cells_aft = [];
			for (let i = 0; i < view_grid_row.input_elements.length; i++) {
				let el = view_grid_row.input_elements[i];
				let bef_index = el.col_index;
				raw_cells_aft.push(raw_cells_bef[bef_index]);
			}
			item.text = smart_join(raw_cells_aft);
		}
	}

	// Close the page
	close_page(view_grid_row.page_id);
}

class ViewGridRow {
    constructor(s, view_index, page_id, row_index, initializers) {
		this.view_index = view_index;
		this.page_id = page_id;
		this.row_index = row_index;
		this.selection_enabled = true;
		this.input_elements = [];
		this.autocomplete_div = null;
		if (row_index < 0 || !g_content[page_id].items || g_content[page_id].items.length <= row_index) {
			alert('row_index out of range');
			return;
		}

		// Heading and close button
		s.push('<table width="100%"><tr><td>');
		s.push('  <table><tr>');
		s.push(`Row ${row_index}`);
		s.push('  </tr></table>');
		s.push('</td><td width="60px" valign="top">');
		s.push(`  <span style="cursor: pointer;" onclick="close_page('${page_id}')">`); // page_id is the id of the parent, not the id of the page we are closing
		make_close_button(s);
		s.push('  </span>');
		s.push('</td></tr></table>');

		// Prepare the names and values
		let items = g_content[page_id].items;
		let row_names = items[0];
		let row_values = items[row_index];
		let names = get_raw_cells(row_names);
		let values = get_raw_cells(row_values);
		while (names.length < values.length)
			names.push('');
		while (values.length < names.length)
			values.push('');
		if (row_index === 0) {
			for (let i = 0; i < names.length; i++)
				names[i] = '' + i;
		}

        // Render the row elements
		s.push(`<div id="list_${page_id}" class="list">`);
        for(let i = 0; i < names.length; i++) {
            s.push('<div class="list_item is-idle"><table width="100%">');
            s.push(`<tr><td valign="top" align="right" width="50px">`);
            s.push('<div class="item_bullet">');
			s.push('≣');
			s.push('</div>');
            s.push('</td><td valign="top">');
			if (i === 0 && items[row_index].file) {
				s.push('todo: image goes here');
			} else {
				s.push(`<table width="100%" cellpadding="2"><tr>`);
				s.push(`<td width="40%" align="right">${names[i]}</td>`);
				s.push(`<td><div><input type="text" id="inp_${page_id}_${i}" value="${values[i]}"></input></div></td>`);
				s.push(`</tr></table>`)
			}
            s.push('</td></tr>');
            s.push('</table></div>');
        }
		s.push('</div>');

		s.push(`<table width="100%" cellpadding="2"><tr>`);
		s.push(`<td width="50%" align="right"></td>`);
		s.push(`<td><button onclick="grid_accept_row_edits(${view_index})">OK</button></td>`);
		s.push(`</tr></table>`)

		this.grabbedItem = undefined;
		this.pointerStartX = undefined;
		this.pointerStartY = undefined;
		this.itemsGap = 0;
		this.itemDivs = [];
		this.prevRect = {};
		initializers.push(() => {
			this.listContainer = document.getElementById(`list_${page_id}`);

			// Handle grab and release events
			this.dragStartHandler = (event) => { this.dragStart(event) };
			this.listContainer.addEventListener('mousedown', this.dragStartHandler);
			this.listContainer.addEventListener('touchstart', this.dragStartHandler);
			this.dragEndHandler = (event) => { this.dragEnd(event) };
			document.addEventListener('mouseup', this.dragEndHandler);
			document.addEventListener('touchend', this.dragEndHandler);

			// Handlers for setting up and tearing down the auto-complete for input elements
			for(let i = 0; i < names.length; i++) {
				let input_el = document.getElementById(`inp_${page_id}_${i}`);
				input_el.col_index = i;
				this.input_elements.push(input_el);
				input_el.addEventListener('focusin', (event) => { this.focusIn(event, row_index, i, this.input_elements[i]); });
				input_el.addEventListener('focusout', (event) => { this.focusOut(event, row_index, i, this.input_elements[i]); });
				input_el.addEventListener('input', (event) => { this.focusIn(event, row_index, i, this.input_elements[i]); });
			}
		});
    }

    // This is not a built-in method.
    // It is called explicitly when this view is no longer being used.
    destructor() {
		document.removeEventListener('mousedown', this.dragStartHandler);
		document.removeEventListener('touchstart', this.dragStartHandler);
		document.removeEventListener('mouseup', this.dragEndHandler);
		document.removeEventListener('touchend', this.dragEndHandler);
    }

	makeSuggestionList(row, col, text) {
		let items = g_content[this.page_id].items;
		if (row < 1 || row >= items.length || col < 0)
			return [];

		// Get the column title
		let values = get_raw_cells(items[0]);
		let title = values[col];

		// Make a list of sample values
		let samples = [];
		for (let i = 1; i < items.length; i++) {
			let values = get_raw_cells(items[i]);
			samples.push(values[col]);
		}

		// Produce a list of suggestions
		let max_suggestions = 10;
		return suggest_value(samples, row - 1, text, title, max_suggestions);
	}

	destroyAutocompleteDiv() {
		if (!this.autocomplete_div)
			return;
		this.autocomplete_div.remove();
		this.autocomplete_div = null;
	}

	acceptSuggestion(col_index, value) {
		let inputEl = document.getElementById(`inp_${this.page_id}_${col_index}`);
		inputEl.value = value;
		this.destroyAutocompleteDiv();
		let next_el = this.input_elements[inputEl.col_index + 1];
		if (next_el)
			next_el.focus();
	}

	focusIn(event, row_index, col_index, input_el) {
		// Get rid of existing suggestion pop-up
		this.destroyAutocompleteDiv();

		// Get a list of suggestions
		let suggestions = this.makeSuggestionList(row_index, col_index, input_el.value);
		if (suggestions.length < 1)
			return;

		// Render the suggestions
		this.autocomplete_div = document.createElement('DIV');
		this.autocomplete_div.classList.add('autocomplete');
		let s = [];
		for (let i = 0; i < suggestions.length; i++) {
			let sugg = suggestions[i][0].length > 30 ? suggestions[i].substring(0, 27) + '...' : suggestions[i][0];
			sugg += ` [${suggestions[i][1]}]`; // Add the reason
			s.push(`<span id="sugg_${i}">${sugg}</span>`);
			s.push('<br>');
		}
		this.autocomplete_div.innerHTML = s.join('');

		// Place it just below the input, and a little bit to the right
		let bodyRect = document.body.getBoundingClientRect();
		let inputRect = input_el.getBoundingClientRect();
		let x = inputRect.left - bodyRect.left + inputRect.bottom - inputRect.top;
		let y = inputRect.top - bodyRect.top + inputRect.bottom - inputRect.top;
		this.autocomplete_div.style.left = '' + x + 'px'; 
		this.autocomplete_div.style.top = '' + y + 'px';
		input_el.parentElement.appendChild(this.autocomplete_div);

		// Activate the suggestion links
		for (let i = 0; i < suggestions.length; i++) {
			let suggestionSpan = document.getElementById(`sugg_${i}`);
			suggestionSpan.onclick = (event) => {
				this.acceptSuggestion(col_index, suggestions[i][0]); 
			};
		}
	}

	focusOut(event, row_index, col_index, input_el) {
		// We need to delay before destroying the div because clicking on it
		// causes a focusOut, which triggers before the click event.
		let new_focus = event.relatedTarget;
		if (new_focus && 
			!event.relatedTarget.classList.contains('autocomplete') &&
			!event.relatedTarget.parentElement.classList.contains('autocomplete')
		) {
			this.destroyAutocompleteDiv();
		}
	}

	_allListItems() {
		if (!this.itemDivs?.length) {
		  this.itemDivs = Array.from(this.listContainer.querySelectorAll('.list_item'));
		}
		return this.itemDivs;
	}
	
	_allIdleItems() {
		return this._allListItems().filter((item) => item.classList.contains('is-idle'));
	}
	
	// Checks for item.dataset.isAbove
	_isItemAbove(item) {
		return item.hasAttribute('data-is-above');
	}

	// Checks for item.data.isToggled
	_isItemToggled(item) {
		return item.hasAttribute('data-is-toggled');
	}
	
	_disablePageScroll() {
		document.body.style.overflow = 'hidden';
		document.body.style.touchAction = 'none';
		document.body.style.userSelect = 'none';
	}
	
	_enablePageScroll() {
		document.body.style.overflow = '';
		document.body.style.touchAction = '';
		document.body.style.userSelect = '';
	}
	
	dragStart(e) {
		// Grab an item
		if (e.target.classList.contains('item_bullet')) {
			this.grabbedItem = e.target.closest('.list_item');
		}
		if (!this.grabbedItem) {
			return;
		}
		
		e.preventDefault();

		// Store the starting point
		this.pointerStartX = e.clientX || e.touches?.[0]?.clientX;
		this.pointerStartY = e.clientY || e.touches?.[0]?.clientY;

		// Set item gap
		if (this._allIdleItems().length <= 1) {
			this.itemsGap = 0;
		} else {
			//this._disablePageScroll();
			const item1 = this._allIdleItems()[0];
			const item2 = this._allIdleItems()[1];
			const item1Rect = item1.getBoundingClientRect();
			const item2Rect = item2.getBoundingClientRect();
			this.itemsGap = Math.abs(item1Rect.bottom - item2Rect.top);
		}

		// Init the grabbed item
		this.grabbedItem.classList.remove('is-idle');
		this.grabbedItem.classList.add('is-grabbed');

		// Init item state
		const grabbedIndex = this._allListItems().indexOf(this.grabbedItem);
		this._allIdleItems().forEach((item, i) => {
			if (grabbedIndex > i) {
				item.dataset.isAbove = '';
			}
		})

		this.prevRect = this.grabbedItem.getBoundingClientRect();

		this.dragEventHandler = (event) => { this.drag(event) };
		document.addEventListener('mousemove', this.dragEventHandler);
		document.addEventListener('touchmove', this.dragEventHandler, { passive: false });
	}

	drag(e) {
		if (!this.grabbedItem) {
			return;
		}
		e.preventDefault();
	
		// Get the mouse position relative to the start
		let clientX = e.clientX;
		if (clientX === undefined) {
		  if (e.touches)
			clientX = e.touches[0].clientX;
		  if (clientX === undefined)
			clientX = this.pointerStartX;
		}
		let clientY = e.clientY;
		if (clientY === undefined) {
		  if (e.touches)
			clientY = e.touches[0].clientY;
		  if (clientY === undefined)
			clientY = this.pointerStartY;
		}
		const pointerOffsetX = clientX - this.pointerStartX;
		const pointerOffsetY = clientY - this.pointerStartY;
	
		// Move the dragged item to where the mouse is
		this.grabbedItem.style.transform = `translate(${pointerOffsetX}px, ${pointerOffsetY}px)`;
	
		// Update the state and position of idle items
		const grabbedItemRect = this.grabbedItem.getBoundingClientRect();
		const grabbedItemY = grabbedItemRect.top + grabbedItemRect.height / 2;
	
		// Update state
		this._allIdleItems().forEach((item) => {
		  const itemRect = item.getBoundingClientRect();
		  const itemY = itemRect.top + itemRect.height / 2;
		  if (this._isItemAbove(item)) {
			if (grabbedItemY <= itemY) {
			  item.dataset.isToggled = '';
			} else {
			  delete item.dataset.isToggled;
			}
		  } else {
			if (grabbedItemY >= itemY) {
			  item.dataset.isToggled = '';
			} else {
			  delete item.dataset.isToggled;
			}
		  }
		})
	
		// Update position
		this._allIdleItems().forEach((item) => {
		  if (this._isItemToggled(item)) {
			const direction = this._isItemAbove(item) ? 1 : -1;
			item.style.transform = `translateY(${direction * (grabbedItemRect.height + this.itemsGap)}px)`;
		  } else {
			item.style.transform = '';
		  }
		})
	}

	dragEnd(e) {
		if (!this.grabbedItem) {
			return;
		}
		log('in dragEnd');

		// Apply new items order
		const reorderedItems = [];
		const reorderedIndexes = [];

		let grabbedIndexBefore = -1;
		this._allListItems().forEach((item, index) => {
			if (item === this.grabbedItem) {
				grabbedIndexBefore = index;
				return;
			}
			if (!this._isItemToggled(item)) {
				reorderedItems[index] = item;
				reorderedIndexes[index] = index;
				return;
			}
			const newIndex = this._isItemAbove(item) ? index + 1 : index - 1;
			reorderedItems[newIndex] = item;
			reorderedIndexes[newIndex] = index;
		})

		for (let index = 0; index < this._allListItems().length; index++) {
			const item = reorderedItems[index];
			if (item === undefined) {
				reorderedItems[index] = this.grabbedItem;
				reorderedIndexes[index] = grabbedIndexBefore;
				break;
			}
		}

		reorderedItems.forEach((item) => {
			this.listContainer.appendChild(item);
		})

		this.grabbedItem.style.transform = '';

		requestAnimationFrame(() => {
			const rect = this.grabbedItem.getBoundingClientRect();
			const yDiff = this.prevRect.y - rect.y;
			const currentPositionX = e.clientX || e.changedTouches?.[0]?.clientX;
			const currentPositionY = e.clientY || e.changedTouches?.[0]?.clientY;

			const pointerOffsetX = currentPositionX - this.pointerStartX;
			const pointerOffsetY = currentPositionY - this.pointerStartY;

			this.grabbedItem.style.transform = `translate(${pointerOffsetX}px, ${pointerOffsetY + yDiff}px)`;
			requestAnimationFrame(() => {
				this.grabbedItem.style = null;
				this.grabbedItem.classList.remove('is-grabbed');
				this.grabbedItem.classList.add('is-idle');
				this.grabbedItem = null;
			});
		});

		// Clean up
		this.itemsGap = 0;
		this.itemDivs = [];

		//this._enablePageScroll();

		this._allIdleItems().forEach((item, i) => {
			delete item.dataset.isAbove;
			delete item.dataset.isToggled;
			item.style.transform = '';
		})

		document.removeEventListener('mousemove', this.dragEventHandler);
		document.removeEventListener('touchmove', this.dragEventHandler);
		this.dragEventHandler = null;
	}
}

function get_raw_cells(item, min_len) {
	let cells = smart_split(item.file ? (',' + item.text) : item.text);
	if (min_len !== undefined) {
		while(cells.length < min_len)
			cells.push('');
	}
	return cells;
}

function on_grid_copy(view_index) {
	let view_grid = g_account.view_pages[view_index];
	let items = g_content[view_grid.page_id].items;
	let text_rows = [];
	for (let y = view_grid.sel_start_row; y < view_grid.sel_end_row; y++) {
		let raw_cells = get_raw_cells(items[y]);
		let selected_cells = raw_cells.slice(view_grid.sel_start_col, view_grid.sel_end_col);
		text_rows.push(smart_join(selected_cells));
	}
	copy_to_clipboard(text_rows.join('\n'));
}

async function grid_operation_done()
{
	await render_pages();
	sync();
}

function on_grid_paste(view_index) {
	navigator.clipboard.readText().then(async (text) => {
		let clipboard_lines = text.split('\n');
		let view_grid = g_account.view_pages[view_index];
		let items = g_content[view_grid.page_id].items;
		let now = Date.now();
		before_page_change(view_grid.page_id);
		let paste_width = 1;
		for (let y = 0; y < clipboard_lines.length; y++) {
			let clipboard_cells = smart_split(clipboard_lines[y]);
			paste_width = Math.max(paste_width, clipboard_cells.length);
			let dest_y = view_grid.sel_start_row + y;
			while (items.length <= dest_y) {
				items.push({
					mod: now,
					text: '',
				});
			}
			let raw_cells = get_raw_cells(items[dest_y], view_grid.sel_start_col + clipboard_cells.length);
			for (let x = 0; x < clipboard_cells.length; x++) {
				raw_cells[view_grid.sel_start_col + x] = clipboard_cells[x];
			}
			items[dest_y].text = smart_join(raw_cells);
			items[dest_y].mod = now;
		}
		await grid_operation_done();
		let new_view_grid = g_account.view_pages[view_index];
		new_view_grid.set_selection(view_grid.sel_start_row, view_grid.sel_start_col, view_grid.sel_start_row + clipboard_lines.length, view_grid.sel_start_col + paste_width, true);
	}).catch(err => {
		console.error('Failed to read clipboard contents: ', err);
	});
}

// re-renders all the pages with an additional edit-row-page
async function on_grid_edit_row(view_index) {
	let view_grid = g_account.view_pages[view_index];
	await render_pages(view_grid.page_id, view_grid.sel_start_row);
	scroll_to_page(`${view_grid.page_id}_${view_grid.sel_start_row}`);
}

// Duplicates the selected row
async function on_grid_add_row(view_index) {
	let view_grid = g_account.view_pages[view_index];
	let items = g_content[view_grid.page_id].items;
	let row_index = view_grid.sel_end_row - 1;
    before_page_change(view_grid.page_id);
	let item_copy = { ...items[row_index] };

	// Clear all the cells
	let raw_cells = get_raw_cells(item_copy);
	for (let i = 0; i < raw_cells.length; i++)
		raw_cells[i] = '';
	item_copy.text = smart_join(raw_cells);

	item_copy.mod = Date.now();
	items.splice(row_index + 1, 0, item_copy);
	await grid_operation_done();
	let new_view_grid = g_account.view_pages[view_index];
	new_view_grid.set_selection(row_index + 1, 0, row_index + 2, raw_cells.length, true)
}

function on_grid_del_row(view_index) {
	let view_grid = g_account.view_pages[view_index];
	let items = g_content[view_grid.page_id].items;
    before_page_change(view_grid.page_id);
	items.splice(view_grid.sel_start_row, view_grid.sel_end_row - view_grid.sel_start_row);
	grid_operation_done();
}

async function on_grid_add_col(view_index) {
	let view_grid = g_account.view_pages[view_index];
	let col_index = view_grid.sel_end_col - 1;
    before_page_change(view_grid.page_id);
	let items = g_content[view_grid.page_id].items;
	let now = Date.now();
	for (let i = 0; i < items.length; i++) {
		let item = items[i];
		raw_cells = get_raw_cells(item, col_index);
		raw_cells.splice(col_index + 1, 0, ' ');
		item.text = smart_join(raw_cells);
		item.mod = now;
	}
	await grid_operation_done();
	let new_view_grid = g_account.view_pages[view_index];
	new_view_grid.set_selection(0, col_index + 1, items.length, col_index + 2, true)
}

function on_grid_del_col(view_index) {
	let view_grid = g_account.view_pages[view_index];
	page_id = view_grid.page_id;
    before_page_change(page_id);
	let items = g_content[page_id].items;
	for (let i = items.length - 1; i >= 0; i--) {
		let item = items[i];
		raw_cells = get_raw_cells(item, view_grid.sel_end_col);
		raw_cells.splice(view_grid.sel_start_col, view_grid.sel_end_col - view_grid.sel_start_col);
		if (raw_cells.length > 0)
			item.text = smart_join(raw_cells);
		else
			items.splice(i, 1);
	}
	grid_operation_done();
}

async function on_grid_row_up(view_index) {
	let view_grid = g_account.view_pages[view_index];
	if (view_grid.sel_start_row === 0 && view_grid.sel_end_row - view_grid.sel_start_row > 1)
		throw new Error('Already at the beginning');
	let items = g_content[view_grid.page_id].items;
	before_page_change(view_grid.page_id);
	let move_me = items.slice(view_grid.sel_start_row, view_grid.sel_end_row);
	items.splice(view_grid.sel_start_row, view_grid.sel_end_row - view_grid.sel_start_row); // remove
	let new_row = view_grid.sel_start_row > 0 ? view_grid.sel_start_row - 1 : items.length;
	items.splice(new_row, 0, ...move_me); // insert
	await grid_operation_done();
	let new_view_grid = g_account.view_pages[view_index]; // reget this because the previous operation changed it
	new_view_grid.set_selection(new_row, 0, new_row + view_grid.sel_end_row - view_grid.sel_start_row, move_me[0].text.length, true);
}

async function on_grid_row_down(view_index) {
	let view_grid = g_account.view_pages[view_index];
	let items = g_content[view_grid.page_id].items;
	if (view_grid.sel_end_row >= items.length && view_grid.sel_end_row - view_grid.sel_start_row > 1)
		throw new Error('Already at the end');
	before_page_change(view_grid.page_id);
	let move_me = items.slice(view_grid.sel_start_row, view_grid.sel_end_row);
	items.splice(view_grid.sel_start_row, view_grid.sel_end_row - view_grid.sel_start_row); // remove
	let new_row = view_grid.sel_start_row < items.length ? view_grid.sel_start_row + 1 : 0
	items.splice(new_row, 0, ...move_me); // insert
	await grid_operation_done();
	let new_view_grid = g_account.view_pages[view_index];
	console.log(`items.length=${items.length}, new_row=${new_row}, diff=${view_grid.sel_end_row - view_grid.sel_start_row}`);
	new_view_grid.set_selection(new_row, 0, new_row + view_grid.sel_end_row - view_grid.sel_start_row, move_me[0].text.length, true);
}

async function on_grid_col_left(view_index) {
	let view_grid = g_account.view_pages[view_index];
	if (view_grid.sel_start_col === 0 && view_grid.sel_end_col - view_grid.sel_start_col > 1)
		throw new Error('Already at the left side');

	// Determine how many cols each row needs to complete this operation
	let needed_cols = view_grid.sel_end_col;
	let items = g_content[view_grid.page_id].items;
	if (view_grid.sel_start_col === 0) {
		let max_cols = 1;
		for (let row = 0; row < items.length; row++) {
			raw_cells = get_raw_cells(items[row]);
			max_cols = Math.max(max_cols, raw_cells.length);
		}
		needed_cols = max_cols;
	}

	// Move the selected columns left
	before_page_change(view_grid.page_id);
	let new_col = view_grid.sel_start_col > 0 ? view_grid.sel_start_col - 1 : needed_cols - 1;
	for (let row = 0; row < items.length; row++) {
		let item = items[row];
		raw_cells = get_raw_cells(item, needed_cols);
		let move_me = raw_cells.slice(view_grid.sel_start_col, view_grid.sel_end_col);
		raw_cells.splice(view_grid.sel_start_col, view_grid.sel_end_col - view_grid.sel_start_col); // remove
		raw_cells.splice(new_col, 0, ...move_me); // insert
		item.text = smart_join(raw_cells);
	}
	await grid_operation_done();
	let new_view_grid = g_account.view_pages[view_index];
	new_view_grid.set_selection(0, new_col, items.length, new_col + view_grid.sel_end_col - view_grid.sel_start_col, true);
}

async function on_grid_col_right(view_index) {
	let view_grid = g_account.view_pages[view_index];

	// Determine how many cols each row needs to complete this operation
	let items = g_content[view_grid.page_id].items;
	let max_cols = 1;
	for (let row = 0; row < items.length; row++) {
		let item = items[row];
		raw_cells = get_raw_cells(item);
		max_cols = Math.max(max_cols, raw_cells.length);
	}
	let needed_cols = Math.min(max_cols, view_grid.sel_end_col + 1);
	if (view_grid.sel_end_col >= needed_cols && view_grid.sel_end_col - view_grid.sel_start_col > 1)
		throw new Error('Already at the right side');

	// Move the selected columns right
	before_page_change(view_grid.page_id);
	let new_col = view_grid.sel_start_col + 1 < max_cols ? view_grid.sel_start_col + 1 : 0;
	for (let row = 0; row < items.length; row++) {
		let item = items[row];
		raw_cells = get_raw_cells(item, needed_cols);
		let move_me = raw_cells.slice(view_grid.sel_start_col, view_grid.sel_end_col);
		raw_cells.splice(view_grid.sel_start_col, view_grid.sel_end_col - view_grid.sel_start_col); // remove
		raw_cells.splice(new_col, 0, ...move_me); // insert
		item.text = smart_join(raw_cells);
	}
	await grid_operation_done();
	let new_view_grid = g_account.view_pages[view_index];
	new_view_grid.set_selection(0, new_col, items.length, new_col + view_grid.sel_end_col - view_grid.sel_start_col, true);
}

async function accept_cell_edit(view_index) {
	let view_grid = g_account.view_pages[view_index];
    let text_area = document.getElementById('edit_cell');
	let items = g_content[view_grid.page_id].items;
	let dest_date = Date.now();
	before_page_change(view_grid.page_id);
	if (text_area) {
		// Set all cells to the same value
		let dest_val = text_area.value.trim();
		for (let y = view_grid.sel_start_row; y < view_grid.sel_end_row; y++) {
			let raw_cells = get_raw_cells(items[y], view_grid.sel_end_col);
			for (let x = view_grid.sel_start_col; x < view_grid.sel_end_col; x++)
				raw_cells[x] = dest_val;
			items[y].mod = dest_date
			items[y].text = smart_join(raw_cells);
		}
	} else {
		// Pull
		if (view_grid.pull_y > 0) {
			let copy_me = get_raw_cells(items[view_grid.sel_start_row], view_grid.sel_end_col);
			for (let y = view_grid.sel_start_row + 1; y < view_grid.sel_end_row; y++) {
				let raw_cells = get_raw_cells(items[y], view_grid.sel_end_col);
				for (let x = view_grid.sel_start_col; x < view_grid.sel_end_col; x++)
					raw_cells[x] = copy_me[x];
				items[y].mod = dest_date
				items[y].text = smart_join(raw_cells);
			}
		} else if (view_grid.pull_y < 0) {
			let copy_me = get_raw_cells(items[view_grid.sel_end_row - 1], view_grid.sel_end_col);
			for (let y = view_grid.sel_end_row - 2; y >= view_grid.sel_start_row; y--) {
				let raw_cells = get_raw_cells(items[y], view_grid.sel_end_col);
				for (let x = view_grid.sel_start_col; x < view_grid.sel_end_col; x++)
					raw_cells[x] = copy_me[x];
				items[y].mod = dest_date
				items[y].text = smart_join(raw_cells);
			}
		} else if (view_grid.pull_x > 0) {
			for (let y = view_grid.sel_start_row; y < view_grid.sel_end_row; y++) {
				let raw_cells = get_raw_cells(items[y], view_grid.sel_end_col);
				for (let x = view_grid.sel_start_col + 1; x < view_grid.sel_end_col; x++) {
					raw_cells[x] = raw_cells[view_grid.sel_start_col];
				}
				items[y].mod = dest_date
				items[y].text = smart_join(raw_cells);
			}
		} else if (view_grid.pull_x < 0) {
			for (let y = view_grid.sel_start_row; y < view_grid.sel_end_row; y++) {
				let raw_cells = get_raw_cells(items[y], view_grid.sel_end_col);
				for (let x = view_grid.sel_end_col - 2; x >= view_grid.sel_start_col; x--) {
					raw_cells[x] = raw_cells[view_grid.sel_end_col - 1];
				}
				items[y].mod = dest_date
				items[y].text = smart_join(raw_cells);
			}
		} else {
			throw new Error('Expected a value or pull direction');
		}
	}
	await grid_operation_done();
	let new_view_grid = g_account.view_pages[view_index];
	if (view_grid.sel_end_row < items.length) {
		new_view_grid.set_selection(view_grid.sel_end_row, view_grid.sel_start_col, view_grid.sel_end_row + 1, view_grid.sel_end_col, true);
	}
}

function cancel_cell_edit() {
	close_menu();
	reset_primary_buttons(page_id);
}

function as_num(val) {
	if (is_date(val)) {
		return string_to_days_since_epoch(val);
	}
	return Number(val);
}

function as_str(val, template) {
	if (is_date(template)) {
		const d = new Date(val * 1000 * 60 * 60 * 24);
		return `${String(d.getUTCFullYear())}-${String(d.getUTCMonth() + 1).padStart(2, '0')}-${String(d.getUTCDate()).padStart(2, '0')}`;
	}
	return '' + val;
}

function range_vals(grid, r1, c1, r2, c2) {
	let vals = [];
	for (let r = Math.min(r1, r2); r < Math.max(r1, r2); r++) {
		for (let c = Math.min(c1, c2); c < Math.max(c1, c2); c++) {
			vals.push(as_num(eval_cell(grid, r, c)));
		}
	}
	return vals;
}

function range_sum(grid, r1, c1, r2, c2) {
	let tot = 0.;
	for (let r = Math.min(r1, r2); r < Math.max(r1, r2); r++) {
		for (let c = Math.min(c1, c2); c < Math.max(c1, c2); c++) {
			tot += as_num(eval_cell(grid, r, c));
		}
	}
	return tot;
}

// p = precedence,
// n = number of parameters (-1=one or more, -2=two operator-style)
// d = deterministic, (false means it has a different value every time)
// r = ranged, (true means it operates on a range of cells, and the function has an implicit grid parameter)
// f = function,
const g_ops = {
	'^': { p: 6, n: -2, d: true, r: false, f: (x, y) => Math.pow(as_num(x) ^ as_num(y)) },
	'/': { p: 5, n: -2, d: true, r: false, f: (x, y) => as_num(x) / as_num(y) },
	'%': { p: 4, n: -2, d: true, r: false, f: (x, y) => smart_mod(as_num(x), as_num(y)) },
	'*': { p: 3, n: -2, d: true, r: false, f: (x, y) => as_num(x) * as_num(y) },
	'-': { p: 2, n: -2, d: true, r: false, f: (x, y) => as_num(x) - as_num(y) },
	'+': { p: 1, n: -2, d: true, r: false, f: (x, y) => as_num(x) + as_num(y) },
	abs: { p: 9, n: 1, d: true, r: false, f: x => Math.abs(as_num(x)) },
	acos: { p: 9, n: 1, d: true, r: false, f: x => Math.acos(as_num(x)) },
	acosh: { p: 9, n: 1, d: true, r: false, f: x => Math.acosh(as_num(x)) },
	asin: { p: 9, n: 1, d: true, r: false, f: x => Math.asin(as_num(x)) },
	asinh: { p: 9, n: 1, d: true, r: false, f: x => Math.asinh(as_num(x)) },
	atan: { p: 9, n: 1, d: true, r: false, f: x => Math.atan(as_num(x)) },
	atanh: { p: 9, n: 1, d: true, r: false, f: x => Math.atanh(as_num(x)) },
	c: { p: 9, n: 0, d: false, r: false, f: () => { throw new Error('internal error'); } },
	ceil: { p: 9, n: 1, d: true, r: false, f: x => Math.ceil(as_num(x)) },
	clip: { p: 9, n: 3, d: true, r: false, f: (x, y, z) => Math.min(Math.max(x, y), z) },
	cos: { p: 9, n: 1, d: true, r: false, f: x => Math.cos(as_num(x)) },
	cosh: { p: 9, n: 1, d: true, r: false, f: x => Math.cosh(as_num(x)) },
	e: { p: 9, n: 0, d: true, r: false, f: () => Math.E },
	eq: { p: 9, n: 4, d: true, r: false, f: (a, b, x, y) => a === b ? x : y },
	floor: { p: 9, n: 1, d: true, r: false, f: x => Math.floor(as_num(x)) },
	log: { p: 9, n: 1, d: true, r: false, f: x => Math.log(as_num(x)) },
	lt: { p: 9, n: 4, d: true, r: false, f: (a, b, x, y) => a < b ? x : y },
	lte: { p: 9, n: 4, d: true, r: false, f: (a, b, x, y) => a <= b ? x : y },
	max: { p: 9, n: -1, d: true, r: false, f: () => Math.max(...arguments) },
	maximum: { p: 9, n: 4, d: false, r: true, f: (grid, r1, c1, r2, c2) => {
			return Math.max(...range_vals(grid, r1, c1, r2, c2));
		}
	},
	mean: { p: 9, n: 4, d: false, r: true, f: (grid, r1, c1, r2, c2) => range_sum(grid, r1, c1, r2, c2) / Math.abs((r2 - r1) * (c2 - c1)) },
	min: { p: 9, n: -1, d: true, r: false, f: () => Math.min(...arguments) },
	minimum: { p: 9, n: 4, d: false, r: true, f: (grid, r1, c1, r2, c2) => {
			return Math.min(...range_vals(grid, r1, c1, r2, c2));
		}
	},
	normal: { p: 9, n: 2, d: false, r: false, f: (mean, stdev) => {
			const u = 1 - Math.random(); // Converting [0,1) to (0,1]
			const v = Math.random();
			const z = Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );
			return z * as_num(stdev) + as_num(mean);
		}
	},
	pi: { p: 9, n: 0, d: true, r: false, f: () => Math.PI },
	quantile: { p: 9, n: 5, d: false, r: true, f: (grid, r1, c1, r2, c2, portion) => {
			// Make a sorted list of all non-NaN values in the range
			let vals = range_vals(grid, r1, c1, r2, c2);
			vals.sort((a, b) => Number(a - b));
			while (vals.length > 0 && isNaN(vals[vals.length - 1]))
				vals.pop();

			// Compute the quantile.
			// If portion is 0.5, it will be the median.
			// If portion os 0.25, it will be the first quartile. Etc.
			let pos = Math.max(0, Math.min(vals.length - 1, portion * (vals.length - 1)));
			let lo_pos = Math.floor(pos);
			let hi_pos = Math.ceil(pos);
			return (1 - (pos - lo_pos)) * vals[lo_pos] + (pos - lo_pos) * vals[hi_pos];
		}
	},
	r: { p: 9, n: 0, d: false, r: false, f: () => { throw new Error('internal error'); } },
	sign: { p: 9, n: 1, d: true, r: false, f: x => Math.sign(as_num(x)) },
	sqrt: { p: 9, n: 1, d: true, r: false, f: x => Math.sqrt(as_num(x)) },
	std: { p: 9, n: 4, d: false, r: true, f: (grid, r1, c1, r2, c2) => {
			let vals = range_vals(grid, r1, c1, r2, c2);
			const mean = vals.reduce((acc, x) => acc + x, 0) / vals.length;
			const sumOfSquaredDifferences = vals.reduce((acc, x) => acc.concat((x - mean) ** 2), [])
												.reduce((acc, x) => acc + x, 0);
			return Math.sqrt(sumOfSquaredDifferences / (vals.length - (vals.length > 1 ? 1 : 0)));
		}
	},
	sum: { p: 9, n: 4, d: false, r: true, f: (grid, r1, c1, r2, c2) => range_sum(grid, r1, c1, r2, c2) },
	tan: { p: 9, n: 1, d: true, r: false, f: x => Math.tan(as_num(x)) },
	tanh: { p: 9, n: 1, d: true, r: false, f: x => Math.tanh(as_num(x)) },
	uniform: { p: 9, n: 2, d: false, r: false, f: (min, max) => Math.random() * (as_num(max) - as_num(min)) + as_num(min) },
	v: { p: 9, n: 2, d: false, r: false, f: (row, col) => { throw new Error('internal error'); } },
};

g_math_ops = null;

function find_operator_with_lowest_precedence(s) {
	// Lazily make a table of math operators
	if (!g_math_ops) {
		g_math_ops = new Set();
		for (let op in g_ops) {
			if (g_ops[op].n === -2)
				g_math_ops.add(op);
		}
	}

	// Run through the expression
	let op_precedence = 100;
    let op_desc;
    let op_pos = -1;
	let op_end = -1;
    let paren_nests = 0;
	for (let i = 0; i < s.length; i++) {
		// Ignore parts enclosed in parentheses
        if (s[i] === '(')
            paren_nests += 1;
        else if (s[i] === ')') {
            paren_nests -= 1
            if (paren_nests < 0)
				throw new Error('")" without matching "("');
		}
		if (paren_nests === 0) {
			let j = -1;
			if (g_math_ops.has(s[i])) {
				// Looks like a math operator
				j = i + 1;
			} else if(s[i] >= 'a' && s[i] <= 'z' && (i === 0 || s[i - 1] < 'a' || s[i - 1] > 'z')) {
				// Looks like a named operation
				j = i + 1;
				while(j < s.length && s[j] >= 'a' && s[j] <= 'z')
					j++;
			}
			if (j >= 0) {
				let cand_op = s.substring(i, j);
				let cand_desc = g_ops[cand_op];
				if (cand_desc && cand_desc.p < op_precedence) {
					op_desc = cand_desc;
					op_precedence = cand_desc.p;
					op_pos = i;
					op_end = j
				}
			}
		}
	}
	return {
		desc: op_desc,
		pos: op_pos,
		end: op_end,
	}
}

// Parses the parentheses-bounded comma-separated arguments to a function call, ala "(expr1, expr2, expr3)".
function parse_args(s, op_name, row, col) {
    s = s.trim();
    if (s[0] != '(')
        throw new Error(`Expected a "(" after "${op_name}"`);
    if (s[s.length - 1] != ')')
        throw new Error(`"(" without matching ")" following "${op_name}"`);
    let args = [];
    let nests = 1;
	let start = 1;
	for (let i = 1; i < s.length; i++) {
		if (s[i] === '(') {
			nests += 1;
		} else if (s[i] === ')') {
			nests -= 1;
			if (nests === 0) {
				if (i + 1 !== s.length)
					throw new Error(`Unexpected symbols following arguments for "${op_name}"`);
				let left_expr = s.substring(start, i);
				args.push(parse_expr(left_expr, row, col));
				return args;
			}
		} else if (nests === 1 && s[i] === ',') {
			let left_expr = s.substring(start, i);
			start = i + 1;
			args.push(parse_expr(left_expr, row, col));
		}
	}
    throw new Error(`Imbalanced parenthesis after "${op_name}"`);
}

function parse_expr(s, row, col) {
	s = s.trim();
	let op = find_operator_with_lowest_precedence(s);
    if (op.desc) {
		let op_name = s.substring(op.pos, op.end);
        if (op.desc.n === -2) { // math operator
			let left = s.substring(0, op.pos).trim();
			let right = s.substring(op.pos + 1);
			let left_expr;
            if (s[op.pos] === '-' && left.length === 0)
                left_expr = { typ: 'const', val: 0 }; // treat "-c" as "0 - c"
            else
                left_expr = parse_expr(left, row, col);
            let right_expr = parse_expr(right, row, col);
			let combined = { typ: 'op', name: op_name, args: [ left_expr, right_expr ] };
            if (left_expr.typ === 'const' && right_expr.typ === 'const' && op.desc.d)
                return { typ: 'const', val: eval_tree(combined, null, row, col) }; // reduce the expression now (because we can)
            else
                return combined;
		} else if (op.desc.n == 0) { // named constants
            if (op.end !== s.length)
                throw new Error(`${op_name} is a constant, but the superfluous character "${s[op.end]}" followed it in "${s.substring(op.pos)}"`);
			if (op_name === 'r')
				return { type: 'r' };
			else if (op_name === 'c')
				return { type: 'c' };
            return { typ: 'const', val: op.f() };
		} else { // named operator
			s = s.substring(op.end).trim();
            let args = parse_args(s, op_name, row, col);
            if (op.desc.n > 0 && args.length != op.desc.n)
                throw new Error(`${op_name} expects ${op.desc.n} args. ${args.length} provided.`);
            else if (op.desc.n == -1 && args.length < 1)
                throw new Error(`${op_name} expects at least 1 arg. None were provided.`);
            let combined = { typ: 'op', name: op_name, args: args };
            if (op.desc.d && args.every(x => x.typ === 'const'))
                return { typ: 'const', val: eval_tree(combined, null, row, col) }; // reduce the expression now (because we can)
            else
                return combined;
		}
	} else {
        // Check for empty string
        if (s.length == 0)
            throw new Error('Operator with unexpected lack of arguments');

        // Look for bounding parens. (If there are no operators, then outer parens must be bounding.)
        if (s[0] === '(') {
            if (s[s.length - 1] === ')')
				return parse_expr(s.substring(1, s.length - 1), row, col);
			else
                throw new Error(`"(" without matching ")" in "${s}"`)
		}

		// See if it is a string literal
		if ((s[0] == '\'' && s[s.length - 1] == '\'') ||
			(s[0] == '"' && s[s.length - 1] == '"')) {
			return { type: 'const', val: s.substring(1, s.length - 1) };
		}

        // See if it is a numeric value
		let val = Number(s);
		if (!isNaN(val))
			return { typ: 'const', val: val };

		throw new Error(`Invalid expression: "${s}"`);
	}
}

// Consumes an expression.
// Returns a value it resolves to (or throws an exception if there is a problem).
function eval_tree(node, grid, row, col) {
	if (node.typ === 'op') {
		let op = g_ops[node.name];
		if (!op)
			throw new Error(`Unrecognized operation: ${node.name}`);
		let args = [];
		for (arg_node of node.args) {
			args.push(eval_tree(arg_node, grid, row, col));
		}
		if (node.name === 'v') {
			if (args.length != 2)
				throw new Error('Expected 2 args');
			let r = Math.floor(Number(args[0]) + 0.5);
			let c = Math.floor(Number(args[1]) + 0.5);
			return eval_cell(grid, r, c);
		} else if (op.r) {
			return op.f(grid, ...args);
		} else {
			return op.f(...args);
		}
	} else if (node.typ === 'const') {
		return node.val;
	} else if (node.type === 'r') {
		return row;
	} else if (node.type === 'c') {
		return col;
	} else {
		throw new Error(`Unrecognized expression type: ${e.typ}`);
	}
}

// Consumes cell coordinates.
// Returns the string to display for that cell.
function eval_cell(grid, row, col) {
	// See if the expression has already been evaluated
	let grid_row = grid[smart_mod(row, grid.length)];
	let cell = grid_row[smart_mod(col, grid_row.length)];
	if (cell.value)
		return cell.value;
	if (cell.expr === undefined)
		throw new Error('Expected an expression');
	if (cell.expr.length === 0 || cell.expr[0] !== '=') {
		cell.value = cell.expr;
		return cell.value;
	}

	// Evaluate the expression
	if (cell.ip) // Is evaluation of this cell already in progress?
		throw new Error('cyclical expression');
	cell.ip = true; // Mark this cell as being in progress
	let tree = parse_expr(cell.expr.substring(1), row, col);
	cell.value = eval_tree(tree, grid, row, col);
	cell.ip = false;
	return cell.value;
}

function on_edit_cell_key_down(e, view_index)
{
	if (e.key === 'Enter')
		accept_cell_edit(view_index);
}

// Makes a page for editing a single row of a grid
function make_grid_row_page(s, page_id, item_index, initializers) {
	s.push(`<div id="page_${page_id}_${item_index}" class="bubble">`);
	let page = g_content[page_id];
	if (page === undefined)
	{
		s.push(`No such page: ${page_id}`);
		s.push('</div><br>'); // End of bubble
		return;
	}
	let item = page.items[item_index];
	if (item === undefined)
	{
		s.push(`No such row: ${page_id}:${item_index}`);
		s.push('</div><br>'); // End of bubble
		return;
	}
	let view_index = g_account.view_pages.length;
	g_account.view_pages.push(new ViewGridRow(s, view_index, page_id, item_index, initializers));

	s.push('</div><br>'); // End of bubble
}

async function grid_new_row(page_id)
{
	let view_grid = find_view(page_id);
	let view_index = view_grid.view_index;
	let items = g_content[view_grid.page_id].items;
	let row_index = items.length - 1;
    before_page_change(page_id);
	let item_copy = { ...items[row_index] };

	// Clear all the cells
	let raw_cells = get_raw_cells(item_copy);
	for (let i = 0; i < raw_cells.length; i++)
		raw_cells[i] = '';
	item_copy.text = smart_join(raw_cells);

	item_copy.mod = Date.now();
	items.splice(row_index + 1, 0, item_copy);
	await grid_operation_done();
	let new_view_grid = g_account.view_pages[view_index];
	new_view_grid.set_selection(row_index + 1, 0, row_index + 2, raw_cells.length, true);
	on_grid_edit_row(view_index);
}

class ViewGrid {
	constructor(s, view_index, page_id, opened_index, initializers, search_highlighter) {
		this.view_index = view_index;
		this.page_id = page_id;
		this.sel_start_row = 0;
		this.sel_start_col = 0;
		this.sel_end_row = 0;
		this.sel_end_col = 0;

		// Make sure there is at least one cell
		let items = g_content[page_id].items;
		if (items.length === 0) {
			before_page_change(page_id);
			items.push({
				mod: Date.now(),
				text: '',
			});
		}

		// Make a grid of expressions
		let grid = [];
		let max_cols = 0;
		for(let row_index = 0; row_index < items.length; row_index++) {
			let item = g_content[page_id].items[row_index];
			let raw_cells = get_raw_cells(item);
			const row = [];
			let col_index = 0;
			if (item.file !== undefined) {
				row.push({ expr: '' });
				col_index++;
			}
			for (; col_index < raw_cells.length; col_index++) {
				row.push({ expr: raw_cells[col_index] })
			}
			grid.push(row);
			max_cols = Math.max(max_cols, row.length);
		}

		// Render the values (resolving expressions to values as needed)
		s.push('<div style="width: 100%; overflow-x:scroll;">');
		s.push(`<table id="grid_${page_id}" border="1" style="cursor: cell;">`);
		
		// Add column indices
		s.push('<tr><td></td>');
		for(let col_index = 0; col_index < max_cols; col_index++) {
			s.push(`<td class="highlighted" style="font-size:70%">${col_index}</td>`);
		}
		s.push('</tr>');

		this.value_grid = []; // A grid of values for use with the footers
		for(let row_index = 0; row_index < items.length; row_index++) {
			s.push('<tr>');

			// Add row index
			s.push(`<td class="highlighted" style="font-size:70%">${row_index}</td>`);

			// Add image (if there is one)
			let item = g_content[page_id].items[row_index];
			let grid_row = grid[row_index];
			let col_index = 0;
			if (item.file !== undefined) {
				s.push(`<td><img src="thumb_${item.file}" onclick="on_click_thumbnail(event, '${page_id}', ${row_index})"></td>`);
				col_index++;
			}

			// Add row values
			let value_row = [];
			for (; col_index < grid_row.length; col_index++) {
				let computed = (grid_row[col_index].expr[0] === '=');
				//s.push(`<td onclick="on_click_cell('${view_index}', ${row_index}, ${col_index});" `);
				s.push('<td ');
				s.push(`style="${computed ? 'border-style:solid; border-width:2px' : 'border-style:dotted'};`);
				s.push(`${row_index === 0 ? 'font-weight: bold; text-decoration: underline;' : ''}">`);

				// Compute the value
				let value;
				try {
					value = eval_cell(grid, row_index, col_index);
				} catch(e) {
					value = e;
					log(`Failed to evaluate cell ${row_index},${col_index}: ${e}\n${e.stack}`);
				}

				// Render the value
				value_row.push(value);
				value += '&nbsp'.repeat(Math.max(0, 4 - value.length));
				s.push(value);
				s.push('</td>');
			}
			this.value_grid.push(value_row);
			s.push('</tr>');
		}
		s.push('</table></div>');

		// Action buttons
		s.push(`<span id="new_area_${page_id}">`);
		s.push('<table width="100%"><tr>');
		s.push('<td width="50px"></td><td>'); // Horizontal spacer
		make_grid_bottom_buttons(s, page_id);
		s.push('</td></tr></table>')
		s.push('</span><br>');
		s.push(`<div id="action_menu_${page_id}"></div>`);

		// Bottom-of-page footers
		if (g_content[page_id].footers) {
			make_grid_footers(g_content[page_id].footers, s, this.value_grid, initializers);
		}

		this._setup_event_listeners();
	}

    // This is not a built-in method.
    // It is called explicitly when this view is no longer being used.
    destructor() {
		//this.view.removeEventListener('touchstart', this.touchstart_handler);
		//this.view.removeEventListener('touchend', this.touchend_handler);
		//this.view.removeEventListener('touchmove', this.touchmove_handler);
		window.removeEventListener('mousedown', this.mousedown_handler);
		window.removeEventListener('mouseup', this.mouseup_handler);
		window.removeEventListener('mousemove', this.mousemove_handler);
		window.removeEventListener('keydown', this.keydown_handler);
	}

	// Respond to mouse, touch, and keystrokes
	_setup_event_listeners() {
		//console.log('Setting up event listeners for the grid');
		this.touchstart_handler = (e) => {
			if (e.targetTouches.length == 1) {
			} else if (e.targetTouches.length == 2) {
			}
		};
		//this.view.addEventListener('touchstart', this.touchstart_handler);

		this.touchend_handler = (e) => {
			if (this.mouse_down) {
			}
		};
		//this.view.addEventListener('touchend', this.touchend_handler);

		this.touchmove_handler = (e) => {
			e.preventDefault();
			if (e.targetTouches.length == 2) {
			} else if (e.targetTouches.length == 1) {
			}
		};
		//this.view.addEventListener('touchmove', this.touchmove_handler);

		this.mousedown_handler = (e) => {
			//if (e.target !== this.view)
			//  return;
			if (e.target.nodeName !== 'TD') {
				if (e.target.nodeName === 'TABLE')
					e.preventDefault(); // This prevents broswer selection when the user clicks between cells
				return;
			}
			e.preventDefault();
			let col = e.target.cellIndex - 1;
			let row = e.target.parentNode.rowIndex - 1;
			this.mouse_down = true;
			this.mouse_down_row = row;
			this.mouse_down_col = col;
			this.set_selection(row, col, row + 1, col + 1, false);
		};
		window.addEventListener('mousedown', this.mousedown_handler);

		this.mouseup_handler = (e) => {
			if (!this.mouse_down)
				return;
			this.mouse_down = false;
			if (e.target.nodeName === 'TD') {
				e.preventDefault();
				let col = Math.max(0, e.target.cellIndex - 1);
				let row = Math.max(0, e.target.parentNode.rowIndex - 1);
				let row1 = Math.min(this.mouse_down_row, row);
				let col1 = Math.min(this.mouse_down_col, col);
				let row2 = Math.max(this.mouse_down_row, row);
				let col2 = Math.max(this.mouse_down_col, col);
				this.set_selection(row1, col1, row2 + 1, col2 + 1, true);
			}
		};
		window.addEventListener('mouseup', this.mouseup_handler);

		this.mousemove_handler = (e) => {
			if (!this.mouse_down)
				return;
			if (e.target.nodeName !== 'TD') {
				if (e.target.nodeName === 'TABLE')
					e.preventDefault(); // This prevents broswer selection when the user clicks between cells
				return;
			}
			e.preventDefault();
			let col = Math.max(0, e.target.cellIndex - 1);
			let row = Math.max(0, e.target.parentNode.rowIndex - 1);
			let row1 = Math.min(this.mouse_down_row, row);
			let col1 = Math.min(this.mouse_down_col, col);
			let row2 = Math.max(this.mouse_down_row, row);
			let col2 = Math.max(this.mouse_down_col, col);
			this.set_selection(row1, col1, row2 + 1, col2 + 1, false);
		};
		window.addEventListener('mousemove', this.mousemove_handler);

		this.keydown_handler = (e) => {
			let k = e.key;
		};
		window.addEventListener('keydown', this.keydown_handler);
	}

	set_selection(row1, col1, row2, col2, allow_side_effects) {
		// Put it into the right order
		if (row2 < row1) {
			let tmp = row2;
			row2 = row1;
			row1 = tmp;
		}
		if (col2 < col1) {
			let tmp = col2;
			col2 = col1;
			col1 = tmp;
		}
		if (!allow_side_effects && row1 === this.sel_start_row && col1 === this.sel_start_col && row2 === this.sel_end_row && col2 === this.sel_end_col)
			return;

		// If the user clicked on a column or row index, select the whole column or row
		let items = g_content[this.page_id].items;
		if (row1 < 0) {
			row1 = 0;
			row2 = items.length;
		}
		if (col1 < 0) {
			col1 = 0;
			col2 = 0;
			for (let y = row1; y < row2; y++) {
				let raw_cells = get_raw_cells(items[y], 0);
				col2 = Math.max(col2, raw_cells.length);
			}
		}

		// Check the new selection
		if (row1 === undefined || row1 >= items.length)
			throw new Error(`Invalid start row: ${row1}`);
		if (row2 === undefined || row2 < row1 || row2 > items.length)
			throw new Error(`Invalid end row: ${row2}`);
		if (col1 === undefined)
			throw new Error(`Invalid start col: ${col1}`);
		if (col2 === undefined || col2 < col1)
			throw new Error(`Invalid end col: ${col2}`);

		// Clear selections from cells that are no longer selected
		let grid_table = document.getElementById(`grid_${this.page_id}`);
		for (let y = this.sel_start_row; y < this.sel_end_row; y++) {
			for (let x = this.sel_start_col; x < this.sel_end_col; x++) {
				if (y < row1 || y >= row2 || x < col1 || x >= col2) {
					let cell = grid_table.rows[y + 1].cells[x + 1];
					if (cell)
						cell.classList.remove('selected');
				}
			}
		}

		// Add selection to all the selected cells
		this.sel_start_row = row1;
		this.sel_start_col = col1;
		this.sel_end_row = row2;
		this.sel_end_col = col2;
		for (let y = this.sel_start_row; y < this.sel_end_row; y++) {
			for (let x = this.sel_start_col; x < this.sel_end_col; x++) {
				let cell = grid_table.rows[y + 1].cells[x + 1];
				if (cell)
					cell.classList.add('selected');
			}
		}

		// Do expensive operations based on the new selection
		if (allow_side_effects) {
			// See if one side of the selected region is full and the opposing side empty.
			// If that occurs, we are going to pull in the direction from full to empty.
			// Otherwise, we are going to just fill the entire region with a value.
			let initial_text = '';
			let right_count = 0;
			let left_count = 0;
			let top_count = 0;
			let bot_count = 0;
			for (let y = this.sel_start_row; y < this.sel_end_row; y++) {
				let raw_cells = get_raw_cells(items[y], this.sel_end_col);
				if (y === this.sel_start_row) {
					initial_text = raw_cells[this.sel_start_col];
					if (initial_text.length === 0)
						initial_text = raw_cells[this.sel_end_col - 1];
					for (let x = this.sel_start_col; x < this.sel_end_col; x++)
						top_count += raw_cells[x].length > 0 ? 1 : 0;
				}
				left_count += raw_cells[this.sel_start_col].length > 0 ? 1 : 0;
				right_count += raw_cells[this.sel_end_col - 1].length > 0 ? 1 : 0;
				if (y + 1 === this.sel_end_row) {
					for (let x = this.sel_start_col; x < this.sel_end_col; x++)
						bot_count += raw_cells[x].length > 0 ? 1 : 0;
					if (initial_text.length === 0)
						initial_text = raw_cells[this.sel_start_col];
					if (initial_text.length === 0)
						initial_text = raw_cells[this.sel_end_col - 1];
				}
			}
			let py = 0;
			let px = 0;
			if (bot_count === 0 && top_count > (this.sel_end_col - this.sel_start_col) / 2)
				py = 1; // pull down
			else if (top_count === 0 && bot_count > (this.sel_end_col - this.sel_start_col) / 2)
				py = -1; // pull up
			else if (right_count === 0 && left_count > (this.sel_end_row - this.sel_start_row) / 2)
				px = 1; // pull right
			else if (left_count === 0 && right_count > (this.sel_end_row - this.sel_start_row) / 2)
				px = -1; // pull left
			if (this.sel_end_row - this.sel_start_row < 2 || this.sel_end_col - this.sel_start_col < 2) {
				// Might as well show the value since we have a single column or row
				px = 0;
				py = 0;
			}
			if (px !== 0 && py !== 0)
				throw new Error('pulling two ways should not be possible');

			// Close any open menus
			close_menu();
			g_editing_page = this.page_id;

			// Set up the primary grid buttons
			let s = [];
			make_grid_primary_buttons(s, this.view_index);
			let span_id = 'primary_buttons';
			let item_span = document.getElementById(span_id);
			item_span.innerHTML = s.join('');

			// Set up lower half of title bar
			s = [];
			s.push(`<table width="100%"><tr>`);
			s.push('<td>');
			if (px === 0 && py === 0)
				s.push(`<span class="tool"><input id="edit_cell" type="text" value="${initial_text}" style="width:99%" onkeydown="on_edit_cell_key_down(event, ${this.view_index});"><span class="tooltip">Example formula for the value in the cell above: =v(r-1, c)</span></span>`);
			else {
				s.push('Pull ');
				if (py > 0)
					s.push('down');
				else if(py < 0)
					s.push('up');
				else if(px > 0)
					s.push('right');
				else
					s.push('left');
				s.push('?');
			}
			s.push('</td>');
			s.push(`<td width="150px"><button onclick="accept_cell_edit(${this.view_index})">Ok</button>`);
			s.push(`<button onclick="cancel_cell_edit(${this.page_id})">Cancel</button></td>`);
			s.push(`</tr></table>`);
			span_id = 'title_bar_menu';
			item_span = document.getElementById(span_id);
			if (!item_span) {
				throw new Error(`no element with id: ${span_id}`);
			}
			item_span.innerHTML = s.join('');
			g_menu_div_id = span_id;
			g_menu_callback = bad_menu_state;

			// Select all the text
			if (px === 0 && py === 0) {
				let edit_cell = document.getElementById(`edit_cell`);
				const text_len = edit_cell.value.length;
				edit_cell.setSelectionRange(0, text_len);
				edit_cell.focus();
			} else {
				this.pull_x = px;
				this.pull_y = py;
			}
		}
	}
}
