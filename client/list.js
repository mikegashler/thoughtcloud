// ----------------------------------------------------------------
// The contents of this file are dedicated to the public domain as
// described at http://creativecommons.org/publicdomain/zero/1.0/.
// ----------------------------------------------------------------

function del_item(page_id, item_index) {
	const dest_page_id = g_content[page_id].items[item_index].dest;
	if (dest_page_id && g_content[dest_page_id]) {
		delete g_content[dest_page_id]._parents;
	}
	g_content[page_id].items.splice(item_index, 1);
}

function flash_item(page_id, index, miliseconds) {
	const item_span = document.getElementById(`item_${page_id}_${index}`);
	item_span.classList.add('selected');
	setTimeout(() => {
		item_span.classList.remove('selected');
	}, miliseconds);
}

function action_copy(page_id) {
	let s = [];
	if (g_selected_items.length < 1) {
		s.push(g_content[page_id].thought);
	}
	s.push('\n');
	for (let i = 0; i < g_content[page_id].items.length; i++) {
		if (g_selected_items.length > 0 && !g_selected_items.includes(i)) {
			continue;
		}
		const item = g_content[page_id].items[i];
		const indent = item.ind ?? 0;
		for (let j = 0; j < indent; j++) {
			s.push('\t');
		}
		s.push('* ');
		if(item.text) {
			s.push(item.text);
		}
		s.push('\n');
	}
	const text = s.join('');
	if (!copy_to_clipboard(text)) {
		menu_copy(`action_menu_${page_id}`, text);
	}
}

function action_paste(page_id) {
	menu_paste(`thought_below_${page_id}`, function(text) {
		// Gather the settings
		const have_thought = document.getElementById('have_thought').checked;
		const asterisks = document.getElementById('asterisks').checked;
		close_menu();

		// Parse the text
		before_page_change(page_id);
		const delimiter = asterisks ? '*' : '\n';
		if (have_thought) {
			let end = text.indexOf(delimiter);
			if (end < 0) {
				end = text.length;
			}
			if (g_content[page_id].thought.length > 0) {
				g_content[page_id].thought += '\n';
			}
			const new_thought = text.substring(0, end).trim();
			g_content[page_id].thought += new_thought;
			text = text.substring(end).trim();
		}
		while (text.length > 0) {
			if (text[0] === delimiter) {
				text = text.substring(1);
			}
			let end = text.indexOf(delimiter);
			if (end < 0) {
				end = text.length;
			}
			const item_text = text.substring(0, end).trim();
			g_content[page_id].items.push({ text: item_text });
			text = text.substring(end).trim();
		}
		item_operation_done();
	});
}

function action_bind(page_id) {
	before_page_change(page_id);
	delete g_content[page_id].free;
	update_incoming_links_to_match_thought(page_id);
	render_pages(page_id);
	sync();
}

function action_unbind(page_id) {
	before_page_change(page_id);
	g_content[page_id].free = 't';
	render_pages(page_id);
	sync();
}

function do_item_action(page_id, op) {
	if (g_selected_items.length === 0) {
		alert('Expected at least one items to be selected');
		return;
	}
	if (op === 'bullet') {
		before_page_change(page_id);
		for (let item_index of g_selected_items) {
			delete g_content[page_id].items[item_index].icon;
		}
		item_operation_done();
	} else if (op === 'num') {
		before_page_change(page_id);
		for (let item_index of g_selected_items) {
			g_content[page_id].items[item_index].icon = '#';
		}
		item_operation_done();
	} else if (op === 'check') {
		before_page_change(page_id);
		for (let item_index of g_selected_items) {
			g_content[page_id].items[item_index].icon = '✅';
		}
		item_operation_done();
	} else if (op === 'edit') {
		if (g_selected_items.length > 1) {
			alert('Sorry, this option is not supported for multi-select');
			return;
		}
		if (g_content[page_id].dest && !g_content[page_id].free) {
			alert('This item has a link. You should either edit the destination page or free it from the text of incoming links');
			return;
		}
		let item_index = g_selected_items[0];
		menu_edit_item(page_id, item_index, g_content[page_id].items[item_index].text);
	} else if (op === 'left') {
		before_page_change(page_id);
		let old_selection = [];
		for (let item_index of g_selected_items) {
			old_selection.push(item_index);
			if (g_content[page_id].items[item_index].ind) {
				g_content[page_id].items[item_index].ind--;
				if (g_content[page_id].items[item_index].ind <= 0) {
					delete g_content[page_id].items[item_index].ind;
				}
			}
		}
		clear_selection();
		g_selected_page = page_id;
		for (const itm of old_selection) {
			g_selected_items.push(itm);
		}
		arrow_done();
		show_item_buttons(page_id);
	} else if (op === 'right') {
		before_page_change(page_id);
		let old_selection = [];
		for (let item_index of g_selected_items) {
			old_selection.push(item_index);
			if (g_content[page_id].items[item_index].ind) {
				g_content[page_id].items[item_index].ind++;
			} else {
				g_content[page_id].items[item_index].ind = 1;
			}
		}
		clear_selection();
		g_selected_page = page_id;
		for (const itm of old_selection) {
			g_selected_items.push(itm);
		}
		arrow_done();
		show_item_buttons(page_id);
	} else if (op === 'del') {
		const text = `Are you sure you want to delete ${g_selected_items.length === 1 ? "this item" : "these " + g_selected_items.length + " items"}?`;
		menu_confirm(text, `title_bar_menu`, function(val) {
			before_page_change(page_id);
			g_selected_items.sort((a, b) => b - a); // sort descending
			for (let item_index of g_selected_items) {
				del_item(page_id, item_index);
			}
			item_operation_done();
		});
	} else if (op == 'remove_link') {
		before_page_change(page_id);
		for (let item_index of g_selected_items) {
			let item = g_content[page_id].items[item_index];
			if (item.dest) {
				if (g_content[item.dest]) {
					delete g_content[item.dest]._parents;
				}
				delete item.dest;
			}
		}
		item_operation_done();
	} else if (op === 'clone') {
		alert('temporarily disabled');
		/*
		before_page_change(page_id);
		let item_count = g_selected_items.length;
		for (let item_index of g_selected_items) {
			let item = g_content[page_id].items[item_index];
			g_content[page_id].items.push(clone_item(item, 4));
		}
		await item_operation_done();
		for (let i = 0; i < item_count; i++) {
			flash_item(page_id, g_content[page_id].items.length - 1 - i, 1000);
		}
		*/
	} else if (op === 'move') {
		menu_move(page_id, function(dest_id) {
			close_menu();
			g_selected_items.sort((a, b) => b - a); // sort descending

			// Make sure we're not trying to move into one of the pages we have selected
			let ok = true;
			for (let item_index of g_selected_items) {
				if (g_content[page_id].items[item_index].dest === dest_id) {
					ok = false;
				}
			}
			if (!ok) {
				alert('Cannot move a link into itself!');
				render_pages(page_id);
				return;
			}

			// Move it
			before_page_change(page_id);
			before_page_change(dest_id);
			for (let item_index of g_selected_items) {
				const items = g_content[page_id].items;
				const item_dest = items[item_index].dest;
				if (item_dest && g_content[item_dest]) {
					delete g_content[item_dest]._parents;
				}
				g_content[dest_id].items.push(items[item_index]);
				items.splice(item_index, 1);
			}

			// Update
			clear_selection();
			render_pages(page_id);
			sync();
		});
	} else if (op === 'split') {
		if (g_selected_items.length > 1) {
			alert('Sorry, this option is not supported for multi-select');
		} else {
			let item_index = g_selected_items[0];
			before_page_change(page_id);
			let item = g_content[page_id].items[item_index];
			let text = item.text;
			while(true) {
				let space_index = text.indexOf('\n');
				if (space_index < 0) {
					break;
				}
				item.mod = Date.now();
				item.text = text.substring(0, space_index);
				text = text.substring(space_index + 1);
				g_content[page_id].items.splice(item_index + 1, 0, { text: text });
				item = g_content[page_id].items[item_index + 1];
				item_index += 1;
			}
			item_operation_done();
		}
	} else if (op === 'merge') {
		if (g_selected_items.length < 2) {
			alert('You must select at least two items to merge');
		} else {
			before_page_change(page_id);
			g_selected_items.sort((a, b) => a - b); // sort ascending
			const item_dest = g_content[page_id].items[g_selected_items[0]];
			for (let i = 1; i < g_selected_items.length; i++) {
				const item_src = g_content[page_id].items[g_selected_items[i]];
				item_dest.text += ('\n' + item_src.text);
			}
			for (let i = g_selected_items.length - 1; i >= 1; i--) {
				del_item(page_id, g_selected_items[i]);
			}
			item_operation_done();
		}
	} else {
		alert(`unrecognized item operation ${op}`);
	}
}
/*
function menu_send(div_id, page_count, cb)
{
	close_menu();
	g_menu_div_id = div_id;
	g_menu_callback = cb;
	const s = [];
	s.push('<div class="modal">');
	s.push(`<h3>Send ${page_count} page${page_count === 1 ? '' : 's'} to:</h3><ul>`);
	let account_names = get account names
	for (const account_name of account_names) {
		if (account_name === g_account.username) {
			continue; // no need to send to self
		}
		s.push(`<li><a class="link" href="#javascript:void(0)" onclick="g_menu_callback('${account_name}'); return false;">`);
		s.push(account_name);
		s.push(`</a>`);
		s.push('</li>');
	}
	s.push('</ul>');
	s.push('<button onclick="close_menu()">Cancel</button>');
	s.push('</div><br>');
	let menu_div = document.getElementById(g_menu_div_id);
    menu_div.innerHTML = s.join('');
}
*/
// Called when the user clicks an icon in the item menu.
// Passes the user's choice to whatever callback was used to create the menu.
function on_click_item_action(operation) {
	g_menu_callback(operation);
}

function menu_move(page_id, cb)
{
	let div_id = `action_menu_${g_selected_page}`;
	close_menu();

	// Don't interrupt if someone else edits the page we're on
	g_editing_page = page_id;
	g_editing_index = -2;

	g_menu_div_id = div_id;
	g_menu_callback = cb;
	const s = [];
	s.push('<div class="modal">');
	s.push(`<h3>Move ${g_selected_items.length} item${g_selected_items.length === 1 ? '' : 's'} to...</h3>`);

	// Parents
	const siblings = [];
	const parents = find_parents(page_id);
	if (parents.length > 0) {
		s.push('<h3>Parents of this page:</h3><ul>');
		for (let i = 0; i < parents.length; i++) {
			s.push(`<li><a class="link" href="#javascript:void(0)" onclick="g_menu_callback('${parents[i][0]}'); return false;">${abbreviate(g_content[parents[i][0]].thought)}</a></li>`);
			const sibs = children_ids(parents[i][0]);
			for (let sib of sibs) {
				if (sib !== page_id) {
					siblings.push(sib);
				}
			}
		}
		s.push('</ul>');
	}

	// Siblings
	if (siblings.length > 0) {
		s.push('<h3>Siblings of this page:</h3><ul>');
		for (let i = 0; i < siblings.length; i++) {
			s.push(`<li><a class="link" href="#javascript:void(0)" onclick="g_menu_callback('${siblings[i]}'); return false;">`);
			s.push(abbreviate(g_content[siblings[i]].thought));
			s.push(`</a>`);
			s.push('</li>');
		}
		s.push('</ul>');
	}

	// Children
	const children = children_ids(page_id);
	if (children.length > 0) {
		s.push('<h3>Children of this page:</h3><ul>');
		for (let i = 0; i < children.length; i++) {
			s.push(`<li><a class="link" href="#javascript:void(0)" onclick="g_menu_callback('${children[i]}'); return false;">`);
			s.push(abbreviate(g_content[children[i]].thought));
			s.push(`</a>`);
			s.push('</li>');
		}
		s.push('</ul>');
	}

	// Inject the content
	s.push('</div><br>');
	let menu_div = document.getElementById(g_menu_div_id);
    menu_div.innerHTML = s.join('');
}

function menu_edit_item(page_id, i, initial_text)
{
	close_menu();

	// Set up to refresh the item when this menu is closed (whether accepted or canceled)
	g_close_ops.push(() => {
		g_editing_index = -3;
		render_pages(page_id);
	});
	g_editing_page = page_id;
	g_editing_index = i;
	let item = g_content[page_id].items[i];
	add_edit_text_menu(`item_${page_id}_${i}`, false, initial_text, function(new_text) {
		before_page_change(page_id);
		item.mod = Date.now()
		item.text = new_text;
		item_operation_done();
	});
}

function toggle_item_selection_helper(page_id, i) {
	if (g_selected_page !== page_id) {
		clear_selection();
	}
	g_selected_page = page_id;
	let item_span = document.getElementById(`item_${page_id}_${i}`);
	if (g_selected_items.includes(i)) {
		const index = g_selected_items.indexOf(i);
		if (index >= 0) {
			g_selected_items.splice(index, 1);
			item_span.classList.remove('selected');
		} else {
			log('clicked item not found in selected indexes');
		}
	} else {
		g_selected_items.push(i);
		item_span.classList.add('selected');
	}
}

function toggle_item_selection(page_id, i) {
	toggle_item_selection_helper(page_id, i);
	show_item_buttons(page_id);
}

function on_click_bullet(page_id, i) {
	if (g_editing_index >= -1) {
		if (g_editing_index === i && g_editing_page === page_id) {
			// It's the textarea we are already editing, so don't do anything
			return;
		}
		let text_area = document.getElementById('edit_text');
		if (text_area) {
			let orig = get_original_text_being_edited();
			if (text_area.value.length > 0 && orig !== text_area.value) {
				document.getElementById('edit_text').scrollIntoView({ behavior: 'smooth', block: 'nearest' });
				text_area.focus();
				event.preventDefault();
				return;
			}
		}
	}
	toggle_item_selection(page_id, i);
}

function on_click_item(event, page_id, i) {
	let item = g_content[page_id].items[i];

	// If an edit with changes is in progress, scroll it into view and ignore the click
	if (g_editing_index >= -1) {
		/*if (g_mobile) {
			// Due to the software keyboard, let's require explicit cancels on mobile
			return;
		}*/
		if (g_editing_index === i && g_editing_page === page_id) {
			// It's the textarea we are already editing, so don't do anything
			return;
		}
		let text_area = document.getElementById('edit_text');
		if (text_area) {
			let orig = get_original_text_being_edited();
			if (text_area.value.length > 0 && orig !== text_area.value) {
				log(`Tried to navigate away from a change in progress: "${orig}" !== "${text_area.value}". Editing index=${g_editing_index}. Clicked on page ${page_id}, item ${i}.`);
				document.getElementById('edit_text').scrollIntoView({ behavior: 'smooth', block: 'nearest' });
				text_area.focus();
				event.preventDefault();
				return;
			}
		}
	}

	// If it's a link, follow it
	if (item.dest) {
		render_pages(item.dest).then(() => {
			scroll_to_page(item.dest);
		});
		event.preventDefault();
		return;
	}

	// If it contains spoilers, reveal the first one
	let item_span = document.getElementById(`item_${page_id}_${i}`);
	if (reveal_first_spoiler(item_span)) {
		event.preventDefault();
		return;
	}

	// Edit the item
	menu_edit_item(page_id, i, item.text || '');
}

function make_item(search_highlighter, page_id, i, initializers) {
	let item = g_content[page_id].items[i];
	const s = [];
	s.push(`<span class="${item.dest ? 'link' : ''}" onclick="on_click_item(event, '${page_id}', ${i})">`);
	s.push(search_highlighter.process(item.text));
	if (search_highlighter.is_selected) {
		g_selected_div = `item_menu_${page_id}_${i}`;
	}
	s.push('</span>');
	s.push('<br>');
	s.push(`<span id="item_menu_${page_id}_${i}"></span>`); // span for item menu
	if (item.file !== undefined) {
		s.push(`<img src="thumb_${item.file}" onclick="on_click_thumbnail(event, '${page_id}', ${i})">`);
	}
	return s.join('');
}

// This class derives from the drag-to-reorder tutorial at:
// https://tahazsh.com/blog/seamless-ui-with-js-drag-to-reorder-example
class ViewList {
    constructor(s, view_index, page_id, opened_index, initializers, search_highlighter) {
		this.view_index = view_index;
		this.page_id = page_id;
		this.selection_enabled = true;

        // Render the items
		let items = g_content[page_id].items;
        let counters = [];
		s.push(`<div id="list_${page_id}" class="list" style="width:fit-content">`);
        for(let i = 0; i < items.length; i++) {
            s.push('<div class="list_item is-idle" style="width:fit-content"><table>');
            const indent = items[i].ind ?? 0;
            while (counters.length < indent + 1) {
                counters.push(0);
            }
            while (counters.length > indent + 1) {
                counters.pop();
            }
            counters[indent]++;
            s.push(`<tr><td valign="top" align="right" width="${50 + 75 * indent}px">`);
            s.push('<div class="item_bullet">');
			make_item_bullet(s, items[i], counters);
			s.push('</div>');
            s.push('</td><td valign="top">');
            s.push(`<span id="item_${page_id}_${i}" `);
            if (page_id === g_selected_page && g_selected_items.includes(i)) {
                s.push(' class="selected"');
            } else if (i === opened_index) {
                s.push(' class="opened"');
            }
            s.push('>');
            s.push(make_item(search_highlighter, page_id, i, initializers));
            s.push(`</span>`); // end of item span
            s.push('</td></tr>');
            s.push('</table></div>');
        }
		s.push('</div>');

        // Action buttons
        s.push(`<span id="new_area_${page_id}">`);
        s.push('<table width="100%"><tr>');
        s.push('<td width="50px"></td><td>'); // Horizontal spacer
        make_list_bottom_buttons(s, page_id);
        s.push('</td></tr></table>')
        s.push('</span><br>');
        s.push(`<div id="action_menu_${page_id}"></div>`);

		this.grabbedItem = undefined;
		this.pointerStartX = undefined;
		this.pointerStartY = undefined;
		this.itemsGap = 0;
		this.itemDivs = [];
		this.prevRect = {};
		initializers.push(() => {
			this.listContainer = document.getElementById(`list_${page_id}`);

			// Handle grab and release events
			this.dragStartHandler = (event) => { this.dragStart(event) };
			this.listContainer.addEventListener('mousedown', this.dragStartHandler);
			this.listContainer.addEventListener('touchstart', this.dragStartHandler);
			this.dragEndHandler = (event) => { this.dragEnd(event) };
			document.addEventListener('mouseup', this.dragEndHandler);
			document.addEventListener('touchend', this.dragEndHandler);
		});
    }

    // This is not a built-in method.
    // It is called explicitly when this view is no longer being used.
    destructor() {
		document.removeEventListener('mousedown', this.dragStartHandler);
		document.removeEventListener('touchstart', this.dragStartHandler);
		document.removeEventListener('mouseup', this.dragEndHandler);
		document.removeEventListener('touchend', this.dragEndHandler);
    }

	_allListItems() {
		if (!this.itemDivs?.length) {
		  this.itemDivs = Array.from(this.listContainer.querySelectorAll('.list_item'));
		}
		return this.itemDivs;
	}

	_allIdleItems() {
		return this._allListItems().filter((item) => item.classList.contains('is-idle'));
	}

	// Checks for item.dataset.isAbove
	_isItemAbove(item) {
		return item.hasAttribute('data-is-above');
	}

	// Checks for item.data.isToggled
	_isItemToggled(item) {
		return item.hasAttribute('data-is-toggled');
	}

	_disablePageScroll() {
		document.body.style.overflow = 'hidden';
		document.body.style.touchAction = 'none';
		document.body.style.userSelect = 'none';
	}

	_enablePageScroll() {
		document.body.style.overflow = '';
		document.body.style.touchAction = '';
		document.body.style.userSelect = '';
	}

	dragStart(e) {
		// Grab an item
		if (e.target.classList.contains('item_bullet')) {
			this.grabbedItem = e.target.closest('.list_item');
		}
		if (!this.grabbedItem) {
			return;
		}
		
		e.preventDefault();

		// Store the starting point
		this.pointerStartX = e.clientX || e.touches?.[0]?.clientX;
		this.pointerStartY = e.clientY || e.touches?.[0]?.clientY;

		// Set item gap
		if (this._allIdleItems().length <= 1) {
			this.itemsGap = 0;
		} else {
			//this._disablePageScroll();
			const item1 = this._allIdleItems()[0];
			const item2 = this._allIdleItems()[1];
			const item1Rect = item1.getBoundingClientRect();
			const item2Rect = item2.getBoundingClientRect();
			this.itemsGap = Math.abs(item1Rect.bottom - item2Rect.top);
		}

		// Init the grabbed item
		this.grabbedItem.classList.remove('is-idle');
		this.grabbedItem.classList.add('is-grabbed');

		// Init item state
		const grabbedIndex = this._allListItems().indexOf(this.grabbedItem);
		this._allIdleItems().forEach((item, i) => {
			if (grabbedIndex > i) {
				item.dataset.isAbove = '';
			}
		})

		this.prevRect = this.grabbedItem.getBoundingClientRect();

		this.dragEventHandler = (event) => { this.drag(event) };
		document.addEventListener('mousemove', this.dragEventHandler);
		document.addEventListener('touchmove', this.dragEventHandler, { passive: false });
	}

	drag(e) {
		if (!this.grabbedItem) {
			return;
		}
		e.preventDefault();

		// Get the mouse position relative to the start
		let clientX = e.clientX;
		if (clientX === undefined) {
		  if (e.touches)
			clientX = e.touches[0].clientX;
		  if (clientX === undefined)
			clientX = this.pointerStartX;
		}
		let clientY = e.clientY;
		if (clientY === undefined) {
		  if (e.touches)
			clientY = e.touches[0].clientY;
		  if (clientY === undefined)
			clientY = this.pointerStartY;
		}
		const pointerOffsetX = clientX - this.pointerStartX;
		const pointerOffsetY = clientY - this.pointerStartY;
	
		// Move the dragged item to where the mouse is
		this.grabbedItem.style.transform = `translate(${pointerOffsetX}px, ${pointerOffsetY}px)`;
	
		// Update the state and position of idle items
		const grabbedItemRect = this.grabbedItem.getBoundingClientRect();
		const grabbedItemY = grabbedItemRect.top + grabbedItemRect.height / 2;
	
		// Update state
		this._allIdleItems().forEach((item) => {
		  const itemRect = item.getBoundingClientRect();
		  const itemY = itemRect.top + itemRect.height / 2;
		  if (this._isItemAbove(item)) {
			if (grabbedItemY <= itemY) {
			  item.dataset.isToggled = '';
			} else {
			  delete item.dataset.isToggled;
			}
		  } else {
			if (grabbedItemY >= itemY) {
			  item.dataset.isToggled = '';
			} else {
			  delete item.dataset.isToggled;
			}
		  }
		})
	
		// Update position
		this._allIdleItems().forEach((item) => {
		  if (this._isItemToggled(item)) {
			const direction = this._isItemAbove(item) ? 1 : -1;
			item.style.transform = `translateY(${direction * (grabbedItemRect.height + this.itemsGap)}px)`;
		  } else {
			item.style.transform = '';
		  }
		})
	}

	dragEnd(e) {
		if (!this.grabbedItem) {
			return;
		}

		// Apply new items order
		const reorderedItems = [];
		const reorderedIndexes = [];

		let grabbedIndexBefore = -1;
		let somethingMoved = false;
		this._allListItems().forEach((item, index) => {
			if (item === this.grabbedItem) {
				grabbedIndexBefore = index;
				return;
			}
			if (!this._isItemToggled(item)) {
				reorderedItems[index] = item;
				reorderedIndexes[index] = index;
				return;
			}
			const newIndex = this._isItemAbove(item) ? index + 1 : index - 1;
			reorderedItems[newIndex] = item;
			reorderedIndexes[newIndex] = index;
			somethingMoved = true;
		})

		for (let index = 0; index < this._allListItems().length; index++) {
			const item = reorderedItems[index];
			if (item === undefined) {
				reorderedItems[index] = this.grabbedItem;
				reorderedIndexes[index] = grabbedIndexBefore;
				break;
			}
		}

		reorderedItems.forEach((item) => {
			this.listContainer.appendChild(item);
		})

		this.grabbedItem.style.transform = '';

		requestAnimationFrame(() => {
			const rect = this.grabbedItem.getBoundingClientRect();
			const yDiff = this.prevRect.y - rect.y;
			const currentPositionX = e.clientX || e.changedTouches?.[0]?.clientX;
			const currentPositionY = e.clientY || e.changedTouches?.[0]?.clientY;

			const pointerOffsetX = currentPositionX - this.pointerStartX;
			const pointerOffsetY = currentPositionY - this.pointerStartY;

			this.grabbedItem.style.transform = `translate(${pointerOffsetX}px, ${pointerOffsetY + yDiff}px)`;
			requestAnimationFrame(() => {
				this.grabbedItem.style = null;
				this.grabbedItem.classList.remove('is-grabbed');
				this.grabbedItem.classList.add('is-idle');
				this.grabbedItem = null;
			});
		});

		// Clean up
		this.itemsGap = 0;
		this.itemDivs = [];

		//this._enablePageScroll();

		this._allIdleItems().forEach((item, i) => {
			delete item.dataset.isAbove;
			delete item.dataset.isToggled;
			item.style.transform = '';
		})

		document.removeEventListener('mousemove', this.dragEventHandler);
		document.removeEventListener('touchmove', this.dragEventHandler);
		this.dragEventHandler = null;

		if (somethingMoved) {
			// Make a list of items with the new ordering
			let old_items = g_content[this.page_id].items;
			let new_items = [];
			let item_spans = []; // (also gather the item spans, to be used in the next code block)
			for (let i = 0; i < reorderedIndexes.length; i++) {
				let old_index = reorderedIndexes[i];
				new_items.push(old_items[old_index]);
				item_spans.push(document.getElementById(`item_${this.page_id}_${old_index}`));
			}

			// Adjust the ids on the span items (so item selection will work after reordering)
			before_page_change(this.page_id);
			for (let i = 0; i < reorderedIndexes.length; i++) {
				item_spans[i].id = `item_${this.page_id}_${i}`;
			}

			// Change the item ordering
			g_content[this.page_id].items = new_items;
		} else {
			// Toggle whether this item is selected
			if (this.selection_enabled) { // Work around some bug where touches register twice
				this.selection_enabled = false;
				on_click_bullet(`${this.page_id}`, grabbedIndexBefore);
				setTimeout(() => { this.selection_enabled = true; }, 250);
			}
		}
	}	
}
