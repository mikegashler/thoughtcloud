// ----------------------------------------------------------------
// The contents of this file are dedicated to the public domain as
// described at http://creativecommons.org/publicdomain/zero/1.0/.
// ----------------------------------------------------------------

function init_css_style(dark_mode) {
	if (dark_mode) {
		update_css_rule('body', '{ background: #202020; color: #f0f0f0; border: 0; margin: 0; }');
		update_css_rule('.bubble', '{ width: 88%; padding: 20px; border: solid #305060; border-radius: 50px; background-color: #080808; margin: 0 auto; }');
		update_css_rule('.cached', '{ background-color: #800808; }');
		update_css_rule('button', '{ background-color: #304060; color: white }');
		update_css_rule('.modal', '{ border: solid #304030; border-radius: 25px; background-color: #404050; padding: 20px; line-height: 2.0; }');
		update_css_rule('.selected', '{ background-color: #206090; }');
		update_css_rule('.highlighted', '{ background-color: #204060; }');
		update_css_rule('textarea', '{ width: 100%; height: 62px; max-width: 100%; background-color: #304030; color: white; border-width: 2px; border-radius: 10px; }');
		update_css_rule('.link', '{ color: #6090c0; cursor: pointer; text-decoration: none; }');
		update_css_rule('.link:hover', '{ text-decoration: underline; }');
		update_css_rule('.inactive', '{ color: #505050; }');
	} else {
		update_css_rule('body', '{ background: #f0f0f0; color: #202020; border: 0; margin: 0; }');
		update_css_rule('.bubble', '{ width: 88%; padding: 20px; border: solid black; border-radius: 50px; background-color: #fafafa; margin: 0 auto; }');
		update_css_rule('.cached', '{ background-color: #fa8aaa; }');
		update_css_rule('button', '{ background-color: #ededf1; color: black }');
		update_css_rule('.modal', '{ border: solid black; border-radius: 25px; background-color: #f8f8ff; padding: 20px; line-height: 2.0; }');
		update_css_rule('.selected', '{ background-color: #ffe000; }');
		update_css_rule('.highlighted', '{ background-color: #ffe0c0; }');
		update_css_rule('textarea', '{ width: 100%; height: 62px; max-width: 100%; background-color: #ffffff; color: black; border-width: 2px; border-radius: 10px; }');
		update_css_rule('.link', '{ color: #0040a0; cursor: pointer; text-decoration: none; }');
		update_css_rule('.link:hover', '{ text-decoration: underline; }');
		update_css_rule('.inactive', '{ color: #c0c0c0; }');
	}
}

function toggle_dark_mode() {
	g_account.dark_mode = !g_account.dark_mode;
	init_css_style(g_account.dark_mode);
	render_pages();
}

function toggle_debug_mode() {
	g_account.debug_mode = !g_account.debug_mode;
	render_pages().then(() => {
		if (g_account.debug_mode) {
			scroll_to_page('debug');
		}
	});
}

function close_page(parent_id) {
	render_pages(parent_id).then(() => {
		scroll_to_page(parent_id);
	});
}

function make_thought(search_highlighter, page_id) {
	const s = [];
	s.push(`<span onclick="on_click_thought('${page_id}')">`);
	s.push(search_highlighter.process(g_content[page_id].thought));
	if (search_highlighter.is_selected) {
		g_selected_div = `thought_below_${page_id}`;
	}
	s.push('</span>');
	return s.join('');
}

function refresh_debug_log() {
	if (!g_account.debug_mode) {
		return;
	}
	const el = document.getElementById('debug_log');
	if(!el) {
		return;
	}
	while (g_account.debug_log.length > 1000) {
		g_account.debug_log.shift();
	}
	let s = [];
	for (let i = 0; i < g_account.debug_log.length; i++) {
		s.push(g_account.debug_log[i]);
		s.push('\n');
	}
	el.innerHTML = s.join('');
}

function set_page_view(page_id) {
	let new_mode;
	if (document.getElementById('mode_list').checked)
		new_mode = 'list';
	else if (document.getElementById('mode_grid').checked)
		new_mode = 'grid';
	else if (document.getElementById('mode_tile').checked)
		new_mode = 'tile';
	else if (document.getElementById('mode_pick').checked)
		new_mode = 'pick';
	else if (document.getElementById('mode_regs').checked)
		new_mode = 'regs';
	else if (document.getElementById('mode_checks').checked)
		new_mode = 'checks';
	else {
		console.log('No mode selected!');
		close_menu();
	}
	before_page_change(page_id);
	if (new_mode === 'list')
		delete g_content[page_id].mode;
	else
		g_content[page_id].mode = new_mode;
	render_pages();
}

function menu_page_view(page_id) {
	close_menu();

	let mode = g_content[page_id].mode;
	if (!mode)
		mode = 'list';

	// Make the menu
	g_menu_div_id = `new_area_${page_id}`;
	g_menu_callback = bad_menu_state;
	const s = [];
	s.push('<div class="modal">');
	s.push(`<h3>Page view:</h3>`);
	s.push('<table cellspacing="5px">');
	s.push(`<tr>`);
	s.push(`	<td valign="top"><input style="transform: scale(1.8)" type="radio" id="mode_list" name="page_view" ${mode === 'list' ? 'checked' : ''}></td>`);
	s.push(`	<td valign="top"><label for="mode_list"> list</label></td>`);
	s.push(`	<td>For making lists. For example, this might be good for listing groceries or tasks to complete.</td>`);
	s.push(`</tr>`);
	s.push(`<tr>`);
	s.push(`	<td valign="top"><input style="transform: scale(1.8)" type="radio" id="mode_grid" name="page_view" ${mode === 'grid' ? 'checked' : ''}></td>`);
	s.push(`	<td valign="top"><label for="mode_grid"> grid</label></td>`);
	s.push(`	<td>For making a spreadsheet table. For example, this might be good for collecting data and analyzing with formulas.</td>`);
	s.push(`</tr>`);
	s.push(`<tr>`);
	s.push(`	<td valign="top"><input style="transform: scale(1.8)" type="radio" id="mode_tile" name="page_view" ${mode === 'tile' ? 'checked' : ''}></td>`);
	s.push(`	<td valign="top"><label for="mode_tile"> tile</label></td>`);
	s.push(`	<td>For rapid navigation. For example, this might be good for well-established interior menus.</td>`);
	s.push(`</tr>`);
	s.push(`<tr>`);
	s.push(`	<td valign="top"><input style="transform: scale(1.8)" type="radio" id="mode_pick" name="page_view" ${mode === 'pick' ? 'checked' : ''}></td>`);
	s.push(`	<td valign="top"><label for="mode_pick"> pick</label></td>`);
	s.push(`	<td>For picking daily choices. For example, this might be good for planning meals or outfits.</td>`);
	s.push(`</tr>`);
	s.push(`<tr>`);
	s.push(`	<td valign="top"><input style="transform: scale(1.8)" type="radio" id="mode_regs" name="page_view" ${mode === 'regs' ? 'checked' : ''}></td>`);
	s.push(`	<td valign="top"><label for="mode_regs"> regs</label></td>`);
	s.push(`	<td>For keeping track of regularly-occurring tasks. For example, this might be good for taking out the trash or getting your oil changed.</td>`);
	s.push(`</tr>`);
	s.push(`<tr>`);
	s.push(`	<td valign="top"><input style="transform: scale(1.8)" type="radio" id="mode_checks" name="page_view" ${mode === 'checks' ? 'checked' : ''}></td>`);
	s.push(`	<td valign="top"><label for="mode_checks"> checks</label></td>`);
	s.push(`	<td>For keeping track of daily goals. For example, this might be good for tracking whether you kept your exercise or studying goals.</td>`);
	s.push(`</tr>`);
	s.push('</table>');
	s.push(` <button onclick="set_page_view('${page_id}')">Set view</button>`);
	s.push(' <button onclick="render_pages()">Cancel</button>');
	s.push('</div><br>');
	let menu_div = document.getElementById(g_menu_div_id);
    menu_div.innerHTML = s.join('');
}

function make_debug_page(s) {
	s.push(`<div id="page_debug" class="bubble" style="background-color:#808080;">`);
    s.push('<table width="100%"><tr><td>');
	s.push('	<h3>Debug info</h3>');
	s.push('</td><td width="60px" valign="top">');
	s.push(`<span style="cursor: pointer;" onclick="toggle_debug_mode();">`);
	make_close_button(s);
	s.push('</span>');
	s.push('</td></tr></table>');
	s.push('<br>');
	s.push('<pre id="debug_log" style="width:100%; overflow-x:scroll; background-color:#606060"></pre>');
	s.push('</div><br>'); // End of bubble
	setTimeout(() => { refresh_debug_log(); }, 50);
}

function make_page(s, parent_id, page_id, opened_index, initializers, search_highlighter) {
	//log(`making page id=${page_id}, par=${parent_id}`);
	s.push(`<div id="page_${page_id}" class="bubble${g_content[page_id].cached ? ' cached' : ''}">`);
	if (!(page_id in g_content)) {
		s.push(`<font color="red">Failed to retrieve page ${page_id}!</font>`);
		s.push('</div><br>'); // End of bubble
		return;
	}

	// Render the thought
	s.push('<br>');
	s.push('<table width="100%"><tr><td>');
	s.push(`<span id="thought_area_${page_id}">`);
	s.push(make_thought(search_highlighter, page_id));
	s.push('</span>');
	s.push(`<span id="thought_below_${page_id}"></span>`);
	s.push('<br></td><td width="60px" valign="top">');
	if (parent_id.length > 0) {
		s.push(`<span style="cursor: pointer;" onclick="close_page('${parent_id}')">`);
		make_close_button(s);
		s.push('</span>');
	}
	s.push('</td></tr></table>');

	// Render the page view
	let view_index = g_account.view_pages.length;
	let page_view = g_content[page_id].mode ? g_content[page_id].mode : 'list';
	if (page_view === 'list') {
		g_account.view_pages.push(new ViewList(s, view_index, page_id, opened_index, initializers, search_highlighter));
	} else if (page_view === 'grid') {
		g_account.view_pages.push(new ViewGrid(s, view_index, page_id, opened_index, initializers, search_highlighter));
	} else if (page_view === 'tile') {
		g_account.view_pages.push(new ViewTile(s, view_index, page_id, opened_index, initializers, search_highlighter));
	} else if (page_view === 'pick') {
		g_account.view_pages.push(new ViewPick(s, view_index, page_id, opened_index, initializers, search_highlighter));
	} else if (page_view === 'regs') {
		g_account.view_pages.push(new ViewRegs(s, view_index, page_id, opened_index, initializers, search_highlighter));
	} else if (page_view === 'checks') {
		g_account.view_pages.push(new ViewChecks(s, view_index, page_id, opened_index, initializers, search_highlighter));
	} else
		throw new Error(`Unrecognized view: ${page_view}`);

	s.push('</div><br>'); // End of bubble
}

// Returns the view object with the specified page_id, or null if not found
function find_view(page_id) {
	for (let i = g_account.view_pages.length - 1; i >= 0; i--)
	{
		let view = g_account.view_pages[i];
		if (view.page_id === page_id)
			return view;
	}
	return null;
}

function make_title_bar(s) {
	s.push('<div style="position:fixed; top:0; left:0; width:100%; background:#808080; display:flex; z-index:2">'); // title bar
	s.push(' <div style="float:left">'); // logo box
    make_logo(s);
	s.push(' </div>'); // end logo box
	s.push(' <div style="float:left; width:85%; display:flex; flex-flow:column; justify-content:space-around;">'); // buttons area
	s.push('  <div style="margin:8px"></div>'); // spacer div (to balance the title_bar_menu div)
	s.push('  <div id="primary_buttons"></div>'); // primary buttons
	s.push('  <div id="title_bar_menu" style="margin:8px; width:100%"></div>');
	s.push(' </div>'); // end buttons area
	s.push(' <div style="float:right; width:15%; display:flex; flex-flow:column; justify-content:space-around;">');
	s.push('  <div id="notification_area"></div>') // notification area
	s.push(' </div>');
	s.push('</div>'); // end title bar
}

// All parameters may be undefined.
// If provided, page_id specifies the bottom-most page to render.
// If provided, item_index specifies the item to make a special page for.
// (It will automatically find a stack of pages from the start page to the specified page.)
// If provided, searchHighlighter provides values necessary to highlight search terms.
async function render_pages(page_id, item_index, search_highlighter) {
	// Change to the specified page
	if (page_id) {
		set_current_page(page_id, item_index);
	}

	// Clear stuff
	g_menu_div_id = ''; // no menu open
	g_selected_div = null; // nothing selected
	g_editing_index = -3; // interruptable

	// If we are not logged in, render the login page
	if (!have_identity()) {
		make_login_page();
		return;
	}

	// Make sure we have a stack to render
	if (g_current_page_stack.length < 1) {
		alert('expected set_current_stack or set_current_page to be called before render_pages');
		return;
	}
	let last_page_id = g_current_page_stack[g_current_page_stack.length - 1][0];

	// Make sure we have all the required pages
	let logged_in = await retrieve_pages(g_current_page_stack.map(pair => pair[0]));
	if (!logged_in)
		return; // retrieve_pages has already rendered the login page, so we should just bail now

	// Tear down existing pages
	while (g_account.view_pages.length > 0) {
		let view_page = g_account.view_pages[g_account.view_pages.length - 1];
		view_page.destructor();
		g_account.view_pages.pop();
	}

	// Prepare search highlighters if needed
	let nil_highlighter;
	if (search_highlighter) {
		search_highlighter.reset();
		if (g_current_page_stack.length > 1) {
			nil_highlighter = new SearchHighlighter({}, -1);
		}
	} else {
		nil_highlighter = new SearchHighlighter({}, -1);
		search_highlighter = nil_highlighter;
	}

	// Title bar
	let initializers = [];
	let s = [];
	make_title_bar(s);
	s.push('<br><br><br><br>'); // space under the title bar

	// Make the pages
	for (const [i, entry] of g_current_page_stack.entries()) {
		const parent_id = i > 0 ? g_current_page_stack[i - 1][0] : '';
		let highlighter = i + 1 < g_current_page_stack.length ? nil_highlighter : search_highlighter;
		if (entry[0] in g_content) {
			//log(`about to render page ${entry[0]}: ${JSON.stringify(g_content[entry[0]])}`);
		} else {
			throw new Error(`Expected page ${entry[0]} to have been retrieved already`);
		}
		make_page(s, parent_id, entry[0], entry[1], initializers, highlighter);
	}

	// Make the item page
	if (item_index !== undefined)
	{
		let view = g_content[last_page_id].mode;
		if (view === 'grid')
			make_grid_row_page(s, last_page_id, item_index, initializers);
		else
			make_image_edit_page(s, last_page_id, item_index, initializers);
	}

	if (g_account.debug_mode) {
		make_debug_page(s);
	}

	// Add some newlines so we can scroll the last page up a bit
	s.push('<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>');

	// Render
	let the_content = document.getElementById('the_content');
	the_content.innerHTML = s.join('');

	// Hook up and load the canvases
	for (let initter of initializers) {
		initter();
	}

	// Do post-loading operations
	reset_primary_buttons();
	refresh_debug_log();
	if (g_selected_div) {
		menu_prev_next(g_selected_div, search_highlighter);
	} else if(g_content[last_page_id].thought.length < 1) {
		on_click_thought(last_page_id);
	}
}

function disable_back_button() {
	// Disable one click of the back button
	window.history.pushState({}, '', window.location.href);

	// Disable a second click of the back button
	window.history.pushState({}, '', window.location.href);
}

function init_view() {
	disable_back_button();
	init_css_style(true/*dark mode*/);
}
