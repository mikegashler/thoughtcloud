// ----------------------------------------------------------------
// The contents of this file are dedicated to the public domain as
// described at http://creativecommons.org/publicdomain/zero/1.0/.
// ----------------------------------------------------------------

function pick_next_item(page_id, pick_index) {
	let items = g_content[page_id].items;
	let picks = g_content[page_id].picks;
	let new_pick = picks.items[pick_index] + 1;
	if (new_pick >= items.length)
		new_pick = 0;
	before_page_change(page_id);
	picks.items[pick_index] = new_pick;
	render_pages(page_id);
}

function pick_random_item(page_id, pick_index) {
	let items = g_content[page_id].items;
	let picks = g_content[page_id].picks;
	let new_pick = Math.floor(Math.random() * (items.length - 1));
	if (new_pick >= picks.items[pick_index])
		new_pick++;
	before_page_change(page_id);
	picks.items[pick_index] = new_pick;
	render_pages(page_id);
}

// This view is for picking daily choices, such as outfits or meals.
class ViewPick {
    constructor(s, view_index, page_id, opened_index, initializers, search_highlighter) {
		this.view_index = view_index;
		this.page_id = page_id;
		let today = locale_string_to_date(date_to_locale_string(new Date()));
		let past_days = 5;
		let total_days = 14;

		// Make sure we have a picks field
		let picks = g_content[page_id].picks;
		let picks_changed = false;
		if (picks === undefined) {
			picks_changed = true;
			picks = {
				date: date_to_locale_string(today),
				items: [],
			}
		}

		// Drop old stuff from the picks
		let day_picks = locale_string_to_date(picks.date);
		let day_start = new Date(today);
		day_start.setDate(today.getDate() - past_days);
		let days_behind = Math.round((day_start - day_picks) / (24 * 60 * 60 * 1000));
		if (days_behind > 0) {
			if (!picks_changed) {
				// deep copy the picks since we haven't called before_page_change yet
				picks = { ...picks };
				picks.items = [ ...picks.items ];
			}
			picks_changed = true;
			day_picks.setDate(day_picks.getDate() + days_behind);
			picks.date = date_to_locale_string(day_picks);
			picks.items = picks.items.slice(days_behind);
		}

		// Add new items to the picks as needed
		let items = g_content[page_id].items;
		while (picks.items.length < total_days) {
			if (!picks_changed) {
				// deep copy the picks since we haven't called before_page_change yet
				picks = { ...picks };
				picks.items = [ ...picks.items ];
			}
			picks_changed = true;
			picks.items.push(Math.floor(Math.random() * items.length));
		}

        // Render the picks
		let cm = new CalendarMaker(s);
		cm.beginCalendar();
		let picks_index = 0;
		let prev_day = null;
		for (let picks_index = 0; picks_index < total_days; picks_index++) {
			// Render the date
			let i = picks_index - past_days;
			let day = new Date(today);
			day.setDate(today.getDate() + i);
			cm.beginDay(day, prev_day);

			// Render the item
			let item_index = picks.items[picks_index];
			let picked_item = items[item_index];
			let event_text = picked_item ? picked_item.text : 'Invalid item';
			let hue = hash_text(event_text);
			s.push('<td>');
			s.push(`<div class="event" style="background-color: ${hsvToHex(hue, 0.5, 0.5)}; border: solid ${hsvToHex(hue, 0.5, 0.8)}">${event_text}</div>`);
			s.push('</td>');

			// Render some control buttons
			s.push('<td>');
			s.push(`<button onclick="pick_next_item('${page_id}', ${picks_index})">Next</button>`);
			s.push(`<button onclick="pick_random_item('${page_id}', ${picks_index})">Random</button>`);
			s.push('</td>');
			cm.endDay();
		}
		cm.endCalendar();

		// todo: deal with search highlighters

        // Action buttons
        s.push(`<span id="new_area_${page_id}">`);
        s.push('<table width="100%"><tr>');
        s.push('<td width="50px"></td><td>'); // Horizontal spacer
        make_pick_bottom_buttons(s, page_id);
        s.push('</td></tr></table>')
        s.push('</span><br>');
        s.push(`<div id="action_menu_${page_id}"></div>`);

		// Store changes
		if (picks_changed) {
			before_page_change(page_id);
			g_content[page_id].picks = picks;
		}
    }

    // This is not a built-in method.
    // It is called explicitly when this view is no longer being used.
    destructor() {
    }
}
