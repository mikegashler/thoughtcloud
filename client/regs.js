// ----------------------------------------------------------------
// The contents of this file are dedicated to the public domain as
// described at http://creativecommons.org/publicdomain/zero/1.0/.
// ----------------------------------------------------------------

class CalendarMaker {
	constructor(s) {
		this.s = s;
		this.today = locale_string_to_date(date_to_locale_string(new Date()));
		this.weekdays = ['Sun',"Mon","Tue","Wed","Thu","Fri",'Sat'];
		this.months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	}

	beginCalendar() {
		this.s.push('<table cellspacing="20px">');
	}

	endCalendar() {
		this.s.push('</table>');
	}

	beginDay(day, prev_day=null) {
		// Insert month-name rows as necessary
		if (prev_day && (prev_day.getMonth() != day.getMonth() || prev_day.getFullYear() != day.getFullYear())) {
			let year = prev_day.getFullYear();
			let month = prev_day.getMonth(); // zero-indexed
			month++;
			if (month >= 12) {
				month = 0;
				year++;
			}
			while (year * 12 + month <= day.getFullYear() * 12 + day.getMonth()) {
				this.s.push(`<tr><td colspan="100">${this.months[month]} ${year}</td></tr>`);
				month++;
				if (month >= 12) {
					month = 0;
					year++;
				}
			}
		}

		// Start the day row
		let show_date = (!prev_day || (day - prev_day > 0));
		this.s.push(`<tr>`);
		let hilite = (show_date && (day - this.today === 0)) ? ' style="background-color:#808000"' : '';
		this.s.push(`<td valign="top"${hilite}>`);
		if (show_date) {
			this.s.push(`${this.weekdays[day.getDay()]}<br><big><b>${day.getDate()}</b></big>`);
		}
		this.s.push(`</td>`);
	}

	endDay() {
		this.s.push('</tr>');
	}
}

function accept_new_recurring_task(page_id) {
	let new_text = document.getElementById('recurring_task_text').value;
	let new_cyclelen = Number(document.getElementById('recurring_task_cyclelen').value);
	let new_start = Number(document.getElementById('recurring_task_next').value);
	if (isNaN(new_cyclelen))
		new_cyclelen = 7;
	if (isNaN(new_start))
		new_start = 0;
	let today = locale_string_to_date(date_to_locale_string(new Date()));
	let completion_date = new Date(today);
	completion_date.setDate(today.getDate() - new_cyclelen + new_start);
	let new_item = {
		text: new_text,
		recur_days: new_cyclelen,
		recur_date: date_to_locale_string(completion_date),
	};
	before_page_change(page_id)
	g_content[page_id].items.push(new_item);
	sync();
	clear_selection();
	close_menu();
}

function add_recurring_task(page_id) {
	let new_area_id = `new_area_${page_id}`;
	if (!document.getElementById(new_area_id)) {
		return;
	}
	close_menu();

	// Set up to refresh the item when this menu is closed (whether accepted or canceled)
	g_close_ops.push(() => {
		g_editing_index = -3;
		render_pages(page_id);
	});
	g_editing_page = page_id;
	g_editing_index = g_content[page_id].items.length;
	let item_span = document.getElementById(new_area_id);
	if (!item_span) {
		throw new Error(`no element with id: ${new_area_id}`);
	}
	let s = [];
	s.push(`<table width="100%">`);
	s.push(`<tr><td>Descr: <input type="text" id="recurring_task_text"></input></td></tr>`);
	s.push(`<tr><td>Occurs every: <input type="text" id="recurring_task_cyclelen" size="3" value="7"></input> days</td></tr>`);
	s.push(`<tr><td>starting in: <input type="text" id="recurring_task_next" size="3" value="0"></input> days</td></tr>`);
	s.push('<tr><td><div class="modal">');
	s.push(` <button onclick="accept_new_recurring_task('${page_id}')"><big>&nbsp;Ok&nbsp;</big></button>`);
	s.push(` <button onclick="close_menu()"><big>Cancel</big></button>`);
	s.push('</div><br>');
	s.push(`</td></tr></table>`);
	item_span.innerHTML = s.join('');
	g_menu_div_id = new_area_id;
	let recurring_task_text = document.getElementById(`recurring_task_text`);
	const text_len = recurring_task_text.value.length;
	recurring_task_text.setSelectionRange(text_len, text_len);
	recurring_task_text.focus();
}

function delete_recurring_task(page_id, index) {
	const text = `Are you sure you want to delete this recurring task?`;
	menu_confirm(text, `title_bar_menu`, function(val) {
		before_page_change(page_id);
		del_item(page_id, index);
		render_pages();
	});
}

function regs_toggle_dets(page_id, index) {
	let div = document.getElementById(`regs_details_${page_id}_${index}`);
	if (div.style.display === 'block')
		div.style.display = 'none';
	else
		div.style.display = 'block';
}

function did_recurring_task(page_id, index) {
	// Check form values
	let days_ago = Number(document.getElementById(`days_ago_${page_id}_${index}`).value);
	let cycle_len = Number(document.getElementById(`cycle_len_${page_id}_${index}`).value);
	if (days_ago >= -740 && days_ago <= 740) {} else {
		alert('days_ago should be a number from -740 to 740');
		return;
	}
	if (cycle_len > 0 && cycle_len <= 740) {} else {
		alert('Cycle length should be a number from 1 to 740');
		return;
	}

	// Update when this item was last completed
	let items = g_content[page_id].items;
	let item = items[index];
	let today = locale_string_to_date(date_to_locale_string(new Date()));
	let completion_date = new Date(today);
	completion_date.setDate(today.getDate() - days_ago);
	before_page_change(page_id);
	item.recur_days = cycle_len;
	item.recur_date = date_to_locale_string(completion_date);
	render_pages();
}

function push_back_recurring_task(page_id, index) {
	// Check form values
	let push_back_days = Number(document.getElementById(`push_back_${page_id}_${index}`).value);
	if (push_back_days >= -740 && push_back_days <= 740) {} else {
		alert('push_back_days should be a number from -740 to 740');
		return;
	}

	// Update when this item was last completed
	let items = g_content[page_id].items;
	let item = items[index];
	let recur_date = locale_string_to_date(item.recur_date)
	recur_date.setDate(recur_date.getDate() + push_back_days);
	before_page_change(page_id);
	item.recur_date = date_to_locale_string(recur_date);
	render_pages();
}

const get_next_date = (item) => {
	if (item.recur_date === undefined || item.recur_days === undefined) {
		let today = locale_string_to_date(date_to_locale_string(new Date()));
		return today;
	}
	let prev_date = locale_string_to_date(item.recur_date);
	let next_date = new Date(prev_date);
	next_date.setDate(prev_date.getDate() + Number(item.recur_days));
	return next_date;
};

// This view is for tasks that recur regularly.
// It adds two fields to each item:
// recur_date indicates the date when the task was last completed.
// recur_days indicates the interval betweeen recurrences of the task.
class ViewRegs {
    constructor(s, view_index, page_id, opened_index, initializers, search_highlighter) {
		this.view_index = view_index;
		this.page_id = page_id;

		// Sort by next occurrence (which is not a change worth tracking)
		let today = locale_string_to_date(date_to_locale_string(new Date()));
		let items = g_content[page_id].items;
		items.sort((a, b) => {
			return get_next_date(a) - get_next_date(b);
		});

        // Render the picks
		const weekdays = ['Sun',"Mon","Tue","Wed","Thu","Fri",'Sat'];
		const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		let cm = new CalendarMaker(s);
		cm.beginCalendar();
		let prev_day = null;
		for (let regs_index = 0; regs_index < items.length; regs_index++) {
			let item = items[regs_index];
			let day = get_next_date(item);

			// Add a row for today if needed
			if (regs_index === 0 && day - cm.today > 0) {
				cm.beginDay(cm.today, null);
				s.push('<td colspan="2">All done for today!</td>');
				cm.endDay();
			}

			cm.beginDay(day, prev_day);

			// Make the item text
			s.push(`<td valign="top">`);
			let cycle_days = item.recur_days ? Number(item.recur_days) : 7; // cycle_days

			// Render the item
			let hue = hash_text(item.text);
			s.push(`<div class="event" style="background-color: ${hsvToHex(hue, 0.5, 0.5)}; border: solid ${hsvToHex(hue, 0.5, 0.8)}">${item.text}</div>`);

			// Add form for completion or deletion
			s.push('</td>');
			s.push('<td>');
			let show_completion_button = Math.round((day - cm.today) / (24 * 60 * 60 * 1000)) < cycle_days;
			make_regs_item_form(s, page_id, regs_index, cycle_days, show_completion_button);
			s.push('</td>');
			cm.endDay();
			prev_day = day;
		}
		cm.endCalendar()

		// todo: deal with search highlighters

        // Action buttons
        s.push(`<span id="new_area_${page_id}">`);
        s.push('<table width="100%"><tr>');
        s.push('<td width="50px"></td><td>'); // Horizontal spacer
        make_regs_bottom_buttons(s, page_id);
        s.push('</td></tr></table>')
        s.push('</span><br>');
        s.push(`<div id="action_menu_${page_id}"></div>`);
	}

    // This is not a built-in method.
    // It is called explicitly when this view is no longer being used.
    destructor() {
    }
}
