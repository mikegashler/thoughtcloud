// ----------------------------------------------------------------
// The contents of this file are dedicated to the public domain as
// described at http://creativecommons.org/publicdomain/zero/1.0/.
// ----------------------------------------------------------------

// Returns the length of the longest match from the start of text with anywhere in query.
// Assumes there is known to be at least one match of at least min_len.
function find_longest_match_len(query, text, min_len) {
	const chunk = text.substring(0, min_len);
	const matches = [];
	let pos = 0;
	while(true) {
		const i = query.indexOf(chunk, pos);
		if (i < 0) {
			break;
		}
		matches.push(i);
		pos += 1
	}
	while(true) {
		// Drop all matches that cannot be extended by one character
		for (let i = matches.length - 1; i >= 0; i--) {
			// If we've reached the end, or the next character doesn't match, drop the match
			if (matches[i] + min_len >= query.length || matches[i] >= text.length || query[matches[i] + min_len] !== text[min_len]) {
				matches[i] = matches[matches.length - 1];
				matches.pop();
			}
		}
		if (matches.length < 1) {
			break;
		}
		min_len++;
	}
	return min_len;
}

// This function must be kept in sync with the corresponding region of the SearchHighlighter.process method
function count_fuzzy_matches(text, search_params, fuzzy_set) {
	let fuzzy_len = 0;
	if (!fuzzy_set) {
		console.log('Internal error: expected a fuzzy set')
		return 0;
	}
	for (const chunk of fuzzy_set.values()) {
		fuzzy_len = chunk.length;
		break;
	}

	let pos = 0;
	let match_count = 0;
	while(pos < text.length) { // For each possible starting character
		if (pos + fuzzy_len <= text.length && fuzzy_set.has(text.substring(pos, pos + fuzzy_len))) {
			const match_len = find_longest_match_len(search_params.query, text.substring(pos), fuzzy_len);
			pos += match_len;
			match_count++;
		} else {
			pos++;
		}
	}
	return match_count;
}

function count_whole_page_fuzzy_matches(page_id, search_params, fuzzy_set) {
	let page = g_content[page_id];
	let count = count_fuzzy_matches(page.thought, search_params, fuzzy_set);
	for (let item of page.items) {
		count += count_fuzzy_matches(item.text, search_params, fuzzy_set);
	}
	return count;
}

// Injects highlighting into text for terms the user searched for.
// If no search was performed, this just returns the input text.
class SearchHighlighter {
	// search_params are the parameters used to perform the search.
	// target_index specifies which match should be highlighted. (may be undefined if no search was performed.)
	constructor(search_params, target_index) {
		this.search_params = search_params || {};
		if (this.search_params.fuzzy_set && !this.search_params.query) {
			log('query should be provided with fuzzy_set');
		}

		// Determine the page id and match index
		this.target_index = target_index;
		if (search_params.results && target_index >= 0) {
			let remainder = target_index;
			let page_index = 0;
			while (page_index < search_params.results.pages.length && remainder >= search_params.results.pages[page_index][1]) {
				remainder -= search_params.results.pages[page_index][1];
				page_index++;
			}
			if (page_index >= search_params.results.pages.length) {
				console.log('Internal error: Target index is not compatible with these search results');
				this.page_id = 'start';
				this.page_match = -1;
			} else {
				this.page_id = search_params.results.pages[page_index][0];
				this.page_match = remainder;
			}
		} else {
			//this.page_id = undefined;
			this.page_match = -1;
		}

        this.reset();

		// Determine the chunk size for fuzzy searches. (Assumes all chunks have uniform size.)
		this.fuzzy_len = 0;
		if (this.search_params.fuzzy_set) {
			for (const chunk of this.search_params.fuzzy_set.values()) {
				this.fuzzy_len = chunk.length;
				break;
			}
		}
	}

	render() {
		render_pages(this.page_id, undefined, this);
	}

	prev_match_index() {
		if (this.target_index === 0) {
			return this.search_params.results.count - 1;
		} else {
			return this.target_index - 1;
		}
	}

	next_match_index() {
		if (this.target_index + 1 >= this.search_params.results.count) {
			return 0;
		} else {
			return this.target_index + 1;
		}
	}

	reset() {
		this.match_count = 0;
		this.is_selected = false;
	}

	ensure_not_empty(text) {
		if (text.length === 0) {
			return '<font color="green">Write me</font>';
		} else {
			return text;
		}
	}

	// Returns the text with query matches highlighted.
	process(text) {
		text = inject_spoilers(inject_links(scrub(text)));
		if (!this.search_params.query) {
			return this.ensure_not_empty(text);
		}
		let processed_text = text;
		this.is_selected = false;
		if (this.search_params.fuzzy_set) {
			// Fuzzy-match search. (This region must be kept in sync with the count_fuzzy_matches function.)
			let pos = 0;
			while(pos < processed_text.length) { // For each possible starting character
				if (pos + this.fuzzy_len <= processed_text.length && this.search_params.fuzzy_set.has(processed_text.substring(pos, pos + this.fuzzy_len))) {
					// Got a match. Let's determine how long it is
					const match_len = find_longest_match_len(this.search_params.query, processed_text.substring(pos), this.fuzzy_len);

					// And let's highlight it
					let bef = '';
					let aft = '';
					//log(`Processing match_count=${this.match_count}, page_match=${this.page_match}, text=${processed_text.substring(Math.max(0, pos - 8), 40)}`);
					if (this.match_count === this.page_match) {
						bef = '<span class="selected">';
						aft = '</span>';
						this.is_selected = true;
					} else {
						bef = '<span class="highlighted">';
						aft = '</span>';
					}
					processed_text = `${processed_text.substring(0, pos)}${bef}${processed_text.substring(pos, pos + match_len)}${aft}${processed_text.substring(pos + match_len)}`;
					pos += (bef.length + match_len + aft.length);
					this.match_count++;
				} else {
					pos++;
				}
			}
		} else {
			// Exact-match search
			let pos = 0;
            const prepped_query = this.search_params.case_sensitive ? this.search_params.query : this.search_params.query.toLowerCase();
			while(true) {
				// Find the next match
				const prepped_text = this.search_params.case_sensitive ? processed_text : processed_text.toLowerCase();
				const i = prepped_text.indexOf(prepped_query, pos);
				if (i < 0) {
					break;
				}

				// Got a match. Let's highlight it.
				let bef = '';
				let aft = '';
				//log(`Processing match_count=${this.match_count}, page_match=${this.page_match}, text=${processed_text.substring(Math.max(0, i - 8), 40)}`);
				if (this.match_count === this.page_match) {
					bef = '<span class="selected">';
					aft = '</span>';
					this.is_selected = true;
				} else {
					bef = '<span class="highlighted">';
					aft = '</span>';
				}
				processed_text = `${processed_text.substring(0, i)}${bef}${processed_text.substring(i, i + this.search_params.query.length)}${aft}${processed_text.substring(i + this.search_params.query.length)}`;
				pos = i + (bef.length + this.search_params.query.length + aft.length);
				this.match_count++;
			}
		}
		return this.ensure_not_empty(processed_text);
	} // end of method process
} // end of class SearchHighlighter

// Searches for pages in page_ids that have text that matches the query
function find_exact_matches(query, page_ids, case_sensitive) {
    prepped_query = case_sensitive ? query : query.toLowerCase();
	let match_pages = [];
	const match_set = {};
	let match_count = 0;
	for (let page_id of page_ids) {
		let page = g_content[page_id];

		// Search the thought
		const prepped_thought = case_sensitive ? page.thought : page.thought.toLowerCase();
		let start_pos = 0;
		while (true) {
			let match_pos = prepped_thought.indexOf(prepped_query, start_pos);
			if (match_pos < 0) {
				break;
			}
			match_count++;
			if (!(page_id in match_set)) {
				match_set[page_id] = 1;
				match_pages.push(page_id);
			} else {
				match_set[page_id]++;
			}
			start_pos = match_pos + prepped_query.length;
		}

		// Search the items
		for (let item of page.items) {
			if (!item.text) {
				continue;
			}
			const prepped_text = case_sensitive ? item.text : item.text.toLowerCase();
			let start_pos = 0;
			while (true) {
				let match_pos = prepped_text.indexOf(prepped_query, start_pos);
				if (match_pos < 0) {
					break;
				}
				match_count++;
				if (!(page_id in match_set)) {
					match_set[page_id] = 1;
					match_pages.push(page_id);
				} else {
					match_set[page_id]++;
				}
				start_pos = match_pos + prepped_query.length;
			}
		}
	}
	let result_pages = [];
	for (const page_id of match_pages) {
		result_pages.push([ page_id, match_set[page_id] ]);
	}
	return {
		pages: result_pages, // A list of [page_id, match_count] pairs
		count: match_count, // Total matches
		pos: 0, // The page currently being viewed
	};
}

// Searches for pages containing partial matches of the query
function find_fuzzy_matches(page_ids, search_params, chunk_size, max_matches) {
	const query = search_params.query;

	// Make a set of query chunks
	let query_chunks = new Set();
	for (let i = 0; i + chunk_size <= query.length; i++) {
		query_chunks.add(query.substring(i, i + chunk_size));
	}
	if (query_chunks.size < 1) {
		return [];
	}

	// Find matching pages
	let matching_pages = [];
	for (let page_id of page_ids) {
		let page = g_content[page_id];
		match_count = 0;
		for (let i = 0; i + chunk_size < page.thought.length; i++) {
			if (query_chunks.has(page.thought.substring(i, i + chunk_size))) {
				match_count++;
			}
		}
		for (let item of page.items) {
			for (let i = 0; i + chunk_size < item.text.length; i++) {
				if (query_chunks.has(item.text.substring(i, i + chunk_size))) {
					match_count++;
				}
			}
		}
		if (match_count > 0) {
			matching_pages.push([page_id, match_count]);
		}
	}

	// Pick out the best matches
	matching_pages.sort((a, b) => b[1] - a[1]);
	let accepted_pages = [];
	let accepted_count = 0;
	for (let i = 0; i < matching_pages.length && i < max_matches; i++) {
		let page_id = matching_pages[i][0];
		let page_match_count = count_whole_page_fuzzy_matches(page_id, search_params, query_chunks);
		accepted_pages.push([page_id, page_match_count]);
		accepted_count += page_match_count;
	}
    if (accepted_count > 0) {
        g_search_params.fuzzy_set = query_chunks;
    }
	return {
		pages: accepted_pages, // A list of [page_id, match_count] pairs
		count: accepted_count, // Total matches
		pos: 0, // The page currently being viewed
	}
}

function do_replace(page_id, bef, aft)
{
	before_page_change(page_id);
	g_content[page_id].mod = Date.now();
	g_content[page_id].thought = g_content[page_id].thought.replaceAll(bef, aft);
	let items = g_content[page_id].items;
	for (const itm of items) {
		if (itm.text !== undefined) {
			itm.mod = Date.now();
			itm.text = itm.text.replaceAll(bef, aft);
		}
	}
	sync();
}

function execute_search(page_id)
{
	// Get options
	let on_page_checkbox = document.getElementById('search_option_on_page');
	let replace_checkbox = document.getElementById('search_option_replace');
	let query_field = document.getElementById(`query`);
	let search_params = {
		query: query_field.value,
		fuzzy_set: null,
		case_sensitive: document.getElementById('search_option_case_sensitive').checked,
		fuzzy: document.getElementById('search_option_fuzzy').checked,
		on_page: (on_page_checkbox && on_page_checkbox.checked),
		replace: replace_checkbox && replace_checkbox.checked,
		results: null,
	}

	// find query
	if (search_params.query.length === 0) {
		alert('They empty string would match everything! Maybe you could make your query a little more specific.');
		close_menu();
		return;
	}

	// See if it's a replace
	if (search_params.replace) {
		if (!search_params.on_page) {
			alert('Sorry, only replacing on the current page is presently supported');
			return;
		}
		let replace_with = document.getElementById('replace_with_value').value;
		do_replace(page_id, search_params.query, replace_with);
		close_menu();
		render_pages();
		return;
	}

	g_search_params = search_params;
	const page_ids = (search_params.on_page ? [page_id] : Object.keys(g_content));
	let results = find_exact_matches(search_params.query, page_ids, search_params.case_sensitive);
	if (results.count === 0 && search_params.fuzzy) {
		results = find_fuzzy_matches(page_ids, search_params, 4, 6);
	}
	if (results.count === 0) {
		alert(`Sorry, no ${search_params.case_sensitive ? "case sensitive " : ""}${search_params.fuzzy ? "partial " : ""}matches were found on ${search_params.on_page ? "this" : "any"} page`);
		close_menu();
		return;
	}
	g_search_params.results = results;
	g_menu_callback(g_search_params);
}

function next_match(i) {
	while (g_close_ops.length > 0) {
		g_close_ops.pop();
	}
	let sh = new SearchHighlighter(g_search_params, i);
	sh.render();
}

function menu_prev_next(div_id, search_highlighter)
{
	close_menu();

	// Make sure we reload the whole page when this menu is closed
	g_close_ops.push(() => {
		g_menu_div_id = '';
		render_pages();
	});

	g_menu_div_id = div_id;
    const prev_index = search_highlighter.prev_match_index();
	const next_index = search_highlighter.next_match_index();
	const s = [];
	s.push('<div class="modal">');
	s.push(` <button onclick="next_match(${prev_index})">Prev match</button>`);
	s.push(` <button id="next_match" onclick="next_match(${next_index})">Next match</button>`);
	s.push(` (${search_highlighter.target_index + 1}/${search_highlighter.search_params.results.count})`);
	s.push(' <button onclick="close_menu()">Close</button>');
	s.push('</div><br>');
	let menu_div = document.getElementById(g_menu_div_id);
	menu_div.innerHTML = s.join('');
	let next_button = document.getElementById(`next_match`);
	next_button.focus();
}

function on_change_case_sensitive() {
	let case_sensitive = document.getElementById('search_option_case_sensitive');
	let fuzzy = document.getElementById('search_option_fuzzy');
	let replace = document.getElementById('search_option_replace');
	if (case_sensitive) {
		fuzzy.checked = false;
	} else {
		if (replace) {
			replace.checked = false;
		}
	}
}

function on_change_fuzzy() {
	let case_sensitive = document.getElementById('search_option_case_sensitive');
	let fuzzy = document.getElementById('search_option_fuzzy');
	let replace = document.getElementById('search_option_replace');
	if (fuzzy) {
		case_sensitive.checked = false;
		if (replace) {
			replace.checked = false;
		}
	}
}

function on_change_on_page() {
	let on_page = document.getElementById('search_option_on_page');
	let replace = document.getElementById('search_option_replace');
	if (on_page.checked) {
	} else {
		if (replace) {
			replace.checked = false;
		}
	}
}

function on_change_replace() {
	let case_sensitive = document.getElementById('search_option_case_sensitive');
	let fuzzy = document.getElementById('search_option_fuzzy');
	let replace = document.getElementById('search_option_replace');
	let on_page = document.getElementById('search_option_on_page');
	let replace_span = document.getElementById('replace_with_span');
	if (replace.checked) {
		case_sensitive.checked = true;
		if (fuzzy) {
			fuzzy.checked = false;
		}
		on_page.checked = true;
		replace_span.style.display = 'inline';
	} else {
		replace_span.style.display = 'none';
	}
}

function make_search_controls(page_id, s, allow_on_page, allow_replace) {
	s.push('<input id="query"></input><br>');
	s.push('<input type="checkbox" id="search_option_case_sensitive" onchange="on_change_case_sensitive()"> <label for="search_option_case_sensitive"> &nbsp;Case sensitive</label><br>');
	s.push('<input type="checkbox" id="search_option_fuzzy" onchange="on_change_fuzzy()" checked> <label for="search_option_fuzzy"> &nbsp;Allow partial matches</label><br>');
	if (allow_on_page) {
		s.push('<input type="checkbox" id="search_option_on_page" onchange="on_change_on_page()"> <label for="search_option_on_page"> &nbsp;Search only this page</label><br>');
	}
	if (allow_replace) {
		s.push('<input type="checkbox" id="search_option_replace" onchange="on_change_replace()"> <label for="search_option_replace"> &nbsp;Replace</label>');
		s.push('<span id="replace_with_span" style="display:none"> with <input type="text" id="replace_with_value"></input></span>');
		s.push('<br>');
	}
	s.push(` <button onclick="execute_search('${page_id}')">Search</button>`);
	s.push(' <button onclick="close_menu()">Cancel</button>');
}

// If on_page is true, a check-box will be added for doing on-page search
function menu_search(page_id, div_id, allow_on_page, allow_replace, cb) {
	close_menu();

	// Don't interrupt if someone else edits the page we're on
	g_editing_page = page_id;
	g_editing_index = -2;

	g_close_ops.push(() => {
		render_pages();
	});
	g_menu_div_id = div_id;
	g_menu_callback = cb;
	const s = [];
	s.push('<div class="modal">');
	make_search_controls(page_id, s, allow_on_page, allow_replace);
	s.push('</div><br>');
	let menu_div = document.getElementById(g_menu_div_id);
    menu_div.innerHTML = s.join('');
	let query_box = document.getElementById(`query`);
	query_box.select();
}

function action_search(pid) {
	menu_search(pid, `title_bar_menu`, true, true, function(search_params) {
		let sh = new SearchHighlighter(g_search_params, 0);
		sh.render();
	});
}
