// ----------------------------------------------------------------
// The contents of this file are dedicated to the public domain as
// described at http://creativecommons.org/publicdomain/zero/1.0/.
// ----------------------------------------------------------------

// This file enables the app to work off-line

// In online mode, full network requests will be forwarded to the server.
// When not online, a simple ping will be sent instead, to avoid a lot of wasteful serializing.
// If the ping succeeds, then the full network request will be sent.
const g_origin = self.location.origin;
let g_debug_messages = [];
let g_cache = null;
const g_cache_name = 'thought_cloud_cache';

function log(s) {
	const msg = `${new Date().toISOString().replace('T', ' ')} (sw) ${s}`;
    console.log(msg);
    g_debug_messages.push(msg);
}

// Deletes and reopens the cache
async function flush_cache() {
    await caches.delete(g_cache_name);
    log(`Flushed the cache.`);
    g_cache = await caches.open(g_cache_name);
    const now_time = new Date().getTime();
    return put_cache_by_name('state', 'cache_time', JSON.stringify(now_time));
}

async function open_cache() {
    // See if the cache is already open
    if (g_cache)
        return g_cache;

    // Open the cache
    g_cache = await caches.open(g_cache_name);

    // Flush the cache if it is too old
    const now_time = new Date().getTime();
    const cache_time_str = await fetch_cache_by_name('state', 'cache_time');
    if (cache_time_str) {
        const two_weeks = 1000 * 60 * 60 * 24 * 14;
        const cache_time = JSON.parse(cache_time_str);
        if (now_time - cache_time > two_weeks) {
            log(`The cache is pretty old. Flushing it.`);
            await flush_cache();
        }
    } else {
        put_cache_by_name('state', 'cache_time', JSON.stringify(now_time));
    }
    return g_cache;
}

// Removes an object from the cache
async function cache_delete_by_name(folder, name) {
    const phony_request = new Request(`${g_origin}/${folder}/${name}`);
    const cache = await open_cache();
    return cache.delete(phony_request);
}

// Puts some text into the cache.
// (Overwrites any existing text in the cache with the same name.)
// folder/name = a name that uniquely identifies the object.
// text = the text to cache.
async function put_cache_by_name(folder, name, text) {
    const phony_request = new Request(`${g_origin}/${folder}/${name}`);
    const cache = await open_cache();
    return cache.put(phony_request, new Response(text));
}

// Retrieves text from the cache (or undefined if not found).
// folder/name = a name that uniquely identifies the object.
async function fetch_cache_by_name(folder, name) {
    const phony_request = new Request(`${g_origin}/${folder}/${name}`);
    const cache = await open_cache();
    const response = await cache.match(phony_request);
    if (!response) {
        //log(`${g_origin}/${folder}/${name} not in cache. Returning default text`);
        return undefined;
    }
    return response.text()
}

async function put_state(params) {
    let stack = params.get('stack').split(',');
    log(`putting stack: ${stack}`);
    put_cache_by_name('state', 'state', JSON.stringify({
        username: params.get('username'),
        hash_pri: params.get('hash_pri'),
        hash_pub: params.get('hash_pub'),
        stack: stack,
        dark: params.get('dark') === 't',
    }));
    return new Response(JSON.stringify({status: 'ok'}));
}

async function fetch_state() {
    let s = await fetch_cache_by_name('state', 'state');
    log(`fetching state: ${s}`);
    return new Response(s);
}

// Adds a response to the cache
// Returns a promise that resolves to undefined when the put is complete.
async function cache_put_by_request(request, response) {
    let response_clone = response.clone();
    let cache = await open_cache();
    return cache.put(request, response_clone);
}

// Retrieves from the cache.
// Returns a promise that resolves to undefined if there is no match.
async function fetch_cache_by_request(request) {
    let cache = await open_cache();
    return cache.match(request);
}

/*
// An asynchronous sleep function
const sleep = async (milliseconds) => {
    return new Promise(resolve => setTimeout(() => { resolve('timeout'); }, milliseconds));
}
*/

// List the files to precache
const precacheResources = [
    '/account.js',
    '/action.js',
    '/bullets.ttf',
    '/buttons.js',
    '/canvas.js',
    '/checks.js',
    '/favicon.ico',
    '/footer.js',
    '/grid.js',
    '/index.html', 
    '/list.js',
    '/page.js',
    '/pick.js',
    '/regs.js',
    '/search.js',
    '/sha512.js',
    '/sync.js',
    '/tile.js',
    '/util.js',
];

// When the service worker is installing, open the cache and add the precache resources to it
self.addEventListener('install', (event) => {
    //log('Service worker installing');
    event.waitUntil(open_cache().then((cache) => cache.addAll(precacheResources)));
});

self.addEventListener('activate', (event) => {
    log('Service worker activated');
});

const flush_cache_op = async (event, url) => {
    flush_cache();
    return new Response(JSON.stringify({ status: 'ok' }));
}

// Fetch logs from this service worker
const fetch_service_worker = async (event, url) => {
    let ob = {
        sw: true,
        logs: g_debug_messages,
    };
    g_debug_messages = [];
    return new Response(JSON.stringify(ob));
}

// Fetches from the server
const fetch_server = async (event) => {
    return fetch(event.request);
}

// Try to get from the server. If we get it, cache it.
const get_from_server_then_cache = async (event) => {
    let response;
    try {
        response = await fetch(event.request);
    } catch(e) {
        //log(`Exception in get_from_server_then_cache for ${event.request.url.pathname}: ${e}`);
    }
    if (response && response.ok) {
        cache_put_by_request(event.request, response);
    }
    return response;
}

// Returns true if the url is a file that is likely to be updated.
// Returns false for images and fonts and bloated files for which old cached copies are probably good enough.
function is_worth_updating(url) {
    let ext = url.split('.').pop();
    if (ext === 'js')
        return true;
    else if (ext === 'html')
        return true;
    else if (ext === 'json')
        return true;
    return false;
}

// Tries to get from the cache.
// If there is not match, gets from the server
const fetch_cache_or_server_by_event = async (event) => {
    let response = await fetch_cache_by_request(event.request);
    if (response) {
        // We have a cached copy, so maybe asynchronously request a fresh copy,
        // but immediately return the cached one
        if (is_worth_updating(event.request.url))
            get_from_server_then_cache(event);
        return response;
    } else {
        // It's not in the cache, so let's return the promise for the request sent to the server
        return get_from_server_then_cache(event);
    }
}

// Caches pages as they are retrieved from the server
const fetch_and_cache_pages = async (event) => {
    let response = await fetch_server(event);
    if (!response || !response.ok)
        return response;
    let content = await response.json();
    for (const [page_id, page_content] of content.upd) {
        put_cache_by_name('pages', page_id, JSON.stringify(page_content));
    }
    return new Response(JSON.stringify(content));
}

// Adds everything in "upd" to the cache.
// Deletes everything in "del" from the cache.
const cache_changes = async (packet) => {
    for (const [page_id, page] of packet.upd) {
        put_cache_by_name('pages', page_id, JSON.stringify(page));
    }
    for (const page_id of packet.del) {
        cache_delete_by_name('pages', page_id);
    }
}

// Caches the updates as they are sent to the server.
// Caches updates as they are being sent back to the client.
const sync_and_cache_pages = async (event) => {
    // cache pages being sent to the server
    let original_request = event.request;
    let request_content = await original_request.json();
    cache_changes(request_content);

    // Send the message on to the server
    let content_copy = JSON.stringify(request_content);
    let request_copy = new Request(original_request.url, {
        body: content_copy,
        cache: original_request.cache,
        headers: original_request.headers,
        method: original_request.method,
    });
    let response = await fetch(request_copy);

    // cache pages being sent from the server
    let response_content = await response.json();
    cache_changes(response_content);
    return new Response(JSON.stringify(response_content));
}

// Fetches a page from the cache
const cget = async (url) => {
    let params = new URLSearchParams(url.search);
    let ids = params.get('ids').split(',');
	let promise_responses = [];
    for (let id of ids) {
        promise_responses.push(fetch_cache_by_name('pages', id));
    }
    let cached_pages = await Promise.all(promise_responses);
    return new Response(JSON.stringify(cached_pages)); // these pages are now double-stringified
}

// Most fetch requests just pass right through.
// But we intercept some specific requests and try to make them faster with caching.
self.addEventListener('fetch', async (event) => {
    let promise_response;
    if (event.request.method === 'GET') {
        let url = new URL(event.request.url);
        if (url.pathname === '/cget.sw') {
            promise_response = cget(url);
        } else if (url.pathname === '/set_state.sw') {
            promise_response = put_state(new URLSearchParams(url.search));
        } else if (url.pathname === '/get_state.sw') {
            promise_response = fetch_state();
        } else if (url.pathname === '/flush_cache.sw') {
            promise_response = flush_cache_op();
        } else {
            //log(`fetch intercepted for ${url.pathname}`);
            promise_response = fetch_cache_or_server_by_event(event);
        }
    } else {
        let url = new URL(event.request.url);
        if (url.pathname === '/get.ajax') {
            promise_response = fetch_and_cache_pages(event);
        } else if (url.pathname === 'sync.ajax') {
            promise_response = sync_and_cache_pages(event);
        } else {
            //log(`fetch intercepted for ${url.pathname}`);
            promise_response = fetch_server(event);
        }
    }
    event.respondWith(promise_response);
});

log('Service worker running');
