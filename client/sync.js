// ----------------------------------------------------------------
// The contents of this file are dedicated to the public domain as
// described at http://creativecommons.org/publicdomain/zero/1.0/.
// ----------------------------------------------------------------

function global_undo() {
	if (g_account.undo_pos <= 0) {
		alert('Reached the beginning of the undo history');
		return;
	}
	if (g_account.undo_pos >= g_account.undo_history.length) {
		// Add the current state to the end of the undo history
		let page_id = g_account.undo_history[g_account.undo_history.length - 1][0];
		let clone = structuredClone(g_content[page_id]);
		g_account.undo_history.push([page_id, clone]);
	}

	// Restore the previous page
	g_account.undo_pos--;
	let page_id = g_account.undo_history[g_account.undo_pos][0];
	let page_content = g_account.undo_history[g_account.undo_pos][1];
	g_content[page_id] = page_content;

	// Render the restored page
	render_pages(page_id);
}

function global_redo() {
	if (g_account.undo_pos >= g_account.undo_history.length - 1) {
		alert('Reached the latest change');
		return;
	}

	// Restore the next change
	g_account.undo_pos++;
	let page_id = g_account.undo_history[g_account.undo_pos][0];
	let page_content = g_account.undo_history[g_account.undo_pos][1];
	g_content[page_id] = page_content;

	// Render the restored page
	render_pages(page_id);
}

// Called before any page is modified so we can remember the old content (in case the server rejects our changes)
function before_page_change(page_id) {
	if (page_id === '0' || page_id === '1' || page_id === '2') {
		let msg = 'Bad page id. This probably indicates that a list was being used like an object somewhere. Rejecting the change!';
		log(msg);
		alert(msg);
		return;
	}
	if (page_id in g_content) {
		// The page is about to be modified or deleted
		delete g_content[page_id]._parents;
		let clone = structuredClone(g_content[page_id]);
		g_account.undo_history = g_account.undo_history.slice(0, g_account.undo_pos);
		g_account.undo_history.push([page_id, clone]);
		g_account.undo_pos = g_account.undo_history.length;
		if (!(page_id in g_content_old_unsent)) {
			g_content_old_unsent[page_id] = clone;
		}
	} else {
		// The page is about to be added
		g_account.undo_history = g_account.undo_history.slice(0, g_account.undo_pos);
		g_account.undo_history.push([page_id, null]);
		g_account.undo_pos = g_account.undo_history.length;
		if (!(page_id in g_content_old_unsent)) {
			g_content_old_unsent[page_id] = {};
		}
	}
}

function encrypt(page_id, page_content) {
	let encoder = new TextEncoder();
	let rev = page_content.rev;
	delete(page_content.rev);
	const bytes = encoder.encode(JSON.stringify(page_content));
	const salt = g_account.hash_pri.slice(0, 32);
	const pad = make_pad(bytes.length, `tc_${g_account.username}_${salt}_${page_id}`);
	for (let i = 0; i < bytes.length; i++) {
		bytes[i] = bytes[i] ^ pad[i];
	}
	let con = uint8ArrayToBase64String(bytes);
	return {
		rev: rev,
		con: con,
	};
}

function decrypt(page_id, page) {
	if (!page.con) {
		log(`Problematic page: ${JSON.stringify(page)}`);
	}
	if (typeof page.con !== 'string') {
		//log(`Page ${page_id} was not encrypted: ${page_content.thought}`);
		page.con.rev = page.rev;
		return page.con;
	}
	let bytes = base64StringToUint8Array(page.con);
	try {
		const salt = g_account.hash_pri.slice(0, 32);
		const pad = make_pad(bytes.length, `tc_${g_account.username}_${salt}_${page_id}`);
		for (let i = 0; i < bytes.length; i++) {
			bytes[i] = bytes[i] ^ pad[i];
		}
		const decoder = new TextDecoder();
		const json_string = decoder.decode(bytes);
		let parsed = JSON.parse(json_string);
		parsed.rev = page.rev;
		return parsed;
	} catch(ex) {
		// Deprecated code:
		// Since decoding with the private key didn't work, let's try
		// decoding with the public key, which is no longer used for encryption.
		bytes = base64StringToUint8Array(page.con);
		const salt = g_account.hash_pub.slice(0, 32);
		const pad = make_pad(bytes.length, `tc_${g_account.username}_${salt}_${page_id}`);
		for (let i = 0; i < bytes.length; i++) {
			bytes[i] = bytes[i] ^ pad[i];
		}
		const decoder = new TextDecoder();
		const json_string = decoder.decode(bytes);
		let parsed = JSON.parse(json_string);
		parsed.rev = page.rev;
		before_page_change(page_id);
		return parsed;
	}
}

// Returns a list of indexes that map from bef items to aft items, prioritizing best matches.
// Some elements may be -1 if there are more items in bef than aft.
function match_items(bef, aft) {
	const costs = [];
	for (let i = 0; i < bef.length; i++) {
		const row = [];
		for (let j = 0; j < aft.length; j++) {
			row.push(string_distance(bef[i].text, aft[j].text));
		}
		costs.push(row);
	}
	results = greedy_matches(costs);
	if (results.length !== bef.length) {
		log('Bad results returned from greedy_matches!');
	}
	for (let i = 0; i < results.length; i++) {
		if (results[i] < -1 || results[i] >= aft.length) {
			log('Bad value returned from greedy_matches!');
		}
	}
	return results;
}

// Merges contents of the text area (that the user is currently working on) back in with the content that was just updated
function merge_editing_page(orig) {
	let remote = g_content[g_editing_page]; // The content from the remote user
	let text_area = document.getElementById('edit_text');
	if(!text_area) {
		log('Failed to find the textarea during a merge conflict on the editing page. Reloading the page.');
		render_pages();
		return;
	}
	if (g_editing_index === -1) { // The user is editing the thought
		//log(`merge_editing_page thought: g_editing_index=-1, orig='${JSON.stringify(orig, null, 2)}', remote='${JSON.stringify(remote, null, 2)}', text_area.value='${text_area.value}'`);
		const merged = three_way_string_merge(orig.thought, remote.thought, text_area.value);
		log(`merged='${JSON.stringify(merged)}'`);
		render_pages().then(() => {
			on_click_thought(g_editing_page); // reopen the edit menu
			text_area = document.getElementById('edit_text');
			if(!text_area) {
				log('Failed to find the textarea for the thought after refreshing the page.');
				return;
			}
			text_area.value = merged;
			text_area.setSelectionRange(merged.length, merged.length);
		});
	} else if (g_editing_index >= 0) { // The user is editing an item
		//log(`merge_editing_page item: g_editing_index >= 0, orig='${JSON.stringify(orig, null, 2)}', remote='${JSON.stringify(remote, null, 2)}', text_area.value='${text_area.value}'`);
		// Determine how the items align
		const matches = match_items(orig.items, remote.items);
		const new_index = matches[g_editing_index];
		if (g_editing_index >= orig.items.length) {
			return; // The item being edited is new, so let's not interrupt the user.
		}
		if (new_index < 0) {
			return; // The item being edited was not touched, so let's not interrupt the user.
		}
		if (new_index >= remote.items.length || remote.items[new_index] === undefined) {
			log(`Error: bad value for new_index: ${new_index}. Only have ${remote.items.length} items!`);
			return;
		}

		// merge the text
		log('about to do a three-way-string-merge');
		console.log(`---abt to do 3-way sm: g_editing_index=${g_editing_index}, new_index=${new_index}`);
		const merged = three_way_string_merge(orig.items[g_editing_index].text, remote.items[new_index].text, text_area.value);
		log(`merged='${JSON.stringify(merged)}'`);
		render_pages().then(() => {
			menu_edit_item(g_editing_page, new_index, merged);
			text_area = document.getElementById('edit_text');
			if(!text_area) {
				log('Failed to find the textarea for the item after refreshing the page.');
				return;
			}
			text_area.value = merged;
			text_area.setSelectionRange(merged.length, merged.length);
			log('done merging items');
		});
	} else {
		throw new Error('unexpected value in g_editing_index');
	}
}

// Merges local changes that were rejected back into the content
function merge_page(page_id, orig, remote, local) {
	log(`Merging orig='${JSON.stringify(orig, null, 2)}', remote='${JSON.stringify(remote, null, 2)}', local='${JSON.stringify(local, null, 2)}'`);
	before_page_change(page_id);

	// Merge the thought
	g_content[page_id].mod = Date.now();
	g_content[page_id].thought = three_way_string_merge(orig.thought, remote.thought, local.thought);

	// Prepare to merge the items
	const matches_remote = match_items(orig.items, remote.items);
	const matches_local = match_items(orig.items, local.items);
	let remote_indexes = new Set();
	for (let i = 0; i < remote.items.length; i++) {
		remote_indexes.add(i);
	}
	let local_indexes = new Set();
	for (let i = 0; i < local.items.length; i++) {
		local_indexes.add(i);
	}

	// Merge all the items that can be traced back to the origin
	for (let orig_index = 0; orig_index < matches_remote.length; orig_index++) {
		const remote_index = matches_remote[orig_index];
		const local_index = matches_local[orig_index];
		g_content[page_id].items[remote_index].mod = Date.now();
		g_content[page_id].items[remote_index].text = three_way_string_merge(orig.items[orig_index].text, remote.items[remote_index].text, local.items[local_index].text);
		remote_indexes.delete(remote_index);
		local_indexes.delete(local_index);
	}

	// Merge any remaining items
	for (let local_index of local_indexes) {
		// Search for a matching remote item in the remainder
		let matched = false;
		for (let remote_index of remote_indexes) {
			if (remote.items[remote_index].text === local.items[local_index].text) {
				matched = true;
				remote_indexes.delete(remote_index);
				break;
			}
		}

		// Add as a new item
		if (!matched) {
			g_content[page_id].items.push(local.items[local_index])
		}
	}
}

// Returns a list of paths from from 'start' to 'page_id'.
// Stops searching after 'max_iters' iterations.
function find_stacks(page_id, item_index=-1, max_iters=100) {
	const stacks = [];
	const q = [[[page_id, item_index]]]; // outer list holds all candidate stacks. middle list holds a candidate stack. inner list is [page, link_index]
	if (page_id === 'start') {
		return q;
	}
	const been_there = new Set();
	for (let i = 0; i < max_iters; i++) {
		const cur = q.shift();
		if (!cur)
			break;
		if (!been_there.has(cur)) {
			been_there.add(cur);
			const pars = find_parents(cur[cur.length - 1][0]);
			for (const par of pars) {
				const cand_path = [ ...cur, par ];
				if (par[0] === 'start') {
					stacks.push(cand_path.reverse());
					return stacks; // This line tells it to return when the first stack is found. If you remove this line, it will return all stacks.
				} else {
					q.push(cand_path);
				}
			}
		}
	}
	return stacks;
}

async function cache_current_state() {
	if (g_account.username.length < 1 || !g_account.hash_pri || !g_account.hash_pub)
		throw new Error('invalid state');
	let pages_only = g_current_page_stack.map(x => x[0]);
	let params = new URLSearchParams();
	params.append('username', g_account.username);
	params.append('hash_pri', g_account.hash_pri);
	params.append('hash_pub', g_account.hash_pub);
	//log(`caching stack: ${JSON.stringify(pages_only)}`);
	params.append('stack', pages_only.join(','));
	params.append('dark', g_account.dark_mode ? 't' : 'f');
	if (g_sw_cache_working) {
		try {
			await httpGet(`set_state.sw?${params.toString()}`);
		} catch(ex) {
			log(ex);
			g_sw_cache_working = false;
		}
	}
}

async function load_current_state() {
	let loaded = false;
	try {
		let state = await httpGet(`get_state.sw`);
		g_account.username = state.username;
		g_account.hash_pri = state.hash_pri;
		g_account.hash_pub = state.hash_pub;
		g_current_page_stack = state.stack.map(x => [x, -1]);
		g_account.dark_mode = state.dark;
		loaded = true;
	} catch(ex) {
		console.log(`Failed to load state: ${ex}`);
	}
	if (!loaded) {
		g_current_page_stack = [['start', -1]];
		g_account.dark_mode = true;
	}
}

function set_current_stack(stack) {
	g_current_page_stack = stack;
	if (have_identity())
		cache_current_state();
	g_selected_items = [];
}

function set_current_page(page_id, item_index=-1) {
	let stacks = find_stacks(page_id, item_index);
	if (stacks.length > 0) {
		set_current_stack(stacks[0]);
	} else {
		set_current_stack([[page_id, item_index]]);
	}
}

function is_page_in_stack(page_id, stack) {
	for (const page_item of stack) {
		if (page_item[0] === page_id) {
			return true;
		}
	}
	return false;
}

function receive_logs(logs) {
	if (!logs) {
		return;
	}
	let hint_index = g_account.debug_log.length; // The end of the list is probably a resonable guess
	for (let i = 0; i < logs.length; i++) {
		let s = logs[i];
		console.log(s);
		hint_index = insert_log(s, hint_index);
	}
	if (logs.length > 0) {
		refresh_debug_log();
	}
}

// Return true iff the current page was updated
function receive_updates(response, reload_if_stack_changes) {
	let editing_page_orig;
	let merges_needed = {};
	for (const condemned of response.del) {
		log(`remote user deleted page ${condemned}`);
		if (condemned === g_editing_page) {
			log('...and that is the page you are looking at!');
			editing_page_orig = g_content[g_editing_page];
		}
		delete g_content[condemned];
	}
	let reload = false;
	for (const [key, value] of response.upd) {
		const decrypted_content = decrypt(key, value);
		// Handle collisions
		if (key in g_content_old_sent) { // At this moment, g_content_old_sent contains keys for content that the server rejected for being out-of-date.
			log(`Got an update collision on page ${key}`);
			merges_needed[key] = g_content[key]; // Hold on to our copy of this page so we can merge it back in
		}
		if (key === g_editing_page && g_editing_index >= -1) {
			log('It is the page you are editing, so merging into the text area');
			// Grab the oldest copy of the page being edited we have on record
			if (g_editing_page in g_content_old_sent) {
				editing_page_orig = g_content_old_sent[g_editing_page];
			} else if (g_editing_page in g_content_old_unsent) {
				editing_page_orig = g_content_old_unsent[g_editing_page];
			} else {
				editing_page_orig = g_content[g_editing_page];
			}
		}

		// Make the updates
		g_content[key] = decrypted_content;
		if (!reload && reload_if_stack_changes && is_page_in_stack(key, g_current_page_stack)) {
			reload = true;
		}
	}

	// Merge any changes that were rejected by the server back into our copy of the content
	for (const key in merges_needed) {
		log(`Merging rejected changes back in for ${key}`);
		merge_page(key, g_content_old_sent[key], g_content[key], merges_needed[key]);
		if (!reload && reload_if_stack_changes && is_page_in_stack(key, g_current_page_stack)) {
			reload = true;
		}
	}

	// Merge any changes in a text area being edited back into our copy of the content
	if (editing_page_orig) {
		log('Merging your active edits back into the update');
		merge_editing_page(editing_page_orig);
		reload = false;
	}

	// Update the revision number and last sync time
	if (response.rev)
		g_account.revision = response.rev;
	let now = new Date();
	g_account.last_sync_in_time = now;
	if (response.tim) { // the ".tim" field is only included in responses to syncs
		// Check for clock skew. todo: why even check for this? Is the client time ever used for anything?
		if (Math.abs(Date.parse(response.tim) - now) > 8_000) {
			log(`Clock skew! Server time: ${new Date(Date.parse(response_obj.tim))}, Client time: ${now}`);
		}
	}

	// Reload if necessary
	if (reload && g_editing_index < -2) {
		let current_page_id = g_current_page_stack[g_current_page_stack.length - 1][0];
		if (!(current_page_id in g_content)) { // If the remote user deleted the current page, throw the user back to the start
			set_current_page('start');
		}
		render_pages();
	}
}

// A JSON-parsed response object will be passed to the callback
async function httpGet(url)
{
	let response = await fetch(url, {
		method: "GET",
		cache: "no-cache",
		headers: { 'Brownie': `sid=${g_session_id}` },
	});
	if (!response.ok)
		throw new Error(`in httpGet(${url}): ${response.statusText}`);

	// let text = await response.text();
	// log(`httpGet(${url}) obtained text ${text}`);
	// return JSON.parse(text);

	return response.json();
}

// Payload is a marshaled (but not JSON-stringified) object
// A JSON-parsed response object will be passed to the callback
async function httpPost(url, payload)
{
	const body = JSON.stringify(payload);
	let response = await fetch(url, {
		method: "POST",
		body: body,
		cache: "no-cache",
		headers: {
			'Content-Type': 'application/json',
			'Brownie': `sid=${g_session_id}`,
		},
	});
	if (!response.ok)
		throw new Error(`in httpPost(${url}): ${response.statusText}`);

	// let text = await response.text();
	// log(`httpPost(${url}) obtained text ${text}`);
	// return JSON.parse(text);

	return response.json();
}

// This is called when we retrieve log messages from the service worker
function sw_callback(response_obj) {
	//log(`Service worker ${response_obj.sw ? "still responding" : "stopped"}`);
	receive_logs(response_obj.logs);
}

// Send changes to the server, and request any updates from other clients
function sync() {
	// No need to sync if the user is not logged in
	if (!g_account.logged_in) {
		return;
	}

	// Update the warning system
	let now = new Date();
	const changed_page_count = Object.keys(g_content_old_unsent).length;
	const time_since_last_network_response = now - g_account.last_sync_in_time;
	update_network_status_icon(changed_page_count, time_since_last_network_response);

	// Make sure we haven't sync'd too recently. We don't want to hammer the server
	const min_milis_between_syncs = (changed_page_count === 0 ? 12_000 : 4_000); // Faster when changes are happening. Slower when they're not.
	if (now - g_account.last_sync_out_time < min_milis_between_syncs) {
		//log('Aborting sync because we synced very recently');
		return;
	}

	// If a prior sync is still pending, just skip this one so we never have more than one pending sync at-a-time.
	// (This is not a problem. It will happen sometimes because our interval timer may
	// trigger a sync immediately after a manual change already triggered a sync.)
	if (Object.keys(g_content_old_sent).length > 0) {
		if (Date.now() - g_sent_time > 10_000) {
			log(`A sync that was sent more than 10 seconds ago has not yet been acknowledged!`);
			if (g_account.debug_mode) {
				//httpPost(`${g_origin}/sw.ajax`, {}).then(sw_callback);
			}
		}
		//log('Aborting sync because a prior sync is still pending');
		return;
	}

	// Gather lists of updates and deletions
	g_account.last_sync_out_time = now;
	let del = [];
	let upd = [];
	for (const page_id in g_content_old_unsent) {
		if (page_id in g_content) {
			const page_content = { ...g_content[page_id] }; // Copy the changed page (so we can remove client-side annotations)
			delete page_content._parents; // This is a client-side only annotation
			//log(`Sending page content for sync: ${JSON.stringify(page_content)}`);
			upd.push([page_id, encrypt(page_id, page_content)]);
		} else {
			del.push(page_id);
		}
	}

	// Send it to the server
	const payload = {
		rev: g_account.revision,
		del: del,
		upd: upd,
	};
	httpPost(`${g_origin}/sync.ajax`, payload).then(response_obj => {
		receive_logs(response_obj.logs);
		if (response_obj.status === 'received') {
			// Receive updates
			if (response_obj.rej === 0) {
				if (response_obj.acc > 0) {
					// Update the revision timestamps for all updated pages
					for (up of upd) {
						let page_id = up[0];
						//log(`page ${page_id} successfully updated`);
						g_content[page_id].rev = response_obj.rev;
					}

					// Reset the logo (to indicate there are no longer any pending changes)
					update_network_status_icon(0, 0);
				}
				g_content_old_sent = {};
			} else {
				let msg = `Some changes were rejected for modifying outdated pages.`;
				msg += ` (This is not necessarily a problem. I will attempt to merge and send them again with the next sync.)`;
				msg += ` Prev rev=${g_account.revision} (${new Date(g_account.revision * 1000)}), New rev=${response_obj.rev} (${new Date(response_obj.rev * 1000)}), keys=${Object.keys(g_content_old_sent)} (${new Date(g_content_old_sent * 1000)})`;
				log(msg);
				g_remerges++;
			}
			receive_updates(response_obj, true);
		} else if (response_obj.status === 'logged_out') {
			log(`Logged out because: ${response_obj.reason}`);
			reset_globals();
			render_pages();
		} else if (response_obj.status === 'error') {
			if (response_obj.message === 'exception during ping: TypeError: Failed to fetch' ||
				response_obj.message === 'exception during sync: TypeError: Failed to fetch' ||
				response_obj.message === 'connection failed') {
				log(`Sync error: ${JSON.stringify(response_obj.message)}`);
			} else {
				log(`Sync error: ${JSON.stringify(response_obj.message)}`);
			}
		} else {
			log(`Sync issue: ${JSON.stringify(response_obj)}`);
		}

		// Merge any remaining sent content back into the unsent content
		g_content_old_unsent = { ...g_content_old_unsent, ...g_content_old_sent };
		g_content_old_sent = {};
	});
	if (changed_page_count > 0) {
		g_sent_time = Date.now();
		g_content_old_sent = g_content_old_unsent;
		g_content_old_unsent = {};
	}

	// Get debug messages from the service worker
	if (g_account.debug_mode) {
		httpPost(`${g_origin}/sw.ajax`, {}).then(sw_callback);
	}
}

function every_five_seconds() {
	sync();
}

async function save_server_state() {
	const payload = {
		act: 'save',
	};
	let response = await httpPost(`${g_origin}/ops.ajax`, payload);
	if (response.status === 'done') {
		alert('Saved');
	} else {
		alert(`Error: ${JSON.stringify(response)}`);
	}
}

async function make_backup() {
	const payload = {
		act: 'backup',
	};
	let response = await httpPost(`${g_origin}/ops.ajax`, payload);
	if (response.status === 'backup_ready') {
		window.location = response.url;
	} else {
		alert(`Error: ${JSON.stringify(response)}`);
	}
}

async function log_in(make_new_account=false) {
	// Send request to log in
	const payload = {
		act: make_new_account ? 'new' : 'login',
		name: g_account.username,
		pw: g_account.hash_pub,
	};
	let promise_response = httpPost(`${g_origin}/login.ajax`, payload);

	// Update the account names. (If there is no list, leave it undefined so we will load it from the server.)
	if (g_account_names && !(g_account.username in g_account_names))
		g_account_names.push(g_account.username);

	// Render the stack. (We can begin rendering now because pages may be cached.)
	let promise_render = render_pages();

	// Process the login response
	let response_obj = await promise_response;
	if (response_obj.accounts)
		g_account_names = response_obj.accounts;
	await promise_render;
	if (response_obj.status === 'ok') {
		log(`Successfully logged in`);
		g_account.logged_in = true;
	} else if (response_obj.status === 'logged_out') {
		log(`Load got logged_out status: ${response_obj.reason}`);
		make_login_page();
	} else {
		if (response_obj.message)
			alert(response_obj.message)
		else
			alert(`Got a problem: ${JSON.stringify(response_obj)}`);
		reset_globals();
		httpGet(`${g_origin}/flush_cache.sw`); // make sure we don't use cached data from a different account
		make_login_page();
	}
}

async function new_account() {
	let new_name = document.getElementById('new_name').value;
	let new_pw = document.getElementById('new_pw').value;
	await change_identity(new_name, new_pw);
	await log_in(true);
}

async function log_out(message='Logging out...')
{
	reset_globals();
	let s = [];
	s.push(`<h3>${message}</h3>`);	
	let the_content = document.getElementById('the_content');
	the_content.innerHTML = s.join('');
	const payload = {
		act: 'logout',
	};
	let promise_logout = httpPost(`${g_origin}/ops.ajax`, payload);
	if (g_sw_cache_working) {
		httpGet(`${g_origin}/flush_cache.sw`).catch(ex => { // make sure the cache cannot be used by a different account
			log(ex);
			g_sw_cache_working = false;
		});
	}
	let response = await promise_logout;
	if (response.accounts)
		g_account_names = response.accounts;
	await render_pages();
}

// This function is called when the server sends fresh pages
// in response to a call to retrieve_pages.
// It recursively retrieves more pages until they have all been retrieved.
// Returns true if the server acknowledged the client.
// Returns false and displays the login page if the server says the client is logged out.
function receive_pages(response_obj, reload_if_stack_changes) {
	//log(`receive_pages got ${JSON.stringify(response_obj)}`);
	if (response_obj.status === 'logged_out') {
		reset_globals();
		if (response_obj.accounts)
			g_account_names = response_obj.accounts;
		render_pages();
		return false; // indicates the client needs to log in
	}

	// Check status
	if (response_obj.status !== 'ok') {
		console.log('Received unrecognized status in receive_pages');
		return true; // indicates the server acknowledged the client is logged in
	}

	// Clear the deleted pages, to prevent new pages from getting deleted
	if (response_obj.del.length > 0) {
		for (let page_id of response_obj.del) {
			log(`page ${page_id} was not found on the server. Not deleting it.`);
		}
	}
	response_obj.del = [];

	// Receive the fresh pages
	receive_updates(response_obj, reload_if_stack_changes);

	// Recursively retrieve other reference pages
	let page_list = [];
	const page_block_size = 12; // how many pages to retrieve at-a-time
	for (const [page_id, page] of response_obj.upd) {
		let decrypted_page = g_content[page_id];
		if (decrypted_page.items) {
			for (let item of decrypted_page.items) {
				if (item.dest && !(item.dest in g_content)) {
					page_list.push(item.dest);
					if (page_list.length >= page_block_size) {
						retrieve_pages(page_list);
						page_list = [];
					}
				}
			}
		}
	}
	if (page_list.length > 0)
		retrieve_pages(page_list);

	return true; // indicates the server acknowledged the client is logged in
}

// Returns a promise of true if the pages are successfully retrieved (from the cache or server).
// Returns a promise of false if the server says the client is logged out. In that case, the client
// should immediately return because the content has already been replaced with the log-in page.
async function retrieve_pages(page_ids) {
	// Determine which pages we need from the cache and the server
	let needed_cache_pages = [];
	let needed_fresh_pages = [];
	for (let page_id of page_ids) {
		if (page_id in g_content) {
			if (g_content[page_id].cached)
				needed_fresh_pages.push(page_id);
		} else {
			needed_cache_pages.push(page_id);
			needed_fresh_pages.push(page_id);
		}
	}
	if (needed_fresh_pages.length < 1)
		return true;

	// Request the fresh pages
	//log(`retrieving pages: ${JSON.stringify(needed_fresh_pages)}`);
	promise_fresh = httpPost('get.ajax', {
		pages: needed_fresh_pages,
	});

	// Try fetching the cached pages
	let got_page_count = 0;
	if (g_sw_cache_working) {
		try {
			let cached_pages = await httpGet(`cget.sw?ids=${needed_cache_pages.join(',')}`); // each page in cached_pages is still JSON-encoded
			for (let i = 0; i < cached_pages.length; i++) {
				let page_id = needed_cache_pages[i];
				if (cached_pages[i])
				{
					//log(`Got page ${page_id} from the cache`);
					if (!(page_id in g_content)) { // double-check that a fresh copy didn't arrive while we were checking the cache
						let page = decrypt(page_id, JSON.parse(cached_pages[i]));
						page.cached = true;
						g_content[page_id] = page;
					}
					got_page_count++;
				} else {
					//log(`Page ${page_id} was not in the cache`);
				}
			}
		} catch(ex) {
			// This probably means the service-worker isn't running, so the cache request
			// went all the way to the server. That's a waste of time, so let's not do that again.
			g_sw_cache_working = false;
		}
	}

	// Receive the fresh pages
	if (got_page_count < needed_cache_pages.length) {
		// await the fresh content (because we lack cached copies of one or more pages in the stack)
		//log(`${got_page_count} of ${page_ids.length} found in cache. Awaiting fresh content from server.`); 
		return receive_pages(await promise_fresh, false);
	} else {
		// passively receive the fresh pages when they arrive
		// (because we already have cached copies of the pages we need to render)
		//log(`All ${page_ids.length} pages found in cache. Proceeding to render without waiting for fresh content.`); 
		promise_fresh.then(response_ob => {
			receive_pages(response_ob, true);
		});
		return true; // indicates the client does not need to log in (as far as we presently know)
	}
}

function register_service_worker() {
	if ('serviceWorker' in navigator) {
		// Wait for the 'load' event to not block other work
		window.addEventListener('load', async () => {
			try {
				await navigator.serviceWorker.register('/service-worker.js');
				log('Service worker registered.');
			} catch (err) {
				log(`Service worker registration failed: ${err}`);
			}
		});
	} else {
		log('serviceWorker is not a property of navigator. (This may occur if https is not being used.)');
	}
}

function have_identity() {
	if (!g_account.username || g_account.username.length < 1)
		return false;
	if (!g_account.hash_pri)
		return false;
	if (!g_account.hash_pub)
		return false;
	return true;
}

async function main() {
	// Init the view. (Disables the back button and creates some CSS)
	init_view();

	// Set up the background caching system
	register_service_worker();

	// Set up the state and Load the page stack from the cache
	reset_globals();
	await load_current_state();
	if (have_identity()) {
		await log_in(false).then(() => {
			scroll_to_page(g_current_page_stack[g_current_page_stack.length - 1][0]);
		});
	} else {
		await log_out('Loading account names...');
	}

	// Begin regular sync'ing
	setInterval(function() { every_five_seconds(); }, 5000);

	// Debug stuff
	let browser = browser_name();
	if (browser === 'Chrome') {
		log('Useful debugging URLs: chrome://inspect/#devices, chrome://inspect/#service-workers, chrome://serviceworker-internals');
	} else if (browser === 'Firefox') {
		log('Useful debugging URLs: about:debugging, about:debugging#workers');
	}
	if (g_account.debug_mode) {
		// Ping the service worker to see if it started successfully
		httpPost(`${g_origin}/sw.ajax`, {}).then(sw_callback);
	}
}

main();
