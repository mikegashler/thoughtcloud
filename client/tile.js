// ----------------------------------------------------------------
// The contents of this file are dedicated to the public domain as
// described at http://creativecommons.org/publicdomain/zero/1.0/.
// ----------------------------------------------------------------

function make_tile_item(search_highlighter, page_id, i, initializers) {
	let item = g_content[page_id].items[i];
	const s = [];
	s.push(`<span style="display: inline-block; vertical-align: middle; line-height: normal; font-size:28px; cursor:pointer;">`);
	s.push(search_highlighter.process(item.text));
	s.push('</span>');
	if (search_highlighter.is_selected) {
		g_selected_div = `item_menu_${page_id}_${i}`;
	}
	if (item.file !== undefined) {
		s.push(`<img src="thumb_${item.file}" onclick="on_click_thumbnail(event, '${page_id}', ${i})">`);
	}
	return s.join('');
}

// This view is read-only, and is designed for rapid navigation
class ViewTile {
    constructor(s, view_index, page_id, opened_index, initializers, search_highlighter) {
		this.view_index = view_index;
		this.page_id = page_id;
		let horiz_tiles = 3;

        // Render the items
		let items = g_content[page_id].items;
		s.push('<table cellspacing=10><tr>');
		for(let i = 0; i < items.length; i++) {
			if (i > 0 && i % horiz_tiles === 0) {
				s.push('</tr><tr>');
			}

			// Hash the text to produce an arbitrary corresponding hue
			let hue = 0.;
			for (let c of g_content[page_id].items[i].text) {
				hue += 0.0664 * c.charCodeAt();
			}
			hue = hue % 1;

			// Display the text in a colored tile
			s.push(`<td><div class="cell boxglow" style="background-color: ${hsvToHex(hue, 0.5, 0.5)}; ${opened_index === i ? 'border: 2px solid red;' : ''}" onclick="on_click_item(event, '${page_id}', ${i})">`);
            s.push(make_tile_item(search_highlighter, page_id, i, initializers));
			s.push('</div></td>');
        }
		s.push('</tr></table>');

        // Action buttons
        s.push(`<span id="new_area_${page_id}">`);
        s.push('<table width="100%"><tr>');
        s.push('<td width="50px"></td><td>'); // Horizontal spacer
        make_tile_bottom_buttons(s, page_id);
        s.push('</td></tr></table>')
        s.push('</span><br>');
        s.push(`<div id="action_menu_${page_id}"></div>`);
    }

    // This is not a built-in method.
    // It is called explicitly when this view is no longer being used.
    destructor() {
    }
}
