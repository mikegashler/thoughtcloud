// ----------------------------------------------------------------
// The contents of this file are dedicated to the public domain as
// described at http://creativecommons.org/publicdomain/zero/1.0/.
// ----------------------------------------------------------------

// Returns a random alphanumeric string of the specified length
function random_id(len) {
    let p = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    return [...Array(len)].reduce(a => a + p[~~(Math.random() * p.length)], '');
}

// Returns the filename part of a path
function basename(path) {
	return path.split(/[\\/]/).pop();
}

// Converts an integer to a zero-padded string with the specified size
function zpad(num, size) {
    let s = num.toString();
	while (s.length < size) {
		s = '0' + s;
	}
    return s.substring(s.length - size, s.length);
}

function shuffle_array(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Mfath.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}

function make_unique_id() {
	for (let len = 6; true; len++) {
		let id = random_id(len);
		if (!(id in g_content))
			return id;
	}
}

// Returns an iterator that counts forever in the specified base.
// Returns a little-endian list of digits. For example, if base is 3, it will return
// []
// [0]
// [1]
// [2]
// [0,0]
// [1,0]
// [2,0]
// [1,1]
// ...
// Example usage:
// const it = counter(3);
// for (int i = 0; i < 100; i++) {
//   console.log(`count=${JSON.stringify(it.next().value)}`);
// }
function* counter(base) {
	let cur = [];
	while (true) {
		yield cur;
		let pos;
		for (pos = 0; pos < cur.length; pos++) {
			cur[pos]++;
			if (cur[pos] >= base) {
				cur[pos] = 0;
			} else {
				break;
			}
		}
		if (pos >= cur.length) {
			cur.push(0);
		}
	}
}

function abbreviate(s, max_len=32) {
	if (s.length <= max_len) {
		return s;
	} else {
		return `${s.substring(0, max_len)}...`;
	}
}

function auto_grow_text_area(el) {
	el.style.height = "5px";
    el.style.height = (el.scrollHeight)+"px";
}

function crop_params(url)
{
    let n = url.indexOf('?');
    if (n < 0) {
        return url;
    } else {
        return url.substr(0, n);
    }
}

function scrollIntoViewWithOffset(id, offset) {
	window.scrollTo({
	  behavior: 'smooth',
	  top:
		document.getElementById(id).getBoundingClientRect().top -
		document.body.getBoundingClientRect().top -
		offset,
	});
}
  
// Copies text to the clipboard
function copy_to_clipboard(str)
{
    let tempInput = document.createElement("textarea");
    tempInput.style = "position: absolute; left: -1000px; top: -1000px";
    tempInput.textContent = str;
    document.body.appendChild(tempInput);
    tempInput.select();
    let ok = false;
    try {
        document.execCommand("copy");
        ok = true;
    } catch(ex) {
        console.warn(`Copy failed: ${ex}`);
    } finally {
        document.body.removeChild(tempInput);
    }
    return ok;
}

function count_leading_digits(s, i) {
	while (i < s.length && s[i] >= '0' && s[i] <= '9')
		i++;
	return i;
}

// Splits a string into an array using commas as delimiters.
// Un-escapes double-backslashes and backslash-quotes.
// Absorbs non-escaped double-quotes, and does not split on commas that occur within them.
function smart_split(s) {
	let arr = [];
	let tmp = [];
	let quoted = false;
	let i = 0;
	while (i < s.length) {
		if (s[i] === '"') {
			quoted = !quoted;
		} else if (s[i] === ',') {
			if (quoted) {
				tmp.push(',');
			} else {
				arr.push(tmp.join('').trim());
				tmp = [];
			}
		} else if (s[i] === '\\') {
			if (quoted && i + 1 < s.length && (s[i + 1] === '\\' || s[i + 1] === '"')) {
				i++;
				tmp.push(s[i]);
			} else {
				tmp.push('\\');
			}
		} else {
			tmp.push(s[i]);
		}
		i++;
	}
	arr.push(tmp.join('').trim());
	return arr;
}

// If an elements contain backslases, quotes, or commas, this escapes them with backslashes.
// Then it join the elements with ", ".
function smart_join(arr) {
	let escaped = [];
	for (s of arr) {
		s = String(s);
		if (s.indexOf(',') < 0 && s.indexOf('"') < 0 && s.indexOf('\\') < 0) {
			escaped.push(s);
		} else {
			let tmp = [];
			tmp.push('"');
			for (let i = 0; i < s.length; i++) {
				if (s[i] === '"')
					tmp.push('\\"');
				else if (s[i] === '\\')
					tmp.push('\\\\');
				else
					tmp.push(s[i]);
			}
			tmp.push('"');
			escaped.push(tmp.join(''));
		}
	}
	return escaped.join(', ');
}

// Performs a Python-like modulus operation.
// (If den is 1 (the default), returns the decimal-part of positive numbers.)
function smart_mod(num, den=1) {
	let t = num % den
	if (t >= 0)
		return t;
	else
		return (t + den) % den;
}

// Converts hue, saturation, value (all in range 0-1) to rgb (also in range 0-1)
function hsv_to_rgb(h, s, v) {
	h = smart_mod(h);
	let r;
	let g;
	let b;
	if (s == 0) {
		r = v;
		g = v;
		b = v;
	} else {
		let t1 = v;
		let t2 = (1 - s) * v;
		let t3 = (t1 - t2) * (h % 0.1666666667) / 0.1666666667;
		if (h < 0.1666666667) { r = t1; g = t2 + t3; b = t2; }
		else if (h < 0.3333333333) { r = t1 - t3; g = t1; b = t2; }
		else if (h < 0.5) { r = t2; g = t1; b = t2 + t3; }
		else if (h < 0.6666666667) { r = t2; g = t1 - t3; b = t1; }
		else if (h < 0.8333333333) { r = t2 + t3; g = t2; b = t1; }
		else { r = t1; g = t2; b = t1 - t3; }
	}
    return { r: r, g: g, b: b };
}

// Converges r, g, b values (all in range 0-1) to hsv (also in range 0-1)
function rgb_to_hsv(r, g, b) {
    let max = Math.max(r, g, b);
	let min = Math.min(r, g, b);
    let d = max - min;
    let h = (max === 0 ? 0 : d / max);
    let s = h;
    let v = max;
    switch (max) {
        case min: h = 0; break;
        case r: h = (g - b) + d * (g < b ? 6: 0); h /= 6 * d; break;
        case g: h = (b - r) + d * 2; h /= 6 * d; break;
        case b: h = (r - g) + d * 4; h /= 6 * d; break;
    }
    return {
        h: h,
        s: s,
        v: v,
    };
}

// Hashes some text. Returns arbitrary-but-consistent floating point value from 0 to 1
function hash_text(text) {
	let hue = 0.;
	for (let c of text) {
		hue += 0.0664 * c.charCodeAt();
	}
	hue = hue % 1;
	return hue;
}

// Converts a number from 0 to 255 to two hexadecimal values
function channelToHex(c) {
	var hex = Math.max(0, Math.min(255, Math.floor(c))).toString(16);
	return hex.length == 1 ? "0" + hex : hex;
}

// Converts hue, saturation, value (all in range 0-1) to a hex string
function hsvToHex(h, s, v) {
	let rgb = hsv_to_rgb(h, s, v);
	return '#' + channelToHex(rgb.r * 256) + channelToHex(rgb.g * 256) + channelToHex(rgb.b * 256);
}
  
// Usually, does an alphabetical comparsion.
// But does a numerical comparison if the first difference between 'a' and 'b' involves a digit.
// (Doesn't yet support negative or floating point values.)
function smart_compare(a, b) {
	let i = 0;
	while(i < a.length && i < b.length && a[i] === b[i]) {
		i++;
	}
	if (i >= a.length) {
		if (i >= b.length) {
			return 0;
		} else {
			return -1;
		}
	} else if (i >= b.length) {
		return 1;
	}
	if (i > 0) {
		a = a.substring(i);
		b = b.substring(i);
	}
	let a_digits = 0;
	while (a_digits < a.length && a[a_digits] >= '0' && a[a_digits] <= '9')
		a_digits++;
	let b_digits = 0;
	while (b_digits < b.length && b[b_digits] >= '0' && b[b_digits] <= '9')
		b_digits++;
	if (a_digits === b_digits) {
		return a.toLowerCase().localeCompare(b.toLowerCase());
	} else if (a_digits < b_digits) {
		const sc = smart_compare('0'.repeat(b_digits - a_digits) + a, b);
		if (sc === 0) {
			return -1; // The one with fewer leading zeros comes first
		} else {
			return sc;
		}
	} else {
		const sc = smart_compare(a, '0'.repeat(a_digits - b_digits) + b);
		if (sc === 0) {
			return 1; // The one with fewer leading zeros comes first
		} else {
			return sc;
		}
	}
}

// Returns a deep copy of the specified item up to the specified depth
function clone_item(item, depth) {
    const new_item = { ...item };
    if (item.dest && depth > 0) {
        const new_page = { ...g_content[item.dest] };
        new_page.items = [];
        for (const old_item of g_content[item.dest].items) {
            new_page.items.push(clone_item(old_item, depth - 1));
        }
        let new_id = make_unique_id();
        g_content[new_id] = new_page;
        new_item.dest = new_id;
    } else {
        delete new_item.dest; // when depth is reached, links will stop
    }
    return new_item;
}

// Removes a leading date from text. Just returns text if it does not lead with a date.
function remove_leading_date(text) {
	let pos = 0;
	let separators = 0;
	while(pos < text.length && ((text[pos] >= '0' && text[pos] <= '9') || text[pos] === '-' || text[pos] === '/')) {
		if (text[pos] === '-' || text[pos] === '/') {
			separators++;
		}
		pos++;
	}
	if (separators < 1 || separators > 2) {
		return text;
	}
	if (pos > 10) {
		return text;
	}
	if (pos < text.length && text[pos] <= ' ') {
		pos++;
	}
	return text.substring(pos);
}

function uint8ArrayToBase64String(arr) {
    const abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"; // base64 alphabet
    const bin = n => n.toString(2).padStart(8,0); // convert num to 8-bit binary string
    const l = arr.length
    let result = '';
    for(let i=0; i<=(l-1)/3; i++) {
        let c1 = i*3+1>=l; // case when "=" is on end
        let c2 = i*3+2>=l; // case when "=" is on end
        let chunk = bin(arr[3*i]) + bin(c1? 0:arr[3*i+1]) + bin(c2? 0:arr[3*i+2]);
        let r = chunk.match(/.{1,6}/g).map((x,j)=> j==3&&c2 ? '=' :(j==2&&c1 ? '=':abc[+('0b'+x)]));
        result += r.join('');
    }
    return result;
}

function base64StringToUint8Array(str) {
  const abc = [..."ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"]; // base64 alphabet
  let result = [];
  for(let i=0; i<str.length/4; i++) {
    let chunk = [...str.slice(4*i,4*i+4)]
    let bin = chunk.map(x=> abc.indexOf(x).toString(2).padStart(6,0)).join('');
    let bytes = bin.match(/.{1,8}/g).map(x=> +('0b'+x));
    result.push(...bytes.slice(0,3 - (str[4*i+2]=="=") - (str[4*i+3]=="=")));
  }
  return Uint8Array.from(result);
}

function stringToUint8Array(s) {
	return new TextEncoder("utf-8").encode(s);
}

function uint8ArrayToString(buf) {
	return new TextDecoder("utf-8").decode(buf);
}

// Converts a data URI to a blob (which can be attached to a form as an uploaded file)
function data_uri_to_blob(data_uri) {
	let parts = data_uri.split(',');
	if (parts[0].indexOf('base64') < 0)
		throw new Error('Expected a base64-encoded URI');
	let uint_arr = base64StringToUint8Array(data_uri.split(',')[1]);
    var mimeString = parts[0].split(':')[1].split(';')[0];
    return new Blob([uint_arr], {type:mimeString});
}

// Returns a pad of specified length generated using sha512 and the seed provided
function make_pad(len, seed) {
    let salt = 'bi~if9d_32uAIjhd8HDjJ8asdflp.,veu$*#!djc@(svkeowbejo9fy<fweibfho';
    const pad = new Uint8Array(len);
    let i = 0;
    while(true) {
        let hash = sha512(salt + seed);
        const hash_part = hash.substring(0, Math.min(2 * (len - i), hash.length));
        const hash_bytes = Uint8Array.from(hash_part.match(/.{1,2}/g).map((byte) => parseInt(byte, 16)));
        for (let j = 0; j < hash_bytes.length; j++) {
            pad[i++] = hash_bytes[j];
        }
        if (i >= len) {
            break;
        }
        seed = sha512(seed + salt);
        salt = hash;
    }
    return pad;
}

function scrub(text) {
	if (!text) {
		text = '';
	}
	text = text.replace(/&/g, '&amp;');
	text = text.replace(/>/g, '&gt;');
	text = text.replace(/</g, '&lt;');
	text = text.replace(/\n/g, '<br>');
	text = text.replace(/  /g, ' &nbsp;');
	return text;
}

// Converts a date object to a YYYY-MM-DD format string in the local time zone
function date_to_locale_string(d) {
	return d.getFullYear() + '-' + ('0' + (d.getMonth()+1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2);
}

// Converts a date string in the form YYYY-MM-DD in the local time zone to a Date object
function locale_string_to_date(s) {
	return new Date(Date.parse(s + 'T00:00:00'));
}

// Returns
// '' if s is not a valid date string, or
// 'YYYY-MM-DD' if s is a date string of that format, or
// 'YYYY-MM-DD hh:mm:ss' if s is a date-time string.
//      (Optionally, any non-numeric character may be used instead of a space,
//       and/or the seconds may be followed by a decimal point and milliseconds,
//       and/or it may end with some non-numeric character.
//       This same value will be returned for all such formats.)
function is_date(s) {
	if (typeof s !== 'string' && !(s instanceof String))
		return '';
	let first_hyphen = s.indexOf('-');
	if (first_hyphen >= 0) {
		let second_hyphen = s.indexOf('-', first_hyphen + 1);
		if (second_hyphen < 0) {
			return '';
		}
		let date_time_separator = second_hyphen + 1;
		while(date_time_separator < s.length && s[date_time_separator] >= '0' && s[date_time_separator] <= '9')
			date_time_separator++;
		let year = Number(s.substring(0, first_hyphen));
		let month = Number(s.substring(first_hyphen + 1, second_hyphen));
		let day = Number(s.substring(second_hyphen + 1, date_time_separator));

		// We must ask the following question in a positive way to handle NaN properly
		if (year >= 1000 && year <= 4000 && month >= 1 && month <= 12 && day >= 1 && day <= 31) {
			let first_colon = s.indexOf(':', date_time_separator + 1);
			if (first_colon >= 0) {
				let second_colon = s.indexOf(':', first_colon + 1);
				if (second_colon >= 0) {
					let terminator = second_colon + 1;
					while(terminator < s.length && s[terminator] >= '0' && s[terminator] <= '9')
						terminator++;
					let hour = Number(s.substring(date_time_separator + 1, first_colon));
					let minute = Number(s.substring(first_colon + 1, second_colon));
					let second = Number(s.substring(second_colon + 1, terminator));
					if (hour >= 0 && hour < 24 && minute >= 0 && minute < 60 && second >= 0 && second < 60) {
						return 'YYYY-MM-DD hh:mm:ss';
					}
				}
			}
			return 'YYYY-MM-DD';
		}
		return '';
	}
	return '';
}

// Takes a date string and returns the number of days it represents since the epoch on 1970-01-01
function string_to_days_since_epoch(s) {
	if (typeof s !== 'string' && !(s instanceof String))
		throw new Error('not a date string');
	let first_hyphen = s.indexOf('-');
	if (first_hyphen >= 0) {
		let second_hyphen = s.indexOf('-', first_hyphen + 1);
		if (second_hyphen < 0) {
			throw new Error('not a date string');
		}
		let date_time_separator = second_hyphen + 1;
		while(date_time_separator < s.length && s[date_time_separator] >= '0' && s[date_time_separator] <= '9')
			date_time_separator++;
		let year = Number(s.substring(0, first_hyphen));
		let month = Number(s.substring(first_hyphen + 1, second_hyphen));
		let day = Number(s.substring(second_hyphen + 1, date_time_separator));

		// We must ask the following question in a positive way to handle NaN properly
		if (year >= 1000 && year <= 4000 && month >= 1 && month <= 12 && day >= 1 && day <= 31)
			return Math.round((new Date(year, month - 1, day) - new Date(1970, 0, 0)) / (60 * 60 * 24 * 1000));
		throw new Error('not a date string');
	}
	throw new Error('not a date string');
}

function inject_links(text) {
	if (!text) {
		text = '';
	}
	text = text.replace(/(https?:\/\/[a-zA-Z0-9\/._-]*)/g, "<a target=\"_blank\" href=\"$1\">$1</a>");
	// !@#$%^&\*()?=+~
	return text;
}

// Finds and reveals a spoiler in the specified element.
// Returns false iff no spoiler was found.
function reveal_first_spoiler(el) {
	if (!el)
		return false;
	if (el.classList.contains('spoiler')) {
		el.classList.remove('spoiler');
		return true;
	}
	for (const child of el.children) {
		if (reveal_first_spoiler(child)) {
			return true;
		}
	}
	return false;
}

// Replaces matching brackets in the text with spans that have the spoiler class.
function inject_spoilers(text) {
	if (!text) {
		return '';
	}
	let pos = text.length;
	while (true) {
		let closer = text.lastIndexOf(']', pos);
		if (closer < 0) {
			break;
		}
		let opener = text.lastIndexOf('[', closer - 1);
		if (opener < 0) {
			break;
		}
		text = text.substring(0, opener) +
			'<span class="spoiler">' +
			text.substring(opener + 1, closer) +
			'</span>' +
			text.substring(closer + 1);
		pos = opener - 1;
	}
	return text;
}

// Finds a CSS rule that starts by matching "match",
// and replaces the whole rule with match+remainder.
function update_css_rule(match, remainder) {
	for(let i = 0; i < document.styleSheets.length; i++) {
	    let sheet = document.styleSheets[i];
	    let rules = sheet.cssRules || sheet.rules;

		// Look for a matching rule to replace
	    for (let j = 0; j < rules.length; j++) {
			let r = rules[j];
			let text = r.cssText || r.style.cssText;
			if (text.startsWith(match)) {
				sheet.deleteRule(j);
				sheet.insertRule(match + remainder, j);
				//r.style.setProperty('background', 'blue');
				return;
			}
		}
	}

	// No matching rules were found, so let's add a new one
	if (document.styleSheets.length > 0) {
		let sheet = document.styleSheets[0];
	    let rules = sheet.cssRules || sheet.rules;
		if (rules.length > 0 && rules[0].cssRules) {
			rules = rules[0].cssRules;
		}
		sheet.insertRule(match + remainder, rules.length);
		return;
	}
	console.log('Failed to update CSS rule');
}

// Consumes two strings.
// Produces a list of list of ints. Each inner-list will have a length of 3, indicating [left_start, right_start, len].
// For non-matching blocks, one of the starting positions will be -1.
// (The offset parameters are only used internally during recursion.)
function diff(left, right, min_match_len = 7, left_offset = 0, right_offset = 0) {
	// Make a left and right mapping of all blocks of size min_match_len to a list of starting locations. (We use a list capped at size two so we can tell which matches are unique)
	const l_map = {};
	const cands = [];
	for (let i = 0; i + min_match_len <= left.length; i++) {
		const s = left.substring(i, i + min_match_len);
		if (l_map[s]) {
			if (l_map[s].length < 2) {
				l_map[s].push(i);
			}
		} else {
			l_map[s] = [i];
			cands.push(s);
		}
	}
	const r_map = {};
	for (let i = 0; i + min_match_len <= right.length; i++) {
		const s = right.substring(i, i + min_match_len);
		if (r_map[s]) {
			if (r_map[s].length < 2) {
				r_map[s].push(i);
			}
		} else {
			r_map[s] = [i];
		}
	}

	// Find unique (one-to-one) matches
	const unique_matches = [];
	for (let s of cands) {
		if(l_map[s] && r_map[s] && l_map[s].length === 1 && r_map[s].length === 1) {
			let l_start = l_map[s][0];
			let r_start = r_map[s][0];
			if (unique_matches.length === 0 ||
				l_start >= unique_matches[unique_matches.length - 1][0] + unique_matches[unique_matches.length - 1][2] ||
				l_start < unique_matches[unique_matches.length - 1][0]) // If it's not inside the same block as the last match
			{
				let l_end = l_start + min_match_len;
				let r_end = r_start + min_match_len;
				while (l_start > 0 && r_start > 0 && left[l_start - 1] === right[r_start - 1]) {
					l_start--;
					r_start--;
				}
				while (l_end < left.length && r_end < right.length && left[l_end] === right[r_end]) {
					l_end++;
					r_end++;
				}
				unique_matches.push([l_start, r_start, l_end - l_start]);
			}
		}
	}

	// Find the biggest unique match
	let best_match = -1;
	for (let i = 0; i < unique_matches.length; i++) {
		if (best_match < 0 || unique_matches[i][2] > unique_matches[best_match][2]) {
			best_match = i;
		}
	}

	// We got a unique match, so let's recurse
	if (best_match >= 0) {
		const match = unique_matches[best_match];
		delete l_map;
		delete r_map;
		delete cands;
		delete unique_matches;
		const bef = diff(left.substring(0, match[0]), right.substring(0, match[1]), min_match_len, left_offset, right_offset);
		const aft = diff(left.substring(match[0] + match[2]), right.substring(match[1] + match[2]), min_match_len, left_offset + match[0] + match[2], right_offset + match[1] + match[2]);
		const adjusted_match = [match[0] + left_offset, match[1] + right_offset, match[2]];
		return [...bef, adjusted_match, ...aft];
	}
	delete unique_matches;

	// Try first matches
	const first_matches = [];
	for (let s of cands) {
		if(l_map[s] && r_map[s]) {
			let l_start = l_map[s][0];
			let r_start = r_map[s][0];
			if (first_matches.length === 0 ||
				l_start >= first_matches[first_matches.length - 1][0] + first_matches[first_matches.length - 1][2] ||
				l_start < first_matches[first_matches.length - 1][0]) // If it's not inside the same block as the last match
			{
				let l_end = l_start + min_match_len;
				let r_end = r_start + min_match_len;
				while (l_start > 0 && r_start > 0 && left[l_start - 1] === right[r_start - 1]) {
					l_start--;
					r_start--;
				}
				while (l_end < left.length && r_end < right.length && left[l_end] === right[r_end]) {
					l_end++;
					r_end++;
				}
				first_matches.push([l_start, r_start, l_end - l_start]);
			}
		}
	}

	// Find the biggest first match
	best_match = -1;
	for (let i = 0; i < first_matches.length; i++) {
		if (best_match < 0 || first_matches[i][2] > first_matches[best_match][2]) {
			best_match = i;
		}
	}

	// We got a best first match, so let's recurse
	if (best_match >= 0) {
		const match = first_matches[best_match];
		delete l_map;
		delete r_map;
		delete cands;
		delete first_matches;
		const bef = diff(left.substring(0, match[0]), right.substring(0, match[1]), min_match_len, left_offset, right_offset);
		const aft = diff(left.substring(match[0] + match[2]), right.substring(match[1] + match[2]), min_match_len, left_offset + match[0] + match[2], right_offset + match[1] + match[2]);
		const adjusted_match = [match[0] + left_offset, match[1] + right_offset, match[2]];
		return [...bef, adjusted_match, ...aft];
	}

	// No matches, so just return the two parts as mismatches
	let results = [];
	if (left.length > 0) {
		results.push([left_offset, -1, left.length]);
	}
	if (right.length > 0) {
		results.push([-1, right_offset, right.length]);
	}
	return results;
}

// This function uses "diff" to estimate a distance metric between two strings.
// It simply returns the sum of the length of all differing segments.
// (Note that this differs from Levenshtein distance in a few ways:
//  (1) diff requires matches to be larger than single characters,
//  (2) this double-counts changed regions,
//  (3) this will be faster, but possibly less exhaustive in some cases.
// )
function string_distance(a, b) {
	const min_match = Math.max(2, Math.min(7, Math.floor(Math.max(a.length, b.length) / 10)));
	const diffs = diff(a, b, min_match);
	let dist = 0;
	for (const d of diffs) {
		if (d[0] < 0 || d[1] < 0) {
			dist += d[2];
		}
	}
	return dist;
}

// Returns the amount of overlap between two strings.
// This is kind of the opposite of string_distance.
function string_overlap(a, b) {
	const min_match = Math.max(2, Math.min(7, Math.floor(Math.max(a.length, b.length) / 10)));
	const diffs = diff(a, b, min_match);
	let sim = 0;
	for (const d of diffs) {
		if (d[0] >= 0 && d[1] >= 0) {
			sim += d[2];
		}
	}
	return sim;
}

// Note: This method could be implemented to be much faster using sorting
// Returns a list of indexes that map rows to columns in the cost matrix, prioritizing lowest-cost first.
// Returns -1 in some elements if there are more rows than columns.
function greedy_matches(costs) {
	// Check dimensions and copy costs
	let row_count = costs.length;
	if (row_count < 1) {
		return [];
	}
	let col_count = costs[0].length;
	if (col_count < 1) {
		return Array(row_count).fill(-1);
	}
	let costs_copy = [];
	for (let i = 0; i < costs.length; i++) {
		if (costs[i].length !== col_count) {
			throw new Error(`Row ${i} has ${costs[i].length} elements. Expected ${col_count}.`);
		}
		costs_copy.push([...costs[i]]);
	}
	let row_indexes = [];
	let col_indexes = [];
	for (let i = 0; i < costs.length; i++) {
		row_indexes.push(i);
	}
	for (let i = 0; i < costs[0].length; i++) {
		col_indexes.push(i);
	}

	// Remove best costs until none remain
	const matches = Array(row_count).fill(-1);
	while (row_indexes.length > 0 && col_indexes.length > 0) {
		// Find the best element
		let best_cost = 1/0;
		let best_row = -1;
		let best_col = -1;
		for (let i = 0; i < row_indexes.length; i++) {
			for (let j = 0; j < col_indexes.length; j++) {
				if (costs_copy[i][j] < best_cost) {
					best_cost = costs_copy[i][j];
					best_row = i;
					best_col = j;
				}
			}
		}
		const row_index = row_indexes[best_row];
		const col_index = col_indexes[best_col];
		matches[row_index] = col_index;

		// remove the row and column of the best element
		row_indexes.splice(best_row, 1);
		col_indexes.splice(best_col, 1);
		costs_copy.splice(best_row, 1);
		for (let i = 0; i < row_indexes.length; i++) {
			costs_copy[i].splice(best_col, 1);
		}
	}
	return matches;
}

function three_way_string_merge(parent, a, b) {
	if (a === b) {
		return a; // Might as well quickly handle this trivial case
	}
	const a_diff = diff(parent, a);
	const b_diff = diff(parent, b);
	let merged = [];
	let a_pos = 0;
	let b_pos = 0;
	let a_off = 0;
	let b_off = 0;
	while(true) {
		//log(`a_pos=${a_pos}, b_pos=${b_pos}`);
		const a_done = (a_pos >= a_diff.length);
		const b_done = (b_pos >= b_diff.length);
		if (a_done && b_done) {
			break;
		}
		if (b_done) {
			// Accept whatever a has left
			if (a_diff[a_pos][1] >= 0) {
				merged.push(a.substring(a_diff[a_pos][1] + a_off, a_diff[a_pos][1] + a_diff[a_pos][2]));
				a_off = a_diff[a_pos][2];
			}
		} else if (a_done) {
			// Accept whatever b has left
			if (b_diff[b_pos][1] >= 0) {
				merged.push(b.substring(b_diff[b_pos][1] + b_off, b_diff[b_pos][1] + b_diff[b_pos][2]));
				b_off = b_diff[b_pos][2];
			}
		} else {
			if (a_diff[a_pos][0] >= 0 && b_diff[b_pos][0] >= 0 && a_diff[a_pos][0] + a_off !== b_diff[b_pos][0] + b_off) {
				throw new Error('Internal error. Parent positions are mismatched.');
			}
			const a_same = (a_diff[a_pos][0] >= 0 && a_diff[a_pos][1] >= 0);
			const b_same = (b_diff[b_pos][0] >= 0 && b_diff[b_pos][1] >= 0);
			if (a_same && b_same) {
				// Accept this section because everyone agrees
				const bytes = Math.min(a_diff[a_pos][2] - a_off, b_diff[b_pos][2] - b_off);
				if (bytes < 1) {
					throw new Error('Internal error');
				}
				merged.push(b.substring(b_diff[b_pos][1] + b_off, b_diff[b_pos][1] + b_off + bytes));
				a_off += bytes;
				b_off += bytes;
			} else if (a_same) {
				if (b_diff[b_pos][0] >= 0) {
					// Ignore this section because it was deleted by b
					const bytes = Math.min(a_diff[a_pos][2] - a_off, b_diff[b_pos][2] - b_off);
					if (bytes < 1) {
						throw new Error('Internal error');
					}
					a_off += bytes;
					b_off += bytes;
				} else if (b_diff[b_pos][1] >= 0) {
					// Accept this section that was inserted by b
					const bytes = b_diff[b_pos][2] - b_off;
					if (bytes < 1) {
						throw new Error('Internal error');
					}
					merged.push(b.substring(b_diff[b_pos][1] + b_off, b_diff[b_pos][1] + b_off + bytes));
					b_off += bytes;
				} else {
					throw new Error('Internal error');
				}
			} else if (b_same) {
				if (a_diff[a_pos][0] >= 0) {
					// Ignore this section because it was deleted by a
					const bytes = Math.min(a_diff[a_pos][2] - a_off, b_diff[b_pos][2] - b_off);
					if (bytes < 1) {
						throw new Error('Internal error');
					}
					a_off += bytes;
					b_off += bytes;
				} else if (a_diff[a_pos][1] >= 0) {
					// Accept this section that was inserted by a
					const bytes = a_diff[a_pos][2] - a_off;
					if (bytes < 1) {
						throw new Error('Internal error');
					}
					merged.push(a.substring(a_diff[a_pos][1] + a_off, a_diff[a_pos][1] + a_off + bytes));
					a_off += bytes;
				} else {
					throw new Error('Internal error');
				}
			} else {
				if (a_diff[a_pos][0] >= 0) {
					if (b_diff[b_pos][0] >= 0) {
						// Ignore the section both a and b deleted
						const bytes = Math.min(a_diff[a_pos][2] - a_off, b_diff[b_pos][2] - b_off);
						if (bytes < 1) {
							throw new Error('Internal error');
						}
						a_off += bytes;
						b_off += bytes;
					} else {
						// a deleted, b inserted. Let's accept the insertion
						const bytes = b_diff[b_pos][2] - b_off;
						if (bytes < 1) {
							throw new Error('Internal error');
						}
						merged.push(b.substring(b_diff[b_pos][1] + b_off, b_diff[b_pos][1] + b_off + bytes));
						b_off += bytes;
					}
				} else {
					if (b_diff[b_pos][0] >= 0) {
						// a inserted, b deleted. Let's accept the insertion
						const bytes = a_diff[a_pos][2] - a_off;
						if (bytes < 1) {
							throw new Error('Internal error');
						}
						merged.push(a.substring(a_diff[a_pos][1] + a_off, a_diff[a_pos][1] + a_off + bytes));
						a_off += bytes;
					} else {
						// a and b both inserted. Let's see how much of their insertions match
						const a_bytes = a_diff[a_pos][2] - a_off;
						const b_bytes = b_diff[b_pos][2] - b_off;
						const min_bytes = Math.min(a_bytes, b_bytes);
						if (min_bytes < 1) {
							throw new Error('Internal error');
						}
						let match_bytes = 0;
						while (match_bytes < min_bytes && a[a_diff[a_pos][1] + a_off + match_bytes] === b[b_diff[b_pos][1] + b_off + match_bytes]) {
							match_bytes++;
						}

						// Resolve the merge conflict
						if (match_bytes * 2 >= min_bytes) {
							// They mostly match, so let's accept the matching portion just once
							merged.push(a.substring(a_diff[a_pos][1] + a_off, a_diff[a_pos][1] + a_off + match_bytes));
							a_off += match_bytes;
							b_off += match_bytes;
						} else {
							// They mostly differ, so let's accept them separately
							merged.push(a.substring(a_diff[a_pos][1] + a_off, a_diff[a_pos][1] + a_off + a_bytes));
							a_off += a_bytes;
							merged.push(b.substring(b_diff[b_pos][1] + b_off, b_diff[b_pos][1] + b_off + b_bytes));
							b_off += b_bytes;
						}
					}
				}
			}
		}

		// Advance to the next diff blocks
		if (!a_done && a_off >= a_diff[a_pos][2]) {
			a_pos++;
			a_off = 0;
		}
		if (!b_done && b_off >= b_diff[b_pos][2]) {
			b_pos++;
			b_off = 0;
		}
	}
	return merged.join('');
}

// Returns [] if s is a valid JSON string with no duplicate keys.
// Returns [start,end] indexes into s of a duplicate key if one is found.
// Throws an error if s is invalid JSON.
function check_json_for_dupes(s) {
	let ob = JSON.parse(s);
	let s2 = JSON.stringify(ob);
	let a = 0;
	let b = 0;
	while (a < s.length && b < s2.length) {
		if (s[a] === s2[b]) {
			a++;
			b++;
		} else if (s[a] === ' ' || s[a] === '\n' || s[a] === '\r' || s[a] === '\t' || s[a] === '\v') {
			a++;
		} else if (s2[b] === ' ' || s2[b] === '\n' || s2[b] === '\r' || s2[b] === '\t' || s2[b] === '\v') {
			b++;
		} else {
			log(`searching from ${s.substring(a)}`);
			let last_colon = s.lastIndexOf(':', a);
			let last_comma = s.lastIndexOf(',', a);
			if (last_colon > last_comma) {
				let prev_quot = s.lastIndexOf('"', last_colon - 1);
				let prev_prev_quot = s.lastIndexOf('"', prev_quot - 1);
				log(`prev_quot=${prev_quot}, prev_prev_quot=${prev_prev_quot}`);
				if (prev_quot >= 0 && prev_prev_quot >= 0) {
					return [prev_prev_quot, prev_quot + 1];
				} else {
					return [a, a + 1];
				}
			} else if (last_comma > last_colon) {
				log('quote > colon');
				let next_quot = s.indexOf('"', a);
				let next_next_quot = s.indexOf('"', next_quot + 1);
				if (next_quot >= 0 && next_next_quot >= 0) {
					return [next_quot, next_next_quot + 1];
				} else {
					return [a, a + 1];
				}
			} else {
				log('nada');
				return [a, a + 1];
			}
		}
	}
	return [];
}

// returns 'e' for empty or all-whitespace strings,
// returns 'n' for numbers (including floating point and integer),
// returns 'd' for dates and time-stamps,
// returns 's' for all other strings
function categorize_string(value) {
	let v = value.trim();
	if (v.length === 0)
		return 'e';
	if (!isNaN(Number(v)))
		return 'n';
	if (is_date(v))
		return 'd';
	return 's';
}

// arr is a string of numbers.
// Returns an array of [forecast-value, termination-code, mean-of-squares, depth].
// forecast-value is the value you probably want.
// The other three values are used for determining the uncertainty of the forecast.
// Values close to 0 indicate high certainty.
// termination code is just a number indicating where in the code the forecast was made.
// Its value will be one of [0, 1, 4, 5].
// mean-of-squares indicates the mean-squared value at that time.
// depth indicates the recursive depth before the forecast was made.
function forecast_polynomial(arr, depth=0) {
	// If we have nothing or little to work with
	if (arr.length < 1)
		return [0, 5, 1000000000, depth];
	if (arr.length < 2) {
		return [arr[0], 1, arr[0] * arr[0], depth];
	}

	// If the mean of squared values is small, forecast the mean will occur next
	let sum = 0;
	let sum_of_squares = 0;
	for (let i = 0; i < arr.length; i++) {
		sum += arr[i];
		sum_of_squares += (arr[i] * arr[i]);
	}
	let thresh = 0.02; // if mean-squared value is below this threshold, we are ready to forecast
	let mean_of_squares = sum_of_squares / arr.length;
	if (mean_of_squares < thresh)
		return [sum / arr.length, 0, mean_of_squares, depth];

	// If we are in too deep, just call it
	if (depth > 10) {
		return [sum / arr.length, 4, mean_of_squares, depth];
	}

	// Compute the forward-difference and recurse
	let deriv = [];
	for (let i = 1; i < arr.length; i++) {
		deriv.push(arr[i] - arr[i - 1]);
	}
	let deriv_forecast = forecast_polynomial(deriv, depth + 1);
	return [arr[arr.length - 1] + deriv_forecast[0], deriv_forecast[1], deriv_forecast[2], deriv_forecast[3]]
}

// Arr is a list of numbers
// Returns the median value
function median(arr) {
	// Sort the array in ascending order
	arr.sort((a, b) => a - b);
  
	// Find the middle index
	const mid = Math.floor(arr.length / 2);
  
	// If the array has an odd number of elements, return the middle element
	if (arr.length % 2 !== 0) {
	  return arr[mid];
	} else {
	  // If the array has an even number of elements, return the average of the two middle elements
	  return (arr[mid - 1] + arr[mid]) / 2;
	}
}

// arr is an array of strings.
// index is the index of the array element for which suggestions are being sought.
// front is the part that has been entered so far. (All suggestions must start with it.)
// title is an optional title for the column.
// Returns an array of tuples of size 2,
// indicating suggested values and corresponding reasons.
function suggest_value(arr, index, front='', title='', max_suggestions=10) {
	if (arr.length < 1)
		return [];
	let suggestions = [];

	// If the last value is a number...
	let category = 's';
	if (index > 0)
		category = categorize_string(arr[index - 1]);
	if (category === 'n') {
		// Find the longest sub-list of all numbers before the index
		let i = index - 1;
		while (i > 0 && categorize_string(arr[i - 1]) === 'n')
			i--;
		let all_nums = arr.slice(i).map(x => Number(x));

		// Suggest a polynomial forecast
		let poly_forecast = forecast_polynomial(all_nums)[0];
		if (poly_forecast[1] < 3) {
			let value = '' + poly_forecast[0];
			if (value.length > front.length && value.startsWith(front))
				suggestions.push([value, 'poly. forecast']);
		}
	
		// Suggest the median value
		{
			let value = '' + median(all_nums);
			if (value.length > front.length && value.startsWith(front))
				suggestions.push([value, 'median value']);
		}

		// Suggest extrapolated value
		if (all_nums.length > 1) {
			let value = '' + (2 * all_nums[all_nums.length - 1] - all_nums[all_nums.length - 2]);
			if (value.length > front.length && value.startsWith(front))
				suggestions.push([value, 'extrapolated']);
		}
	}

	// If we suspect it may be a date...
	if (category === 'd' || title.toLowerCase().indexOf('date') >= 0) {
		// Suggest today's date
		{
			let value = date_to_locale_string(new Date());
			if (value.length > front.length && value.startsWith(front))
				suggestions.push([value, 'today']);
		}

		// Suggest yesterday's date
		{
			let yesterday = new Date();
			yesterday.setDate(yesterday.getDate() - 1);
			let value = date_to_locale_string(yesterday);
			if (value.length > front.length && value.startsWith(front))
				suggestions.push([value, 'yesterday']);
		}

		// Suggest extrapolated date
		if (index > 1) {
			prev = locale_string_to_date(arr[index - 1].trim()).getTime();
			pen = locale_string_to_date(arr[index - 2].trim()).getTime();
			if (!isNaN(prev) && !isNaN(pen)) {
				let value = date_to_locale_string(new Date(2 * prev - pen));
				if (value.length > front.length && value.startsWith(front))
					suggestions.push([value, 'extrapolated']);
			}
		}
	}

	// Suggest most-frequent values
	if (suggestions.length < max_suggestions + 4) { // the +4 allows for a few repeats
		// Make a frequency table for the values
		let freq = {};
		for (let i = 0; i < arr.length; i++) {
			let value = arr[i].trim();
			if (value === undefined || value.length < 1)
				continue;
			if (value.length > front.length && value.startsWith(front)) {
				if (value in freq)
					freq[value]++;
				else
					freq[value] = 1;
			}
		}
		let freq_vals = Object.keys(freq);

		// Sort the most frequent values
		freq_vals.sort((a, b) => {
			if (freq[b] !== freq[a])
				return freq[b] - freq[a]; // most-to-least frequent
			else
				return a.localeCompare(b); // fall back to forward-alphabetical order
		});

		// Suggest the most-frequent values
		for (let i = 0; i < freq_vals.length; i++) {
			let occurrences = freq[freq_vals[i]];
			if (occurrences < Math.sqrt(arr.length) / 2)
				break;
			suggestions.push([freq_vals[i], `${occurrences} occurrence${occurrences === 1 ? '' : 's'}`]);
		}
	}

	// Filter out any repeat suggestions, and limit to max_suggestions
	let suggs_no_repeats = [];
	let seen = {};
	for (let i = 0; i < suggestions.length; i++) {
		if (suggs_no_repeats.length >= max_suggestions)
			break;
		if (!(suggestions[i][0] in seen)) {
			seen[suggestions[i][0]] = 1;
			suggs_no_repeats.push(suggestions[i]);
		}
	}
	return suggs_no_repeats;
}
