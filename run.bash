#!/bin/bash
set -e
PORT=8080

delay_open_browser() {
    sleep 0.5
    python3 -m webbrowser http://localhost:${PORT}/index.html
}

# Check for type errors
mypy server/main.py --strict --ignore-missing-imports --allow-subclassing-any

# See if anyone is using the port we want
#lsof -i tcp:${PORT}

# Call the function to open the browser
delay_open_browser &

# Start the server
python3 server/main.py
