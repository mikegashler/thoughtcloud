
How to run the server:
* python3 main.py server

How to set up the server to run as a daemon:
* customize server/thought_cloud.service by editing the parts in braces.
* sudo cp server/thought_cloud.service /etc/systemd/system
* sudo systemctl enable thought_cloud.service
* sudo systemctl restart thought_cloud.service
