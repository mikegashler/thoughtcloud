# ---------------------------------------------------------------
# The contents of this file are dedicated to the public domain as
# described at http://creativecommons.org/publicdomain/zero/1.0/.
# ---------------------------------------------------------------

from typing import Mapping, Any, Dict
import state
from datetime import datetime, timedelta

last_purge = datetime.now()
private_chat_rooms:Dict[str,Any] = {}
public_chat_rooms:Dict[str,Any] = {}

def purge_private_chat_rooms() -> None:
    # Delete all rooms that haven't sync'ed in over a minute
    global private_chat_rooms
    global last_purge
    time_now = datetime.now()
    if time_now - last_purge > timedelta(seconds=60):
        last_purge = time_now
        for k in list(private_chat_rooms.keys()):
            if time_now - private_chat_rooms[k]['last_sync_time'] > timedelta(seconds=60):
                del private_chat_rooms[k]

def update_active_clients(clients:Dict[str,Any], id:str) -> int:
    time_now = datetime.now()
    clients[id] = time_now
    for client_id in list(clients.keys()):
        if client_id != id and ((time_now - clients[client_id]) > timedelta(seconds=5)):
            del clients[client_id]
    return len(clients)

def make_error(session:state.Session, message:str, expected:bool=False) -> Mapping[str, Any]:
    if not expected:
        session.log(message)
    return {
        'status': 'delay' if expected else 'error',
        'message': message,
    }

def make_private_chat_page(params: Mapping[str, Any], session: state.Session) -> Mapping[str, Any]:
    global private_chat_rooms
    if not 'action' in params:
        return make_error(session, 'Expected an "action" field')
    action = params['action']
    if action == 'sync':
        if not 'room' in params:
            return make_error(session, 'Expected a chat room id')
        room_id = params['room']
        if not room_id in private_chat_rooms:
            return make_error(session, f'No chat room with id: {room_id}')
        room = private_chat_rooms[room_id]
        chats = []
        if not 'first' in params or params['first'] != True:
            while room['second_pos'] < len(room['chats']):
                chats.append(room['chats'][room['second_pos']])
                room['second_pos'] += 1
            id = f'{room_id}_1'
        else:
            while room['first_pos'] < len(room['chats']):
                chats.append(room['chats'][room['first_pos']])
                room['first_pos'] += 1
            id = f'{room_id}_2'
        active_clients = update_active_clients(room['clients'], id)
        while room['first_pos'] > 0 and room['second_pos'] > 0:
            room['chats'].pop(0)
            room['first_pos'] -= 1
            room['second_pos'] -= 1
        room['last_sync_time'] = datetime.now()
        purge_private_chat_rooms()
        return {
            'status': 'ok',
            'chats': chats,
            'clients': active_clients,
        }
    elif action == 'chat':
        if not 'room' in params:
            return make_error(session, 'Expected an chat room id')
        room_id = params['room']
        if not room_id in private_chat_rooms:
            return make_error(session, f'No chat room with id: {room_id}')
        room = private_chat_rooms[room_id]
        timestamp = datetime.now().isoformat().replace('T', ' ')
        timestamp = timestamp[:timestamp.rfind(':')]
        if not 'first' in params or params['first'] != True:
            room['chats'].append((False, timestamp, params['salt'], params['text'], params['chat_id']))
        else:
            room['chats'].append((True, timestamp, params['salt'], params['text'], params['chat_id']))
    elif action == 'hs4':
        if not 'room' in params:
            return make_error(session, 'Expected an chat room id')
        room_id = params['room']
        if not room_id in private_chat_rooms:
            return make_error(session, f'No chat room with id: {room_id}')
        room = private_chat_rooms[room_id]
        if not 'first' in params or params['first'] != True:
            if not 'first_sym_key' in room:
                return make_error(session, f'still waiting for other client to send symmetric key', True)
            return {
                'status': 'ok',
                'sym_key': room['first_sym_key'],
            }
        else:
            if not 'first_sym_key' in room:
                return make_error(session, f'still waiting for other client to send symmetric key', True)
            return {
                'status': 'ok',
                'sym_key': room['second_sym_key'],
            }
    elif action == 'hs3':
        if not 'room' in params:
            return make_error(session, 'Expected an chat room id')
        if not 'sym_key' in params:
            return make_error(session, 'Expected a sym key')
        room_id = params['room']
        if not room_id in private_chat_rooms:
            return make_error(session, f'No chat room with id: {room_id}')
        room = private_chat_rooms[room_id]
        if not 'first' in params or params['first'] != True:
            room['second_sym_key'] = params['sym_key']
        else:
            room['first_sym_key'] = params['sym_key']
    elif action == 'hs2':
        if not 'room' in params:
            return make_error(session, 'Expected an chat room id')
        room_id = params['room']
        if not room_id in private_chat_rooms:
            return make_error(session, f'No chat room with id: {room_id}')
        room = private_chat_rooms[room_id]
        if not 'first' in params or params['first'] != True:
            if not 'first_public_key' in room:
                return make_error(session, 'missing the public key of the room creator')
            return {
                'status': 'ok',
                'public_key': room['first_public_key'],
            }
        else:
            if not 'second_public_key' in room:
                return make_error(session, 'still waiting for other client to connect', True)
            return {
                'status': 'ok',
                'public_key': room['second_public_key'],
            }
    elif action == 'hs1':
        if not 'room' in params:
            return make_error(session, 'Expected an chat room id')
        if not 'public_key' in params:
            return make_error(session, 'Expected a public key')
        room_id = params['room']
        if not 'first' in params or params['first'] != True:
            # Find an existing room
            if not room_id in private_chat_rooms:
                return make_error(session, 'No one with the id "{room_id}" is waiting to chat')
            room = private_chat_rooms[room_id]
            if 'second_public_key' in room:
                return make_error(session, 'This conversation is already in progress')
            room['second_public_key'] = params['public_key']
        else:
            # Make a new room
            room = {
                'first_public_key': params['public_key'],
                'chats': [],
                'clients': {},
                'first_pos': 0,
                'second_pos': 0,
                'last_sync_time': datetime.now(),
            }
            private_chat_rooms[room_id] = room
    else:
        return make_error(session, f'Unrecognized action: ${action}')

    # Return something
    return {
        'status': 'ok',
    }

def make_public_chat_room(room_id:str) -> None:
    room:Dict[str,Any] = {
        'chats': [],
        'clients': {},
        'thread': 0,
    }
    public_chat_rooms[room_id] = room

def make_public_chat_page(params: Mapping[str, Any], session: state.Session) -> Mapping[str, Any]:
    global public_chat_rooms
    if not 'action' in params:
        return make_error(session, 'Expected an "action" field')
    action = params['action']
    if action == 'sync':
        if not 'room' in params:
            return make_error(session, 'Expected a chat room id')
        if not 'pos' in params:
            return make_error(session, 'Expected a pos parameter')
        if not 'id' in params:
            return make_error(session, 'Expected an id')
        if not 'thread' in params:
            return make_error(session, 'Expected a thread id')
        room_id = params['room']
        pos = params['pos']
        thread = params['thread']
        id = params['id']
        if not room_id in public_chat_rooms:
            make_public_chat_room(room_id)
        room = public_chat_rooms[room_id]
        if thread != room['thread']:
            pos = 0
        active_clients = update_active_clients(room['clients'], id)
        return {
            'status': 'ok',
            'chats': room['chats'][pos:],
            'clients': active_clients,
            'thread': room['thread'],
        }
    elif action == 'chat':
        # Extract parameters
        if not 'room' in params:
            return make_error(session, 'Expected an chat room id')
        if not 'chat' in params:
            return make_error(session, 'Expected a chat field')
        room_id = params['room']
        chat = params['chat']

        # Find the room
        if not room_id in public_chat_rooms:
            make_public_chat_room(room_id)
        room = public_chat_rooms[room_id]

        # Add a timestamp to the chat
        timestamp = datetime.now().isoformat().replace('T', ' ')
        timestamp = timestamp[:timestamp.rfind(':')]
        chat['time'] = timestamp

        # Add the message
        if chat['text'] == '/clear':
            room['thread'] += 1
            room['chats'].clear()
        else:
            room['chats'].append(chat)
        return {
            'status': 'ok',
        }
    return {}
