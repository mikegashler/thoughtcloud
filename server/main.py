# ---------------------------------------------------------------
# The contents of this file are dedicated to the public domain as
# described at http://creativecommons.org/publicdomain/zero/1.0/.
# ---------------------------------------------------------------

import json
import os
import sys
import time
from typing import Mapping, Any
from PIL import Image
import signal
import atexit
import threading

import state
from http_daemon import log, WebServer, Response
import worker
import chat

web_server:WebServer

def exit_handler() -> None:
    web_server.stop() # This was probably already called, but let's call it again just in case

def signal_handler(sig:int, frame) -> None: # type: ignore
    log(f'Got a SIGTERM')
    web_server.stop()
    time.sleep(0.5) # Give the server thread a moment to shut down
    sys.exit(0)

ajax_handlers = {
    'ops.ajax': worker.make_ops_page, # for various operations, like change_name, delete_account, etc.
    'resend.ajax': worker.make_resend_page, # for overriding all content with the client's copy
    'sw.ajax': worker.make_sw_page, # informs clients the service worker is not intercepting fetches like it should
    'login.ajax': worker.make_login_page, # logs in
    'get.ajax': worker.make_get_page, # sends only requested pages to the client
    'sync.ajax': worker.make_sync_page, # for regular updates
    'upload.ajax': worker.make_upload_page, # for uploading files
    'aether_chat.ajax': chat.make_private_chat_page, # for one-to-one secure conversations
    'public_chat.ajax': chat.make_public_chat_page, # for one-to-one insecure conversations
}

def generate_thumbnail(orig_filename:str, thumb_filename:str) -> None:
    image = Image.open(orig_filename)
    max_size = (300, 300)
    image.thumbnail(max_size)
    image.convert('RGB')
    image.save(thumb_filename)
    print(f'Made thumbnail: {thumb_filename}')

def url_to_page(response:Response, url:str, params: Mapping[str, Any], session_id:str, ip_addr:str) -> None:
    # Try an ajax handler
    if url in ajax_handlers:
        session = state.get_or_make_session(session_id, ip_addr)
        result = ajax_handlers[url](params, session)
        response.send_response(200)
        response.end_headers()
        if url == 'upload.html':
            print(f'upload response: {result}')
        response.wfile.write(bytes(json.dumps(result), 'utf8'))
        return

    # See if it is a service-worker call
    ext = os.path.splitext(url)[1]
    if ext == '.sw':
        response.send_response(404, 'service worker down')
        response.send_header('Content-type', 'text/html')
        response.end_headers()
        response.wfile.write(bytes('{}', 'utf8'))
        return

    # See if it is a client file
    client_filename = os.path.join('client', url)
    if os.path.exists(client_filename):
        with open(client_filename, 'rb') as f:
            content = f.read()
        response.send_file(url, content, session_id)
        return

    # See if it is an account file (or thumbnail of one)
    session = state.get_or_make_session(session_id, ip_addr)
    if session.name in state.accounts:
        account_folder = f'accounts/{session.name}/files'
        account_filename = os.path.join(account_folder, url)
        if os.path.exists(account_filename):
            with open(account_filename, 'rb') as f:
                content = f.read()
            response.send_file(url, content, session_id)
            print(f'Sending account file: {account_filename}')
            return
        elif url.startswith('thumb_'):
            orig_filename = os.path.join(account_folder, url[6:])
            if os.path.exists(orig_filename):
                print(f'Generating thumbnail for {orig_filename}')
                generate_thumbnail(orig_filename, account_filename)
                with open(account_filename, 'rb') as f:
                    content = f.read()
                response.send_file(url, content, session_id)
                print(f'Sending account file: {account_filename}')
                return

    # Return a not found error
    response.send_response(404)
    response.send_header('Content-type', 'text/html')
    response.end_headers()
    response.wfile.write(bytes(f'404 {url} not found.\n', 'utf8'))

def maintenance_thread() -> None:
    log('Entering maintenance thread')
    while web_server.keep_going:
        time.sleep(1.)
        state.maybe_save_all()
    log('Exiting maintenance thread')

def main() -> None:
    # Load state
    log(f'----------------Starting')
    os.chdir(os.path.join(os.path.dirname(__file__), '..'))
    state.load_state()

    # Read some config values
    config = state.config
    host = config['host'] if 'host' in config else '127.0.0.1'
    port = config['port'] if 'port' in config else 8080
    ssl_privkey = config['ssl_privkey'] if 'ssl_privkey' in config else ''
    ssl_cert = config['ssl_cert'] if 'ssl_cert' in config else ''
    ping_gap = config['monitor'] if 'monitor' in config else 0

    # Make a web server
    global web_server
    web_server = WebServer(host, port, ssl_privkey, ssl_cert)

    # Catch signals
    signal.signal(signal.SIGTERM, signal_handler)
    atexit.register(exit_handler)

    # Set up monitoring
    # The ping_gap value specifies the delay (in seconds) between pings to check server health.
    # This value must be smaller than "WatchdogSec" in /etc/systemd/system/thought_cloud.service
    if ping_gap > 0:
        web_server.monitor(ping_gap)

    # Set up maintenance
    m_thread = threading.Thread(target=maintenance_thread)
    m_thread.start()

    # Serve pages
    web_server.serve(url_to_page)

    # Save state
    state.save_all()

if __name__ == "__main__":
    main()
