# ---------------------------------------------------------------
# The contents of this file are dedicated to the public domain as
# described at http://creativecommons.org/publicdomain/zero/1.0/.
# ---------------------------------------------------------------

from collections import deque
from datetime import datetime, timedelta
import json
import os
import random
import shutil
import string
import tempfile
import time
import traceback
from typing import Dict, Any, Deque, Tuple, List, Mapping, Any, Set
from test_content import test_content

from http_daemon import log

def make_random_page_id() -> str:
    return '_' + ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(6))

class Account():
    def __init__(self, name:str) -> None:
        self.name = name
        self.password = '' # A hexadecimal string of the sha512 hash of this user's password
        self.content: Dict[str, Any] = {} # All of this account's content
        self.created: datetime = datetime.now() # time stamp when the account was created
        self.accessed: datetime = self.created # time stamp when the account was last accessed
        self.saved = datetime.now()
        self.history: Deque[Tuple[float, List[str]]] = deque() # time, list of updated or deleted page_ids
        self.unsaved_changes = 0

    def marshal(self) -> Mapping[str, Any]:
        return {
            'pw': self.password,
            'content': self.content,
            'created': self.created.isoformat(),
            'accessed': self.accessed.isoformat(),
        }

    @staticmethod
    def unmarshal(name:str, ob:Mapping[str, Any]) -> 'Account':
        account = Account(name)
        account.password = ob['pw']
        account.content = ob['content']
        now = datetime.now()
        account.created = datetime.fromisoformat(ob['created']) if 'created' in ob else now
        account.accessed = datetime.fromisoformat(ob['accessed']) if 'accessed' in ob else account.created
        account.saved = now
        account.history.clear()
        return account

    def page(self, key:str) -> Any:
        if not 'con' in self.content[key]:
            raise ValueError(f'no con in {self.content[key]}')
        return self.content[key]

    def reset_history(self, revision:int) -> None:
        self.content = {}
        self.revision = revision

    # Returns the time (in seconds since epoch), with a guarantee of coming after the last revision in the history
    def now(self) -> float:
        while True:
            rev = time.time()
            if len(self.history) == 0 or rev > self.history[-1][0]:
                return rev 

    # Returns the number of changed pages
    def receive_updates(self, session:"Session", rev_now:float, deleted_pages:List[str], updated_pages:List[Tuple[str, Dict[str, Any]]]) -> Tuple[int, int]:
        rejected = 0

        # Reject any updates that were already revised
        modified:List[str] = []
        for page_id, page in updated_pages:
            rev = page['rev'] if 'rev' in page else 1e12
            old_content = self.content[page_id] if page_id in self.content else {}
            old_rev = old_content['rev'] if 'rev' in old_content else 0
            if rev + 0.01 < old_rev or rev > rev_now: # The +0.01 is to avoid confusion due to rounding errror
                session.log(f'Rejecting update to: {page_id}. It was revised it at: {old_rev}, but client revised a version from: {rev}. Now={rev_now}')
                rejected += 1
            else:
                page['rev'] = rev_now
                self.content[page_id] = page
                modified.append(page_id)

        # Delete pages
        for page_id in deleted_pages:
            if page_id in self.content:
                del self.content[page_id]
                modified.append(page_id)
            else:
                rejected += 1
                session.log(f'Tried to delete non-existent page: {page_id}')

        # Update the history
        if len(modified) > 0:
            self.history.append((rev_now, modified))
        return len(modified), rejected

    def get_updates(self, start_rev:float, end_rev:float) -> Tuple[List[str], List[Tuple[str, Mapping[str, Any]]]]:
        # Find the starting position. (If regular updates are requested, it will almost always be near the end.)
        hist_pos = len(self.history) - 1
        if hist_pos < 0:
            return [], []
        while hist_pos > 0 and self.history[hist_pos - 1][0] > start_rev:
            hist_pos -= 1
        while hist_pos < len(self.history) and self.history[hist_pos][0] <= start_rev:
            hist_pos += 1

        # Gather updates
        used:Set[str] = set()
        dels: List[str] = []
        upds: List[Tuple[str, Mapping[str, Any]]] = []
        while hist_pos < len(self.history) and self.history[hist_pos][0] < end_rev:
            for page_id in self.history[hist_pos][1]:
                if not page_id in used:
                    used.add(page_id)
                    if page_id in self.content:
                        upds.append((page_id, self.content[page_id]))
                    else:
                        dels.append(page_id)
            hist_pos += 1
        return dels, upds

    # Makes sure all the ids coming from another account are unique from any ids in this account
    def scrub_incoming_pages(self, incoming_pages: List[Tuple[str, Mapping[str, Any]]], root_id: str) -> str:
        for i in range(len(incoming_pages)):
            id = incoming_pages[i][0]
            if id in self.content or id == 'inbox': # This should be a very rare occurence, but it could be disatrous if it ever happens, so...
                while True:
                    new_id = make_random_page_id()
                    if not new_id in self.content and id != 'inbox':
                        break
                for j in range(len(incoming_pages)):
                    page_content = incoming_pages[j][1]
                    if 'items' in page_content:
                        for item in page_content['items']:
                            if 'dest' in item and item['dest'] == id:
                                item['dest'] = new_id
                incoming_pages[i] = (new_id, incoming_pages[i][1])
                if root_id == id:
                    root_id = new_id
        return root_id

    # Saves the data for this account
    def save(self) -> None:
        if self.unsaved_changes == 0:
            return
        self.unsaved_changes = 0
        self.saved = datetime.now()

        # Make a server-side backup file
        account_folder = os.path.join('accounts', self.name)
        os.makedirs(account_folder, exist_ok=True)
        data_path = os.path.join(account_folder, 'data.json')
        s = self.saved.isoformat()
        period_index = s.find('.')
        if period_index < 0:
            period_index = len(s)
        backup_filename = os.path.join(account_folder, s[:period_index].replace(':', '-').replace('T', '_') + '.json')
        if os.path.exists(data_path):
            os.rename(data_path, backup_filename)

        # Save the state
        with open(data_path, 'w') as f:
            f.write(json.dumps(self.marshal(), indent=1))
        log(f'Saved')

        # Cull the old server-side backups.
        # All backups less than two weeks old are kept
        # A limited number of older backups are kept
        now = time.time()
        two_weeks_ago = now - 2 * 7 * 24 * 60 * 60
        two_months_ago = now - 2 * 30 * 24 * 60 * 60
        two_years_ago = now - 2 * 366 * 24 * 60 * 60
        cull_server_side_backups(account_folder, two_years_ago, two_months_ago, 15)
        cull_server_side_backups(account_folder, two_months_ago, two_weeks_ago, 15)

    # Saves if there are a lot of changes,
    # or if there is at least one change and some time has passed since the last save
    def maybe_save(self) -> None:
        if self.unsaved_changes == 0:
            return
        if self.unsaved_changes > 20 or datetime.now() - self.saved > timedelta(minutes=4):
            self.save()

class Session():
    def __init__(self) -> None:
        time_now = datetime.now()
        self.name = '' # The name of the account currently logged in with this session
        self.last_active_time = time_now
        self.last_ip = ''
        self.ban_time = time_now
        self.logs: List[str] = []

    def log(self, s:str) -> None:
        self.logs.append(f'{datetime.now()} (be) {s}')
        log(s) # this calls common.log

    def add_logs(self, response:Dict[str,Any]) -> Dict[str, Any]:
        if 'logs' in response:
            raise ValueError('response already has a logs field')
        response['logs'] = self.logs
        self.logs = []
        return response

    def marshal(self) -> Mapping[str, Any]:
        return {
            'name': self.name,
            'date': self.last_active_time.isoformat(),
        }

    @staticmethod
    def unmarshal(ob: Mapping[str, Any]) -> 'Session':
        sess = Session()
        sess.name = ob['name']
        try:
            sess.last_active_time = datetime.fromisoformat(ob['date'])
        except ValueError:
            sess.log(f'Error parsing date: {ob["date"] if "date" in ob else "no_date"}. Defaulting to now')
            sess.last_active_time = datetime.now()
        return sess

def get_or_make_session(session_id:str, ip_address:str) -> Session:
    recognized = True
    if not session_id in sessions:
        sessions[session_id] = Session()
        recognized = False
    session = sessions[session_id]
    session.last_active_time = datetime.now()
    session.last_ip = ip_address
    if not recognized:
        session.log(f'Made a new session for id: {session_id}')
    return session

def make_test_content() -> Dict[str,Any]:
    new_content = {}
    for page_id in test_content:
        new_content[page_id] = {
            'rev':0.,
            'con':test_content[page_id],
        }
    return new_content

def load_state() -> None:
    global config
    global sessions

    # Load the config file
    if os.path.exists('config.json'):
        with open('config.json', 'r') as f:
            config_file = json.loads(f.read())
        log(f'config.json loaded')
        config.update(config_file)
    else:
        log(f'No config.json file found. Using default configurations.')

    # # Deprecated: Convert old-style state.json file to the new accounts folder form
    # if os.path.exists('state.json'):
    #     with open('state.json', 'r') as f1:
    #         old_state = json.loads(f1.read())
    #     for account_name in old_state['accounts']:
    #         os.makedirs(f'accounts/{account_name}')
    #         with open(f'accounts/{account_name}/data.json', 'w') as f2:
    #             f2.write(json.dumps(old_state['accounts'][account_name], indent=1))
    #     log(f'old state file converted to new format')
    #     os.rename('state.json', 'state_old.json')

    # Make the accounts folder and a test account if an accounts folder does not exist
    if not os.path.exists('accounts'):
        os.makedirs('accounts/test')
        now_str = datetime.now().isoformat()
        test_data = {
            "pw": "f3f63dae0ababb507edcfdfd3a4020ea2bbcadce6a120fcffaa04d7a6388ec6a0442431911dfce2d3e371989c329ae5de9e628abc25c67df00e48bbda86fc60a",
            "content": make_test_content(),
            "created": now_str,
            "accessed": now_str,
            "rev": 1,
        }
        with open('accounts/test/data.json', 'w') as f:
            f.write(json.dumps(test_data, indent=1))
        os.makedirs('accounts/test/files', exist_ok=True)
        shutil.copyfile('server/test_drawing.png', 'accounts/test/files/test_drawing.png')

        log(f'Accounts folder and test data created')

    # Load all the accounts
    global accounts
    account_names = [ name for name in os.listdir('accounts') if os.path.isdir(os.path.join('accounts', name)) ]
    accounts = {}
    for account_name in account_names:
        account_path = os.path.join('accounts', account_name)
        data_path = os.path.join(account_path, 'data.json')
        if not os.path.isfile(data_path):
            log(f'Error, missing data file: {data_path}')
            continue
        with open(data_path, 'r') as data_file:
            account_ob = json.loads(data_file.read())
        accounts[account_name] = Account.unmarshal(account_name, account_ob)

    # Load sessions
    global sessions
    sessions = {}
    if os.path.isfile('sessions.json'):
        try:
            with open('sessions.json', 'r') as sessions_file:
                sessions_ob = json.loads(sessions_file.read())
            for session_id in sessions_ob:
                sessions[session_id] = Session.unmarshal(sessions_ob[session_id])
        except:
            log(f'Something is wrong with sessions.json. Starting over. {traceback.format_exc()}')

# Delete backups until there are no more than maxnum backups in the specified range
def cull_server_side_backups(account_folder:str, start_time: float, end_time: float, maxnum: int) -> None:
    all_backups = [
        (os.path.join(account_folder, f), os.path.getmtime(os.path.join(account_folder, f)))
        for f in os.listdir(account_folder)
        if os.path.isfile(f) and f[:2] == '20' and os.path.splitext(f)[1] == '.json'
    ]
    relevant_backups = [ t for t in all_backups if t[1] >= start_time and t[1] < end_time ]
    sorted_backups = sorted(relevant_backups, key=lambda t: t[1])
    deltas = [ (sorted_backups[i][0], sorted_backups[i][1] - sorted_backups[i - 1][1] if i > 0 else 1000000000) for i in range(len(sorted_backups)) ]
    sorted_deltas = sorted(deltas, key=lambda t: t[1])
    for i in range(len(sorted_deltas) - maxnum):
        condemned_filename = sorted_deltas[i][0]
        os.remove(condemned_filename)

def make_client_side_backup(account_name: str) -> str:
    temp_folder = tempfile.gettempdir()
    time_now = datetime.now()
    backup_name = f'tc_backup_{account_name}_{time_now.year}-{str(time_now.month).zfill(2)}-{str(time_now.day).zfill(2)}'
    dest_backup_folder = os.path.join(temp_folder, backup_name)
    dest_client_folder = os.path.join(dest_backup_folder, 'client')
    dest_server_folder = os.path.join(dest_backup_folder, 'server')
    dest_accounts_folder = os.path.join(dest_backup_folder, 'accounts')
    dest_account_folder = os.path.join(dest_accounts_folder, account_name)
    dest_account_files = os.path.join(dest_account_folder, 'files')

    # Build the directory structure
    if os.path.exists(dest_backup_folder):
        shutil.rmtree(dest_backup_folder, ignore_errors=True)
    os.makedirs(dest_server_folder, exist_ok=True)
    os.makedirs(dest_client_folder, exist_ok=True)
    os.makedirs(dest_account_folder, exist_ok=True)

    # Copy client files
    for filename in os.listdir('./client/'):
        ext = os.path.splitext(filename)[1]
        if ext in ['.js', '.html', '.ico', '.png', '.txt', '.ttf']:
            shutil.copyfile(os.path.join('client', filename), os.path.join(dest_client_folder, filename))

    # Copy server files
    for filename in os.listdir('./server/'):
        ext = os.path.splitext(filename)[1]
        if ext in ['.py', '.service']:
            shutil.copyfile(os.path.join('server', filename), os.path.join(dest_server_folder, filename))

    # Copy account files
    global accounts
    account = accounts[account_name]
    with open(os.path.join(dest_account_folder, 'data.json'), 'w') as f:
        json.dump(account.marshal(), f, indent=1)

    # Copy root files
    for filename in ['run.bash']:
        shutil.copyfile(filename, os.path.join(dest_backup_folder, filename))

    # Make the zip
    archive_name = f'{backup_name}.zip'
    shutil.make_archive(f'client/{backup_name}', 'zip', temp_folder, backup_name)
    shutil.rmtree(dest_backup_folder, ignore_errors=True)
    # todo: cull old client-side backups zips
    return archive_name

def save_sessions() -> None:
    global sessions
    expire_days = 42
    now_time = datetime.now()
    sess = {}
    for session_id in sessions:
        if now_time - sessions[session_id].last_active_time < timedelta(days=expire_days):
            sess[session_id] = sessions[session_id].marshal()
        else:
            name = sessions[session_id].name
            name_str = f' for {name}' if len(name) > 0 else ''
            log(f'Dropping session {session_id}{name_str} because it has not been used for {expire_days} days. Now={now_time}, last_active_time={sessions[session_id].last_active_time}')
    try:
        with open('sessions.json', 'w') as f:
            f.write(json.dumps(sess, indent=1))
    except:
        log(f'Error saving sessions: {traceback.format_exc()}')
    log('sessions saved')

def save_all() -> None:
    # Save all the accounts
    global accounts
    for account_name in accounts:
        account = accounts[account_name]
        account.save()

    # Save sessions
    save_sessions()

def maybe_save_all() -> None:
    global accounts
    for account_name in accounts:
        account = accounts[account_name]
        account.maybe_save()

# Here is an example config.json file:
# {
#   "host": "gashler.com",
#   "port": 8981,
#   "monitor": 90,
#   "ssl_privkey": "/home/mike/.ssl/privkey.pem",
#   "ssl_cert": "/home/mike/.ssl/fullchain.pem"
#  }
config: Dict[str, Any] = {
    'ban_time': 5, # seconds
}

accounts: Dict[str, Account] = {}
sessions: Dict[str, Session] = {}
