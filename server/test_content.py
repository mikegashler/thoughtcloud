# ---------------------------------------------------------------
# The contents of this file are dedicated to the public domain as
# described at http://creativecommons.org/publicdomain/zero/1.0/.
# ---------------------------------------------------------------

test_content = {
  "start": {
    "thought": "This is a test account. Any changes you make in this account will be reset daily. So please feel free to tinker.",
    "items": [
      {
        "text": "to do",
        "dest": "l5irRS",
        "mod": 1699447542535
      },
      {
        "text": "doodles",
        "dest": "DS7Key"
      },
      {
        "text": "shopping list",
        "dest": "no50Fs",
        "mod": 1699450272786
      },
      {
        "text": "facts to study for test",
        "dest": "IUPaZa",
        "mod": 1699452444642
      },
      {
        "text": "speech outline",
        "dest": "wCRfsH",
        "mod": 1699453514723
      },
      {
        "text": "reading list",
        "dest": "BNFqdg",
        "mod": 1699452439102
      },
      {
        "text": "dates when you-know-what occurre...",
        "dest": "qTDo3P",
        "mod": 1699452899334
      },
      {
        "text": "Inventory",
        "dest": "4bYGNj",
        "mod": 1731796527276
      }
    ],
    "mode": "tile",
    "mod": 1699449720558
  },
  "l5irRS": {
    "thought": "to do",
    "items": [
      {
        "text": "do immediately",
        "dest": "m5Ks0Y",
        "mod": 1699447550243
      },
      {
        "text": "do soon",
        "dest": "MVI4Qc",
        "mod": 1699447747418
      },
      {
        "text": "do eventually",
        "dest": "yxJXyM",
        "mod": 1699447756234
      }
    ],
    "mod": 1699447542535
  },
  "m5Ks0Y": {
    "thought": "do immediately",
    "items": [
      {
        "text": "Make dentist appointment",
        "mod": 1699447598525
      },
      {
        "text": "Visit John Doe"
      },
      {
        "text": "Complete presentation"
      }
    ],
    "mod": 1699447550242
  },
  "MVI4Qc": {
    "thought": "do soon",
    "items": [
      {
        "text": "buy groceries"
      },
      {
        "text": "make a diet plan"
      },
      {
        "text": "reply to that one guy who keeps sending me stuff"
      }
    ],
    "mod": 1699447747417
  },
  "yxJXyM": {
    "thought": "do eventually",
    "items": [
      {
        "text": "clean out the attic"
      },
      {
        "text": "wash car"
      },
      {
        "text": "slick up resume"
      }
    ],
    "mod": 1699447756234
  },
  "DS7Key": {
    "thought": "doodles",
    "items": [
      {
        "text": "Fish car idea.\nThis is pretty-much the greatest idea ever.",
        "mod": 1726244870304,
        "file": "test_drawing.png"
      },
      {
        "text": "If you change an image, don't forget to press the save 💾 button.",
        "mod": 1699453363626
      }
    ]
  },
  "no50Fs": {
    "thought": "shopping list",
    "items": [
      {
        "text": "eggs",
        "icon": "✅"
      },
      {
        "text": "milk",
        "icon": "✅"
      },
      {
        "text": "butter",
        "icon": "✅"
      },
      {
        "text": "flour",
        "icon": "✅"
      },
      {
        "text": "chips",
        "icon": "✅"
      },
      {
        "text": "salmon"
      },
      {
        "text": "yogurt"
      },
      {
        "text": "tomatoes"
      },
      {
        "text": "onions"
      },
      {
        "text": "hamburger"
      },
      {
        "text": "tortillas"
      },
      {
        "text": "salsa"
      }
    ],
    "mod": 1699450272784
  },
  "IUPaZa": {
    "thought": "facts to study for test",
    "items": [
      {
        "text": "(Note: text enclosed in brackets is hidden until you click on it.)",
        "mod": 1699452272891
      },
      {
        "text": "The capital of France is [Paris].",
        "mod": 1699452262096
      },
      {
        "text": "7 * 8 = [56]"
      },
      {
        "text": "Some [frogs] are capable of detecting individual photons.",
        "mod": 1699452247635
      },
      {
        "text": "John Adams was the [second] President of the United States.",
        "mod": 1699452344874
      },
      {
        "text": "[cosecant] is the reciprocal of sine."
      },
      {
        "text": "The national animal of Scotland is [a unicorn]."
      }
    ],
    "mod": 1699452444642
  },
  "BNFqdg": {
    "thought": "reading list",
    "items": [
      {
        "text": "Tour of Python for Engineers: https://youtu.be/bDbucbjisnk"
      },
      {
        "text": "The Art of War by Sun Tzu"
      },
      {
        "text": "Inductive Biases for Deep Learning of Higher-Level Cognition: https://arxiv.org/abs/2011.15091"
      },
      {
        "text": "To Kill a Mockingbird by Harper Lee"
      }
    ],
    "mod": 1699452439102
  },
  "qTDo3P": {
    "thought": "dates when you-know-what occurred",
    "items": [
      {
        "text": "Date, Days since last occurrence",
        "mod": 1726259763844
      },
      {
        "text": "2023-10-18, ",
        "mod": 1726259738085
      },
      {
        "text": "2023-10-19, \"=v(r,c-1)-v(r-1,c-1)\"",
        "mod": 1726243626672
      },
      {
        "text": "2023-10-20, \"=v(r,c-1)-v(r-1,c-1)\"",
        "mod": 1726243626672
      },
      {
        "text": "2023-10-21, \"=v(r,c-1)-v(r-1,c-1)\"",
        "mod": 1726243626672
      },
      {
        "text": "2023-10-21, \"=v(r,c-1)-v(r-1,c-1)\"",
        "mod": 1726243626672
      },
      {
        "text": "2023-10-22, \"=v(r,c-1)-v(r-1,c-1)\"",
        "mod": 1726243626672
      },
      {
        "text": "2023-10-24, \"=v(r,c-1)-v(r-1,c-1)\"",
        "mod": 1726243626672
      },
      {
        "text": "2023-10-26, \"=v(r,c-1)-v(r-1,c-1)\"",
        "mod": 1726243626672
      },
      {
        "text": "2023-10-30, \"=v(r,c-1)-v(r-1,c-1)\"",
        "mod": 1726243626672
      },
      {
        "text": "2023-11-01, \"=v(r,c-1)-v(r-1,c-1)\"",
        "mod": 1726243626672
      },
      {
        "text": "2023-11-08, \"=v(r,c-1)-v(r-1,c-1)\"",
        "mod": 1726243626672
      }
    ],
    "mod": 1699452899334,
    "mode": "grid",
    "footers": [
      "{\"name\":\"line\",\"params\":[\"1\",\"1\",\"12\",\"2\"]}"
    ]
  },
  "wCRfsH": {
    "thought": "speech outline",
    "items": [
      {
        "text": "intro",
        "icon": "#"
      },
      {
        "text": "tell joke about priest and fire hydrant",
        "ind": 1,
        "icon": "#"
      },
      {
        "text": "main idea",
        "mod": 1699453578881,
        "icon": "#"
      },
      {
        "text": "people like saunas",
        "mod": 1699453632084,
        "ind": 1,
        "icon": "#"
      },
      {
        "text": "global warming will be cool",
        "ind": 1,
        "icon": "#"
      },
      {
        "text": "actually, don't use the word \"cool\" (for obvious reasons)",
        "ind": 2,
        "mod": 1699453746195,
        "icon": "#"
      },
      {
        "text": "buy our aerosols",
        "ind": 1,
        "icon": "#"
      },
      {
        "text": "supporting evidence",
        "icon": "#"
      },
      {
        "text": "testimonial from guy on bus",
        "ind": 1,
        "icon": "#"
      },
      {
        "text": "anecdote involving worm and doughnut",
        "ind": 1,
        "icon": "#"
      },
      {
        "text": "conclusion",
        "icon": "#"
      },
      {
        "text": "people do whatever they want anyway",
        "ind": 1,
        "icon": "#"
      }
    ],
    "mod": 1699453514723
  },
  "4bYGNj": {
    "thought": "Inventory",
    "items": [
      {
        "mod": 1731799197360,
        "text": "Descr, Est. Manufactured Date, Owned by MOAM, Donor, Donor contact, Est. Value, Notes, Building, Location, Shelf, Quantity"
      },
      {
        "mod": 1731799200352,
        "text": "Life-sized cast of a flying monkey from the Wizard of Oz, 1939, Yes, Myra Elmwood, \"38217 Pleasant View Court, Masonville MT 57022\", $16811, Only 10 were made, Main, Toy room, 0, 1"
      },
      {
        "mod": 1731799262608,
        "text": "Bull prick cane, 1982, Yes, Pete Zibrams, 416-225-1458, $14, Pete tanned it himself when the bull died, Main, \"Front room, behind glass display case\", , 2"
      },
      {
        "mod": 1731799338130,
        "text": "Giant Cracker Jack poster, 1977, On loan, , , \"$12,000\", , , , , 1"
      },
      {
        "mod": 1731799440280,
        "text": "WW2 Ammo carrier, 1942, Yes, , , , \"Must return to John Tibrams on January 1, 2032\", , In Military vehicle tent, , 1"
      }
    ],
    "mod": 1731796527275,
    "mode": "grid"
  }
}