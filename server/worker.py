# ---------------------------------------------------------------
# The contents of this file are dedicated to the public domain as
# described at http://creativecommons.org/publicdomain/zero/1.0/.
# ---------------------------------------------------------------

from typing import Mapping, Any
import state
from datetime import datetime, timedelta
import os
import shutil

config = state.config

def make_error(session:state.Session, message:str) -> Mapping[str, Any]:
    session.log(message)
    return {
        'status': 'error',
        'message': message,
    }

def make_ops_page(params: Mapping[str, Any], session: state.Session) -> Mapping[str, Any]:
    if not 'act' in params:
        return make_error(session, f'expected an act parameter')
    action = params['act']
    if action == 'logout': # Explicitly log out
        msg = f'user {session.name} logged out'
        session.log(msg)
        session.name = ''
        return {
            'status': 'logged_out',
            'reason': msg,
            'accounts': list(state.accounts.keys()),
        }
    elif action == 'change_pw':
        if datetime.now() < session.ban_time:
            session.log(f'banned user {params["name"] if "name" in params else ""} attempted to change the password')
            return { 'status': 'banned', 'message': f'Sorry, you are still under a {config["ban_time"]} second ban for getting the password wrong. Please try again later.' }
        name = params['name']
        old_pw = params['old_pw']
        new_pw = params['new_pw']
        if not name in state.accounts:
            return make_error(session, f'sorry, no account named {name} was found to change password')
        account = state.accounts[name]
        if old_pw != account.password:
            session.ban_time = datetime.now() + timedelta(seconds=config['ban_time'])
            session.log(f'user {name} entered the wrong password in attempt to change password. Banning for {config["ban_time"]} seconds')
            return { 'status': 'wrong', 'message': f'sorry, the old password entered was incorrect' }
        account.password = new_pw
        session.log(f'user {name} changed the password')
        return { 'status': 'changed', 'message': 'The password has been changed' }
    elif action == 'change_name':
        if datetime.now() < session.ban_time:
            session.log(f'banned user {params["old_name"] if "old_name" in params else ""} attempted to change usernames')
            return { 'status': 'banned', 'message': f'Sorry, you are still under a {config["ban_time"]} second ban for getting the password wrong. Please try again later.' }
        old_name = params['old_name']
        new_name = params['new_name']
        pw = params['pw']
        if not old_name in state.accounts:
            return make_error(session, f'sorry, no account named {old_name} was found to change name')
        account = state.accounts[old_name]
        if pw != account.password:
            session.ban_time = datetime.now() + timedelta(seconds=config['ban_time'])
            session.log(f'user {old_name} entered the wrong password in attempt to change username. Banning for {config["ban_time"]} seconds')
            return { 'status': 'wrong', 'message': f'sorry, the password entered was incorrect' }
        if new_name in state.accounts:
            session.log(f'user {old_name} wanted to change usernames to {new_name}, but it was already taken')
            return make_error(session, f'sorry, the name {new_name} is already taken for change name')
        state.accounts[new_name] = account
        del state.accounts[old_name]
        session.log(f'user {old_name} changed username to {new_name}')
        return { 'status': 'changed', 'message': 'The name has been changed' }
    elif action == 'del_account':
        if datetime.now() < session.ban_time:
            session.log(f'banned user {params["name"] if "name" in params else ""} attempted to delete the account')
            return { 'status': 'banned', 'message': f'Sorry, you are still under a {config["ban_time"]} second ban for getting the password wrong. Please try again later.' }
        name = params['name']
        pw = params['pw']
        if not name in state.accounts:
            return make_error(session, f'sorry, no account named {name} was found to delete')
        account = state.accounts[name]
        if pw != account.password:
            session.log(f'user {name} entered the wrong password in attempt to delete the account. Banning for {config["ban_time"]} seconds')
            session.ban_time = datetime.now() + timedelta(seconds=config['ban_time'])
            return { 'status': 'wrong', 'message': f'sorry, the password entered was incorrect' }
        del state.accounts[name]
        msg = f'user {name} deleted their account'
        session.log(msg)
        return { 'status': 'logged_out', 'reason': msg, 'accounts': list(state.accounts.keys()) }
    elif action == 'save': # This is an obscure feature for debugging only. Users should not need to use it.
        if len(session.name) < 1:
            msg = 'logged out user tried to do server-side save'
            session.log(msg)
            return { 'status': 'logged_out', 'reason': msg, 'accounts': list(state.accounts.keys()) }
        state.save_all()
        return { 'status': 'done', 'message': 'Saved.' }
    elif action == 'backup': # makes a client-side backup archive
        if len(session.name) < 1:
            msg = 'logged out user tried to get a backup'
            session.log(msg)
            return { 'status': 'logged_out', 'reason': msg, 'accounts': list(state.accounts.keys()) }
        backup_url = state.make_client_side_backup(session.name)
        session.log(f'backup made for user {session.name}')
        return { 'status': 'backup_ready', 'url': backup_url }
    else:
        return make_error(session, f'unrecognized ops action: {action}')

def maybe_reset_test_account(session: state.Session, account:state.Account, now:datetime) -> None:
    if session.name == 'test':
        if now - account.created > timedelta(days=1):
            account.content = state.make_test_content()
            account.created = now
            shutil.copyfile('server/test_drawing.png', 'accounts/test/files/test_drawing.png')

# Logs in or creates a new account (and logs in)
def make_login_page(params: Mapping[str, Any], session: state.Session) -> Mapping[str, Any]:
    if datetime.now() < session.ban_time:
        session.log(f'banned user {params["name"] if "name" in params else ""} attempted to log in')
        return { 'status': 'banned', 'message': f'Sorry, you are still under a {config["ban_time"]} second ban for getting the password wrong. Please try again later.' }
    if not 'name' in params:
        return make_error(session, f'expected a name parameter to login')
    if len(params['name']) < 1:
        return make_error(session, f'expected the name to have at least one character')
    if not 'pw' in params:
        return make_error(session, f'expected a pw parameter to login')
    name = params['name']
    pw = params['pw']
    action = params['action'] if 'action' in params else 'login'
    if action == 'new':
        # Make a new account
        if name in state.accounts:
            return make_error(session, f'sorry, the name {name} is already taken')
        account = state.Account(name)
        account.password = pw
        state.accounts[name] = account
        session.name = name
        session.log(f'user {session.name} made a new account')
        state.save_sessions()
    elif action == 'login':
        if not name in state.accounts:
            return make_error(session, f'sorry, no account named {name} was found to login')
        account = state.accounts[name]
        if pw != account.password:
            session.ban_time = datetime.now() + timedelta(seconds=config['ban_time'])
            session.log(f'user {name} entered the wrong password. Banning for {config["ban_time"]} seconds')
            return { 'status': 'wrong', 'message': f'sorry, the password entered was incorrect' }
        if session.name != name: # No need to do anything if the user is just refreshing the page
            session.name = name
            state.save_sessions()
        #session.log(f'user {session.name} logged in')
    else:
        return make_error(session, f'Unrecognized login action: {action}')
    response = {
        'status': 'ok',
    }
    return response

# Get specified pages
def make_get_page(params: Mapping[str, Any], session: state.Session) -> Mapping[str, Any]:
    if len(session.name) < 1:
        msg = f'get stranger'
        session.log(msg)
        return {
            'status': 'logged_out',
            'reason': msg,
            'accounts': list(state.accounts.keys()),
        }
    if not session.name in state.accounts:
        return {
            'status': 'logged_out',
            'reason': f'no account named {session.name} for get. Logging out',
            'accounts': list(state.accounts.keys()),
        }
    account = state.accounts[session.name]

    # Reset the test account if needed
    now = datetime.now()
    maybe_reset_test_account(session, account, now)

    # Get the updates and failures
    updated = []
    deleted = []
    for page in params['pages']:
        if page in account.content:
            updated.append((page, account.page(page)))
        else:
            deleted.append(page)

    # Send the requested content
    account.accessed = now
    response = {
        'status': 'ok',
        'del': deleted,
        'upd': updated,
    }

    return response

# Receive updates from the client, and send any updates back
def make_sync_page(params: Mapping[str, Any], session: state.Session) -> Mapping[str, Any]:
    if len(session.name) < 1:
        msg = 'logged out client tried to sync'
        session.log(msg)
        return session.add_logs({
            'status': 'logged_out',
            'reason': msg,
        })
    if not session.name in state.accounts:
        session.log('unrecognized account tried to sync')
        session.name = ''
        return session.add_logs({
            'status': 'error',
            'message': f'no account named {session.name}',
        })

    # Extract params
    account = state.accounts[session.name]
    rev_prev:float = params['rev']
    rev_now:float = account.now()

    # Receive changes
    accepted, rejected = account.receive_updates(session, rev_now, params['del'], params['upd'])
    account.unsaved_changes += accepted

    # Respond with updates
    dels, upds = account.get_updates(rev_prev, rev_now)
    account.maybe_save()
    return session.add_logs({
        'status': 'received',
        'rev': rev_now,
        'del': dels,
        'upd': upds,
        'acc': accepted,
        'rej': rejected,
        'tim': datetime.now().isoformat(),
    })

# This is called when the client's service has shut down,
# and so does not prevent this page from being reached
# by intercepting the fetch to this page.
def make_sw_page(params: Mapping[str, Any], session: state.Session) -> Mapping[str, Any]:
    return {
        'sw': False,
        'logs': [],
    }

# This is called when the client wants to replace all the content in one shot
def make_resend_page(params: Mapping[str, Any], session: state.Session) -> Mapping[str, Any]:
    if len(session.name) < 1:
        msg = f'logged out user attempted to resend data. (Denied.)'
        session.log(msg)
        return {
            'status': 'logged_out',
            'reason': msg,
        }
    if not session.name in state.accounts:
        session.log(f'unrecognized user {session.name} attempted to resend data')
        session.name = ''
        return {
            'status': 'error',
            'message': f'no account named {session.name}',
        }

    # Extract params
    account = state.accounts[session.name]
    client_revision = params['rev']

    # Receive the full content
    session.log(f'receiving full content from user {session.name}')
    account.reset_history(client_revision)
    accepted, rejected = account.receive_updates(session, client_revision, [], params['upd'])

    # Respond
    state.save_all()
    return {
        'status': 'received',
        'rev': client_revision,
        'del': [],
        'upd': [],
        'acc': accepted,
        'rej': rejected,
    }

# This is called when the client uploads a files
def make_upload_page(params: Mapping[str, Any], session: state.Session) -> Mapping[str, Any]:
    print(f'params={params}')
    # Find the account
    account = state.accounts[session.name]
    print(f'account.name={account.name}')

    # Move the file into the account files folder
    source_path = params['file']
    dest_dir = f'accounts/{account.name}/files'
    os.makedirs(dest_dir, exist_ok=True)
    dest_filename = params['dest']
    dest_path = os.path.join(dest_dir, dest_filename)
    os.rename(source_path, dest_path)

    # Delete the thumbnail if there is one
    thumb_path = os.path.join(dest_dir, f'thumb_{dest_filename}')
    if os.path.exists(thumb_path):
        os.remove(thumb_path)

    # Return something
    session.log(f'received upload: {dest_path}')
    return {
        'status': 'received_upload',
    }